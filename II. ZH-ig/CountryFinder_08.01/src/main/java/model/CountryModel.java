/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import country.Country;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Aliz
 */
public class CountryModel implements Model {
    //ez a nyilvántartás amely tartalmazza az országokat

    private Controller controller;
    private Set<Country> countries = new HashSet<>();

    private static final Logger LOGGER = Logger.getLogger(CountryModel.class.getName());
    private String fileName = "countries.txt";

    public CountryModel() {
        this.load(fileName);
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public Set<Country> getAllCountries() {
        return this.countries;
    }

    @Override
    public void load(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String temp = reader.readLine();
            countries.clear();      //előzetesen kiürítjük a listát
            while (temp != null) {
                temp = temp.trim();
                if (!temp.isEmpty()) {
                    countries.add(new Country(temp));
                    }
                temp = reader.readLine();
            }
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void filterCountries(String name) {
       Set<Country> countries = this.countries.stream()
                .filter(c -> c.toString().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toSet());
       this.countries = countries;
    }

}
