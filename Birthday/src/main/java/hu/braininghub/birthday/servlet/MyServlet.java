/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.birthday.servlet;

import hu.braininghub.birthday.service.MyService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet létrehozása. Minden életciklus lefutása esetén legyen valami a
 * konzolra írva.
 *
 * doGet: URL paraméterben megkapott születési évszámot egy JSP-n megjeleníti,
 * úgy hogy a Servletbe injektált Service rétegbeli objektum az utolsó két
 * számot megtartja,a többit levágja.
 *
 * XYZService is igaz, hogy az életciklusai lefutása esetén legyen valami a
 * konzolra írva.
 *
 * @author Aliz
 */
@WebServlet(name = "MyServlet", urlPatterns = "/myservlet")
public class MyServlet extends HttpServlet {

    @Inject
    MyService service;

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("This is the beginning");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String number = request.getParameter("birthDate");

        String birthday = service.printBirthday(number);
        request.setAttribute("birthday", birthday);
        request.getRequestDispatcher("WEB-INF/birthdate.jsp").forward(request, response);

    }

    @Override
    public void destroy() {
        System.out.println("This is the end.");
    }

}
