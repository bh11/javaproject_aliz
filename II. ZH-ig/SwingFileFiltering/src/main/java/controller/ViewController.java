/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Aliz
 */
public interface ViewController extends Controller{
    
    void handleGoButton(String path);  //ezt a view fogja meghívni
    
    void save();
}
