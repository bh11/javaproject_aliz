<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:52:18
    Author     : Aliz
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Employees</h1>
        <br/>
        <div>
            <form method="get" action="EmployeeServlet">
                Please type the name for which you are searching for:
                <input type="text" name="name">
                <input type="submit" value="Go"/>
            </form>
        </div>
        <br/>
        <table border="1">
            
            <% 
                List<String> names = (List<String>)request.getAttribute("names");
                
                for(String s : names) {
                    out.print("<tr><td>" + s + "</td></tr>");
                }
                %>
            
        </table>
        
    </body>
</html>
