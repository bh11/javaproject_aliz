/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class DivideBy7 {

    public static void main(String[] args) {

        //Kérj be egy számot a felhasználótól. Ha 7-tel osztható, írd ki, hogy "kismalac". 
        //Ezt a feladatot oldd meg if és switch felhasználásával is.
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();

        if (number / 7 == 0) {
            System.out.println("Kismalac.");
        } else {
            System.out.println("A szam 7-el nem oszthato.");
        }

        System.out.println("Megoldas switch-el:");
        switch (number % 7) {
            case 0:
                System.out.println("Kismalac.");
                break;
            default:
                System.out.println("A szam 7-el nem oszthato.");
        }
    }
}
