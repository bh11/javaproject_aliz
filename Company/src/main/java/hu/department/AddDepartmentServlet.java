/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.department;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
public class AddDepartmentServlet extends HttpServlet{
    
    private DepartmentService departmentService = new DepartmentService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         departmentService.addDepartment(new Department(request.getParameter("name"), 
                request.getParameter("ID"))); 
        request.setAttribute("departments", departmentService.getDepartments());
        request.getRequestDispatcher("departments.jsp").forward(request, response);
    }
    
    
}
