/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import employee.Boss;
import employee.Worker;
import employee.WorkerFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aliz
 */
public class RegistryModel implements Model{
    //ez a nyilvántartás amely tartalmaza a cég dolgozóit
    //főnököket kezeljük név alapján és a főnökökön keresztül hozzáférünk a worker-ekhez
    
    private static final Logger LOGGER = Logger.getLogger(RegistryModel.class.getName());
    
    private Map<String, Boss> bosses = new HashMap<>();

    @Override
    public Boss addBoss(String name) {
        Boss boss = new Boss(name);
        bosses.put(name, boss);
        return boss;
    }

    @Override
    public Worker addWorker(Boss boss, String name, int age) {
        WorkerFactory wf = new WorkerFactory(boss);
        Optional<Worker> mayAddedworker = wf.createNewWorker(name, age);
        
        //ha ez false, akkor az Optional dobozban nincs az az új dolgozó akit éppen létrehoztunk, mert már van belőle
        if(mayAddedworker.isPresent()) {
            return mayAddedworker.get();    //itt kezeljük Optionalből és visszaadjuk a új dolgozót
        }
        
        //kikeressük a workert ha már létezik és visszatérünk a meglévő dolgozóval
        return boss.getWorkers().stream()
                .filter(w -> w.getName().equals(name) && w.getAge() == age)
                .findFirst()
                .get();
    }

    @Override
    public Boss getBoss(String name) {
        return this.bosses.get(name);
    }

    @Override
    public Collection<Boss> getAllBosses() {
        //visszaadja a benne lévő értékeket
        return this.bosses.values();
    }

        @Override
    public void save(String fileName) {
        try (FileOutputStream fs = new FileOutputStream(fileName);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(this.bosses);
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);            
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, null, io);
        }
    }

    @Override
    public void load(String fileName) {
        try (FileInputStream fs = new FileInputStream(fileName);
                ObjectInputStream ou = new ObjectInputStream(fs)) {
            this.bosses = (Map<String, Boss>) ou.readObject();
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);          
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);           
        } catch (ClassNotFoundException ex) {
            //automatikusan javaolja a Netbeans a try után
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
}
