/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Aliz
 */
public class TwoSets {
    /*5.	Egy metódus paraméterül kap két Set<String> kollekciót. 
    Adjuk meg, hány olyan elem van, ami csak az egyik paraméterben fordul elő?*/
    public static int individualElementsOfTwoSets(Set<String> set1, Set<String> set2){
        HashSet<String> result = new HashSet<>();
        //unió
        result.addAll(set1);
        result.addAll(set2);
        //metszet
        HashSet<String> intersection = new HashSet<>(set1);
        intersection.retainAll(set2);
        //az unióból kivonjuk a metszetet
        result.removeAll(intersection);
        
        return result.size();      
    }
    
    public static void main(String[] args) {
        List<String> temp1 = Arrays.asList("ff", "bla", "gg", "Karoly", "mi", "ho");
        HashSet<String> mySet1 = new HashSet<>(temp1);
        
        List<String> temp2 = Arrays.asList("ff", "ekg", "jj", "Karoly", "siraly", "ki");
        HashSet<String> mySet2 = new HashSet<>(temp2);
        
        int numberOfIndividualElements = individualElementsOfTwoSets(mySet1, mySet2);
        System.out.println(numberOfIndividualElements);       
    }   
}
