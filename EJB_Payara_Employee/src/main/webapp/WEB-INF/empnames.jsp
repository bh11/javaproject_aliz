<%-- 
    Document   : empnames
    Created on : 2020. jan. 31., 20:52:18
    Author     : Aliz
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Employees</h1>
        <br/>
        <div>
            <form method="post" action="EmployeeServlet">
                Please type the name for which you are searching for:
                <input type="text" name="partOfName">
                <input type="submit" value="Filter by part of name"/>
            </form>
        </div>
        <br/>
            <p>First table</p>
                <table border="1">
                    <c:forEach var="name" items="${names}">
                        <tr><td><c:out value="${name}"/></td></tr>
                    </c:forEach>              
                </table>  
        <br/>
        <p>Second table</p>
            <table border="1">
                <c:forEach var ="name" items="${names}">
                    <tr><td>
                            <c:out value="${name}"/>
                            (<c:out value="${name.length()}"/>)
                        </td></tr>
                </c:forEach>
            </table>     
    </body>
</html>
