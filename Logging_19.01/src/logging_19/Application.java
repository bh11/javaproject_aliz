/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logging_19;

import controller.Controller;
import controller.MyController;
import model.LogStore;
import model.Model;
import view.ConsoleView;
import view.SwingView;
import view.View;

/**
 *
 * @author Aliz
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        View view;
        if(args.length != 0 && args[0].equals("console")){
            view = new ConsoleView();
        } else {
            view = new SwingView();
        }
        
        Model model = new LogStore();
        Controller controller = new MyController(model, view);
        
        view.build();
    }
    
}
