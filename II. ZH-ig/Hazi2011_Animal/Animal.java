/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2011_Animal;

/**
 *
 * @author Aliz
 */
public class Animal {
    private String name;
    private int age;
    private int countOfLegs;
    
    private int index = 0;

    public Animal() {
    }
       
    public void say(){
        System.out.println("OOOO.");
    }
    

    public String getName() {
        return this.name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    protected void setAge(int age) {
        this.age = age;
    }

    public int getCountOfLegs() {
        return this.countOfLegs;
    }

    protected void setCountOfLegs(int countOfLegs) {
        this.countOfLegs = countOfLegs;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", age=" + age + ", countOfLegs=" + countOfLegs + '}';
    }
    
    
    
    
}
