/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.linkedlist;

/**
 *
 * @author Aliz
 */
public class SinglyLinkedList<T> {
    
    private Node head;  //first node of linkedlist
    
    public int length(){
        int length = 0;
        Node current = head;    //current is the first node
        while(current != null){ //null a vége a láncnak
            length++;
            current = current.next;     //léptetjük a node-ot
        }
        return length;
    }
    
    private Node tail(){
        Node tail = head;
        
        while(tail.next != null){   //amíg a végére nem értünk
            tail = tail.next;
        }
        return tail;
    }
    
    public void append(T data){
        if(head == null){
            head = new Node<T>(data);  //ha nincs lánc akkor legyen egy
            return;
        }
        tail().next = new Node<T>((data)); //ha van, akkor menjen a végére és fűzzon hozzá egy újat
        
    }
    
    public boolean isEmpty(){
        return head == null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node current = head;
        
        builder.append("[");
        while(current != null){
            builder.append(current);
            current = current.next;
            if(current != null){
                builder.append(", ");
            }
        }
        builder.append("]");
        
        return builder.toString();
    }
    
    
        private static class Node<T>{

            private Node<T> next;
            private T data;

            public Node(T data){
                this.data = data;
            }

            @Override
            public String toString() {
                return data.toString();
            }        
        
    }
}
