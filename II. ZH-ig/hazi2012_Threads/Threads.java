/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2012_Threads;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aliz
 */
public class Threads implements Runnable{
    /*
    3. feladat:
    Legyen egy olyan szálosztály, ami a Runnable interfészt valósítja meg és megfelel a következő követelménynek:
        Az osztály megfelelő metódusa 5 másodpercenként kiírja a neved és a neptunkódod mellett a pontos dátumot (év.hó.nap.óra.perc.másodperc formában) (végtelen ciklusban).
    A main metódusban indíts el 5 szálat a következőképpen (ti objektumok szálak, ahol i=1...5):
    t1.start();
    t1.join();
    t2.start();
    t2.join(1000);
    t3.start();
    t3.join();
    t4.start();
    t5.start();
    Magyarázd meg az eredményt!
    */
    
    public static void print(String name, int neptun) throws InterruptedException{
        while(true){
            System.out.println("Your name: "+name);
            System.out.println("Your neptun code: "+neptun);
            System.out.println("Current time: "+LocalDateTime.now());
            Thread.sleep(5000);
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        Thread t1 = new Thread();
        Thread t2 = new Thread(new Threads());
        Thread t3 = new Thread();
        Thread t4 = new Thread();
        Thread t5 = new Thread();
        
        
        t1.start();
        t1.join();
        t2.start();
        t2.join(1000);
        t3.start();
        t3.join();
        t4.start();
        t5.start();
    
    }

    @Override
    public void run() {
        try {
            print("Aliz", 1234);
        } catch (InterruptedException ex) {
            Logger.getLogger(Threads.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
