/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi0412_LambdaList;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class LambdaList {
    public static void main(String[] args) {
        
        List<String> list = new ArrayList<>();
        list.add("malacka");
        list.add("kutya");
        list.add("mókus");
        list.add("kacsacsoruemlos");
        list.add("elefant");
        list.add("alma");
        
        //hossz szerinti sorbarendezés
        /*
        Ha a ugyanakkora mint b, akkor az eredmény 0.
        Ha a nagyobb mint b, akkor pozitív.
        Ha a kisebb mint b, akkor negatív.
        */
        list.sort((a, b) -> a.length() - b.length());//modositja a lista sorrendjet
        System.out.println("Sorrend hossz alapján növekvő: "+list);
        
        //hosszúság szerint visszafelé
        //ami negatív volt az most pozitív
        list.sort((a, b) -> b.length() - a.length());
        System.out.println("Sorrend hossz alapján csökkenő: "+list);
        
        //ABC sorrend az első karaktert nézve
        list.sort((a, b) -> a.charAt(0) - b.charAt(0));
        System.out.println("ABC sorrend szerint: "+list);
        
        //Az a String az első amely először tartalmaz "e" karaktert
        list.sort((strA, strB) -> {
            //ha az strA e-vel kezdodik, de az strB nem akkor az strA elorebb 
            if(strA.charAt(0) == 'e' && strB.charAt(0) != 'e'){
                return -1;//elorebb legyen
            }
            
            if(strA.charAt(0) == strB.charAt(0)){
                return 0;
            }
            
            return 1;//minden mas esetben hatrebb kerul
        });
        System.out.println("e sorrend szerint: "+list);
        
    }
}
