/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;


/**
 *
 * @author Aliz
 */
public interface Filter {
    //paramáterül kapunk egy filet
    boolean filter(File file);
    boolean test(File file);   //ha default akkor viszont mindig felül kell írni

}
