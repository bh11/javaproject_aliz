/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package save;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class Save {
      public static void save(String expression, String fileName){
       try (FileWriter fw = new FileWriter(fileName);
             PrintWriter pw = new PrintWriter(fw);) //kiírjuk egy fájlba            
        {
            pw.println(expression);
        } catch (IOException ex) {
            System.out.println(ex);
        }
   }
}
