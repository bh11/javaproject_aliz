/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Aliz
 */
public interface Controller {
    
    /**
     * Button pressed adds a Log to a list and saves the list.
     */
    void handleButton();
    
    /**
     * After the model has been updated, notify View.
     */
    void notifyView();
}
