/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

/**
 *
 * @author Aliz
 */
public final class Commands {
    //az összes konstans amit meg tud kapni a konzol
    //vagy abstract, vagy ami jobb: privát a konstruktor és finallé az osztályt
    
    public static final String CHANGE_DIRECTORY = "cd";
    public static final String LIST = "ls";
    public static final String PARENT_DIRECTORY = "..";
    public static final String PWD = "pwd";
    public static final String RENAME = "mv";
    
    //privat konstruktor
    private Commands(){
        
    }
}
