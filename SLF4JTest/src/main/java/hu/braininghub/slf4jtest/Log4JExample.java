/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.slf4jtest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *
 * @author Aliz
 */
public class Log4JExample {
     
    private static Logger logger = LoggerFactory.getLogger(Log4JExample.class);
 
    public static void main(String[] args) {
        logger.debug("Debug log message");
        logger.info("Info log message");
        logger.error("Error log message");
    }
    
}
