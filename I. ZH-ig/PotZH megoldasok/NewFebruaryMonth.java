/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh1pot;

/**
 *
 * @author Aliz
 */
public class NewFebruaryMonth {

    static int generateRandomNumber(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    public static void main(String[] args) {
        int[] february = new int[28];
        for (int i = 0; i < february.length; i++) {
            february[i] = generateRandomNumber(-10, 15);
        }

        //1: pentekek: 0., 7., 14., 21. elem
        double averageCelsius = 0;
        int sum = 0;
        for (int i = 0; i < february.length; i += 7) {
            System.out.println(february[i]);
            sum += february[i];
            averageCelsius = ((double) sum) / 4;
        }
        System.out.println("A penteki atlaghomerseklet: " + averageCelsius);

        //2. Februar osszes napjanak atlaga
        int sumValue = 0;
        int count = 0;
        for (int i = 0; i < february.length; i++) {
            sumValue += february[i];
            count++;
        }
        int average = sumValue / count;
        System.out.println("A havi atlaghomerseklet: " + average);

        //3. A hetkoznapok atlaghomerseklete
        //1., 2., 8., 9., 15., 16., 22., 23. elem hetvege
        sumValue = 0;
        count = 0;
        for (int i = 0; i < february.length; i++) {
            //i:       0 1 2 3 4 5 6 7 8 9 10 11 12 13
            //modulus: 0 1 2 3 4 5 6 0 1 2 3  4  5  6
            int modulus = i % 7;
            //1: szombat, 2: vasárnap, mert minden 0. a péntek
            if (modulus != 1 && modulus != 2) {
                sumValue += february[i];
                count++;
            }
        }
        average = sumValue / count;
        System.out.println("A hétköznapok atlaghomerseklete: " + average);

        //4. A maximmum homerseklet
        int maxCelsius = Integer.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < february.length; i++) {
            if (february[i] > maxCelsius) {
                maxCelsius = february[i];
                index = i;
            }
        }

        String str = "";
        switch (index % 7) {
            case 0: 
                str = "péntek";
                break;
            case 1:
                str = "szombat";
                break;
            case 2:
                str = "vasárnap";
                break;
            case 3:
                str = "hétfő";
                break;
            case 4:
                str = "kedd";
                break;
            case 5:
                str = "szerda";
                break;
            case 6:
                str = "csütörtök";
                break;
        }
        System.out.println("A maximum homerseklet: " + maxCelsius + ", napja: " + str);

    }
}
