/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class While {
    public static void main(String[] args) {
        
//        byte b = 0;
//        
//        while (true) {
//            System.out.println(b++); //0-val ter vissza, de a ciklus vegen mar 1 az erteke          
//        }

        int i = 1;
        
        while (i <= 10) {
            System.out.println(i + ". Aliz");
        i = i +1;
        }
        
        //1. feladat
        Scanner sc = new Scanner(System.in);
        
        int j = 0;
        int sum = 0;        //mert igy az elso ertek onmaga lesz, de ha szorzas lenne akkor 1-el inditanank
        while (j < 5) {     //lehet megadni neki pl sum < 200, kkor nem kell a J-t novelni
            int number = sc.nextInt();
            sum = sum + number;
            j++;
        }
        System.out.println("Eredmeny: " + sum);
        
        
        
    }
  
}
