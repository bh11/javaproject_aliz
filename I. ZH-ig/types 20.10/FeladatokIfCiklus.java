/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class FeladatokIfCiklus {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean asking = true;  //addig amig true, folyamatosan bekerunk egy szamot
        int number = -1;        //kell neki kezdoertek, hogyha csak a feltetelben van megadva

        do {
            System.out.println("Kerem adjon meg egy 1 es 5 kozotti szamot.");
            if (sc.hasNextInt()) {
                number = sc.nextInt();

                if (number >= 1 && number <= 5) {
                    asking = false;
                }
            } else {
                sc.next();
            }     //azert hogy mozgassuk a kurzort
        } while (asking);
        if (number == 1) {      //switch-el is lehetne, mert konkret ertekeink vannak
            System.out.println("I.");
        } else if (number == 2) {
            System.out.println("II.");
        } else if (number == 3) {
            System.out.println("III.");
        } else if (number == 4) {
            System.out.println("IV.");
        } else {
            System.out.println("V.");
        }
        //System.out.printf("A megadott szam: %s -> String, %d -> int. Az ennek megfelelo romai szam", number);

        //2. feladat
        char a = 'A';
        while (a <= 'Z') {
            System.out.println(a);
            a++;
        }

        //vagy
        char c = 'A';
        while (c <= 'Z') {
            System.out.println(c++ + " ");
        }

        char k = 'a';
        char l = 'l';
        int s = k + l;      //a muvelet miatt automatikusan int-nek veszi, char-kent nem lehet osszeadni

        //3. feladat
        boolean isRequiredNumber = false;
        int number1 = -1;

        do {
            System.out.println("Kerem adjon meg egy pozitiv egesz szamot.");
            if (sc.hasNextInt()) {
                number = sc.nextInt();
                if (number1 > 0) {
                    isRequiredNumber = true;
                }
            } else {
                sc.nextLine();
            }
        } while (!isRequiredNumber);     //vagy while(number<0); es akkor rovidebb a kod, nincs szukseg booleanra

        //pozitiv osztok szamanak kiiratasa
        int i = 1;
        while (i <= number) {
            if (number % i == 0) {
                System.out.println(i);
            }
            i++;
        }

        //Prim -e?
        int countOfDivider = 0;
        int j = 1;
        while (j <= number) {
            if (number % j == 0) {
                countOfDivider++;
            }
            j++;
        }
        if (countOfDivider == 2) {
            System.out.println("Prim.");
        }

        //4. feladat
        //Oszthatosag
        int number2 = 20;

        while (number2 <= 945) {
            if (i % 3 == 0 && i % 7 != 0) {
                System.out.println(number2);
            }
            i++;
        }

        //5. feladat
        System.out.println("Kerem adjon meg ket egesz szamot.");

        int number3 = sc.nextInt();
        int number4 = sc.nextInt();

        if (number3 > number4) {
            System.out.println((number3 % 10 == 7) + " " + (number4 % 10 == 7));
        } else {
            System.out.println((number4 % 10 == 7) + " " + (number3 % 10 == 7));
        }

        //Extra az oszthatosaghoz:
        boolean isValidInput = false;
        int number5 = -1;

        do {
            System.out.println("Adjon meg egy egesz szamot.");
            if (sc.hasNextInt()) {
                number5 = sc.nextInt();
                isValidInput = true;
            } else {
                sc.next();
            }
        } while (!isValidInput);

        int number6 = sc.nextInt();
        // min-max
        int from = number5 > number6 ? number6 : number5;
        int to = number5 > number6 ? number5 : number6;

        from:
        Math.min(number5, number6);

        System.out.println("----- 7-re vegzodo szamok -----");
        int m = from;
        while (i <= to) {
            if (i % 10 == 7) {
                System.out.println(i);
            }
            i++;
        }
        
        
        
        

    }
}
