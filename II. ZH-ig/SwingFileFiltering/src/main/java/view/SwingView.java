/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.awt.BorderLayout;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class SwingView extends JFrame implements View{

    private ViewController controller;
    
    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField jTextField;

    
    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }

    @Override
    public void update(List<File> files) {  //itt össze kell fűzni a fileokat Stringgé és belerakni a TextAreaba
        jTextArea.setText(files.stream()
                .map(File::getPath)
                .collect(Collectors.joining("\n"))  //joining az elválasztó
                //itt a for ciklussal is össze lehet fűzni a StringBuilder
        );
        
    }

    @Override
    public void enableView() {
        construct();
        
        pack(); //újraszámolja, rendezi a területet
        //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new CloseWindow(() -> this.controller.save()));   //szerializálás
        setVisible(true);
    }
    
    private void construct(){   //felépíti a viewt
        jTextArea = new JTextArea(30, 70);
        jButton = new JButton("GO!");
        jTextField = new JTextField(20);    //20 oszlop széles
        
        JPanel p1 = new JPanel();
        p1.add(jTextField);
        p1.add(jButton);
        
        JPanel p2 = new JPanel();
        p2.add(jTextArea);
        
        //itt a this a Frame
        this.add(p1, BorderLayout.NORTH);
        this.add(p2, BorderLayout.CENTER);   
        
        jButton.addActionListener(e -> controller.handleGoButton(jTextField.getText()));
    }
    
    
    
}
