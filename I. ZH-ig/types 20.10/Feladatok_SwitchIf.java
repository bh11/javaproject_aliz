/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Feladatok_SwitchIf {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //1. feladat
        char c = sc.next().charAt(0);       //beolvasunk egy szot, es vesszuk a nulladik elemet
        if (c <= 'z' && c >= 'a') {
            System.out.println("Kisbetus.");
        }
        if (c <= 'Z' && c >= 'A') {
            System.out.println("Nagybetus.");
        }

        if (c <= '0' && c >= '9') {
            System.out.println("Szam.");
        }

        //2. feladat
        int height = sc.nextInt();

        if (height >= 0 && height <= 200) {
            System.out.println("Alfold.");
        } else if (height >= 201 && height <= 501) {
            System.out.println("Dombsag.");
        } else if (height >= 501 && height <= 1500) {
            System.out.println("Kozephegyseg.");
        } else if (height >= 1501) {
            System.out.println("Hegyseg.");
        }

        //vagy kihasznalni csak a felso vagy also hatarokat
        if (sc.hasNextInt()) {
            height = sc.nextInt();
            if (height < 0) {
                System.out.println("Hibas bemenet.");
            }
            if (height <= 200) {
                System.out.println("Alfold1.");
            } else if (height <= 500) {
                System.out.println("Dombsag2");
            } else if (height <= 1500) {
                System.out.println("Kozephegyseg.3");
            } else if (height > 1500) {
            }
        } else {
            System.out.println("Hegyseg.");
        }

        //3. feladat
//        System.out.println("Kerem adja meg a haromszog oldalainak hosszusagat.");
//        int number1 = sc.nextInt();
//        int number2 = sc.nextInt();
//        int number3 = sc.nextInt();
//        
//         if ((number1 + number2) > number3) {
//            System.out.println("Ez egy haromszog");
//        } else if ((number2 + number3) > number1) {
//            System.out.println("Ez egy haromszog");
//        } else if ((number1 + number3) > number2) {
//            System.out.println("Ez egy haromszog");
//        } else {
//            System.out.println("Ez nem egy haromszog.");
//        }
        int a, b, d;
        a = sc.nextInt();
        b = sc.nextInt();
        d = sc.nextInt();

        boolean abgreaterc = a + b > c;
        boolean acgreaterb = a + c > b;
        boolean bcgreatera = b + c > a;
        boolean isCorrectTriangle = a == b || a == c || b == c;

        System.out.println(abgreaterc && acgreaterb && bcgreatera && isCorrectTriangle);
        System.out.println((a + b > c) && (a + c > b) && (b + c > a) && a == b || a == c || b == c);

    }

}

