/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author Aliz
 */
public class Car extends FourWheelVehicle {

    private String licenseNumber;
    private int year;
    private String manufactureOfCar;
    private String typeOfCar;
    private CarDocuments documents = null;
    public static final int INCREASE_SPEED = 10;
    public static final int DECREASE_SPEED = -10;
    
    //konstruktor
    public Car(CarDocuments documents) {
        this.documents = documents;
    }

    //metodusok
    public void goFaster() {
        int actualSpeed = this.getSpeed();
        int newSpeed = actualSpeed+INCREASE_SPEED;
        this.setSpeed(newSpeed);
    }

    public boolean isStopped() {
        int actualSpeed = this.getSpeed();
        if(actualSpeed == 0){
            return true;
        }
        return false;
    }

    public void goSlower() {
        if (this.isStopped()) {     //sajat metodus hivasa
            return;     //ne csinaljon semmit
        }
        int actualSpeed = this.getSpeed();
        int newSpeed = actualSpeed+DECREASE_SPEED;
        this.setSpeed(newSpeed);
    }

    //setterek, getterek
    public CarDocuments getDocuments() {
        return documents;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    protected void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getYear() {
        return year;
    }

    protected void setYear(int year) {
        this.year = year;
    }

    public String getManufactureOfCar() {
        return manufactureOfCar;
    }

    protected void setManufactureOfCar(String manufactureOfCar) {
        this.manufactureOfCar = manufactureOfCar;
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    protected void setTypeOfCar(String typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

}
