/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class WhilePow {

    public static void main(String[] args) {

        //3. Kérj be egy pozitív egész számot (addig kérjünk be adatot, amíg pozitív számot nem kapunk). 
        //Írd ki 0-tól az adott számig a számok négyzetét. Pl.: bemenet: 4 - kimenet: 0 1 4 9 16
        Scanner sc = new Scanner(System.in);

        int requestedNumber = -1;
        while (requestedNumber <= 0) {
            System.out.println("Kerem adjon meg egy pozitiv egesz szamot.");
            if (sc.hasNextInt()) {
                requestedNumber = sc.nextInt();

                int i = 2;

                double number = 0;
                while (number <= requestedNumber) {
                    double square = Math.pow(number, i);
                    number++;
                    System.out.println("A negyzetek a szamig: " + square);

                }
            }
        }
    }
}
