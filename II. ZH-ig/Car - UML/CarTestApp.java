/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author Aliz
 */
public class CarTestApp {
    public static void main(String[] args) {
        CarDocuments docs = new CarDocuments();
        Car car = new Car(docs);
        
        car.goFaster();
        car.goFaster();
        System.out.println("A kocsi sebessege: "+car.getSpeed());
        car.goSlower();
        System.out.println("A kocsi sebessege: "+car.getSpeed());
        
    }
}
