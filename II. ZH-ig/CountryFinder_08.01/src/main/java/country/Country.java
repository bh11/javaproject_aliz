/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package country;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Country implements Serializable{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private String name;

    public Country(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {        
        return Objects.hash(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Country)) {
            return false;
        }
        final Country other = (Country) obj;
        //Objects visgálja a nullt is
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    
}
