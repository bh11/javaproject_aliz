use hotel;
create table hotel(id varchar(10), name varchar(20), address varchar(100));
insert into hotel values('H111', 'Grosvenor Hotel', 'London');
use hotel;
create table room(id tinyint, hotel_id varchar(10), type varchar(5), price double);
insert into room values(1, 'H111', 'S', 72.00);
create table booking(id varchar(20), hotel_id varchar(20), date_from date, date_to date, room_id tinyint);
insert into booking values('H111', 'G111','1999-01-01', '1999-01-02', 1);
create table guest(id varchar(20), name varchar(20), adress varchar(10));
insert into guest values('G111', 'John Smith', 'London');
create table archive(id varchar(20), hotel_id varchar(20), date_from date, date_to date, room_id tinyint);
insert into archive(id, hotel_id, date_from, date_to, room_id) select booking.id, booking.hotel_id, booking.date_from,
booking.date_to, booking.room_id from booking where date_from or date_to = '2000-01-01';
delete from booking where date_from or date_to = '2000-01-01';
SET SQL_SAFE_UPDATES = 0;
delete from booking where date_from or date_to = '2000-01-01';
select * from ((hotel inner join room on room.hotel_id=hotel.id) inner join booking on booking.id=room.hotel_id)
inner join guest on guest.id=booking.hotel_id;	-- ?
insert into hotel values('H111', 'Four Seasons Hotel', 'London');
insert into hotel values('H122', 'Hilton Hotel', 'Birmingham');
insert into hotel values('H133', 'Sofitel', 'London');
select * from hotel where address = 'London';
insert into booking values('H111', 'G111','2001-01-01', '2001-01-02', 1);
insert into guest values('G111', 'Megan Wellsmither', 'London');
select guest.name, guest.adress from guest where adress='London' order by guest.name asc;
insert into room values(1, 'H111', 'D', 39.00);
insert into room values(1, 'H111', 'F', 37.00);
insert into room values(1, 'H111', 'F', 38.00);
select * from room where type='D' or type='F' having price < 40.00;
insert into booking values('H111', 'G111','2001-01-01', null, 1);
select * from booking where date_to is null;
select count(*) from hotel;
select avg(price) from room;
insert into room values(1, 'H111', 'D', 65.00);
select sum(price) from room where type='D';
insert into booking values('H111', 'G111','2001-08-01', '2001-08-02', 1);
select booking.date_from, booking.date_to, guest.name from booking inner join guest on hotel_id=guest.id 
where date_from between '%-08-01' and '%-08-31' OR date_to between '%-08-01' and '%-08-31';
select count(room.id), hotel.name from room inner join hotel on room.hotel_id=hotel.id;
select count(room.id), hotel.name from room inner join hotel on room.hotel_id=hotel.id where hotel.address='London';
select avg(booking.id), hotel.name from booking inner join hotel on booking.id=hotel.id 
where date_from between '%-08-01' and '%-08-31' OR date_to between '%-08-01' and '%-08-31';
select max(room.type), booking.id from room inner join booking on room.id=booking.room_id;
select sum(room.price) from room;















