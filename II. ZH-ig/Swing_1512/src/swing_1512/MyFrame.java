/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing_1512;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class MyFrame extends JFrame{
    private final JButton jb = new JButton("Click!");
    private final JTextField jt = new JTextField("Write something", 50);  //50 karakter hosszú lesz kinézetre
    
    public MyFrame(){
        buildWindow();
    }
    
    private void buildWindow(){
        JPanel jp = new JPanel();   //flowLayout-ja van
        jp.add(jb);
        jp.add(jt);
        
        jt.setEditable(false);
        
        jb.addActionListener(l -> {jt.setEditable(true);    //ha megnyomjuk a gombot, akkor legyen szerkeszthető
                jt.setText("You can write something.");
            }
        );    
        
        add(jp, BorderLayout.SOUTH);    //ez a this-re vonatkozik, alul legyen a szöveg
        pack();     //újrarendezi az ablakot
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        
        
    }
}
