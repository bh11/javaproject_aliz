/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212;

/**
 *
 * @author Aliz
 */
public class ArrayStoreException {
    
    public static void main(String[] args) {      
        //Azt jelzi, hogy megpróbáltunk nem megfelelő típusú objektumot tárolni egy objektumtömbbe
        
        /**
        MIvel a Double osztály öröklődik Number osztályból
        csak Double típusú érték kerülhet ebbe a tömbbe*/
        Number[] array = new Double[2]; 
  
        // megpróbálunk Float-ot belerakni
        array[0] = new Float(3.4); 
    
    }
}

