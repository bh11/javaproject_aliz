/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Ending11 {

    public static void main(String[] args) {

        //4. Kérj be 2 számot a felhasználótól. A kisebbtől a nagyobbig írd ki a 11-re végződő számokat!
        Scanner sc = new Scanner(System.in);

        System.out.println("Kerem adjon meg ket szamot.");

        int number1 = sc.nextInt();

        int number2 = sc.nextInt();

        int from = number1 > number2 ? number2 : number1;   //a kisebbtol
        int to = number1 > number2 ? number1 : number2;     //a nagyobbig

        from:
        Math.min(number1, number2);

        to:
        Math.max(number2, number1);

        System.out.println("A legkisebb ertek: " + from);
        System.out.println("A legnagyobb ertek: " + to);

        System.out.println("----- A 11-re vegzodo szamok -----");
        int i = 1;
        while (i <= to) {
            if (i % 100 == 11) {
                System.out.println(i);
            }
            i++;
        }

    }
}
