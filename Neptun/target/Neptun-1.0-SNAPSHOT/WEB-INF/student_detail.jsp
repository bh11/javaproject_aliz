<%-- 
    Document   : student_detail
    Created on : 2020. márc. 1., 13:54:13
    Author     : Aliz
--%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student detail</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>Student:</h1>
        <h1>Details:</h1>
         <table class="table table-striped table-dark"> 
              <tr>
                <td>
                  ID:
                </td>
                <td>
                    <c:out value="${student.id}" />
                </td>
            </tr>
            <tr>
                <td>
                  Név:
                </td>
                <td>
                    <c:out value="${student.name}" />
                </td>
            </tr>
             <tr>
                <td>
                  Neptun kód:
                </td>
                <td>
                    <c:out value="${student.neptunCode}" />
                </td>
            </tr>
             <tr>
                <td>
                  Születési dátum:
                </td>
                <td>
                    <c:out value="${student.dateOfBirth}" />
                </td>
            </tr>
            <tr>
                <td>
                  Tárgyak listája:
                </td>
                <td>
                    <c:forEach var = "s" items="${student.subjects}" > 
                    <div>
                        <a class="btn btn-default" href="#" role="button">${s.name}</a>
                     </div>
                    </c:forEach>
                </td>
            </tr>
         </table>
    </body>
</html>
