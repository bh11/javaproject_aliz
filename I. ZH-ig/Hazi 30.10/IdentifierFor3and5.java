/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

/**
 *
 * @author Aliz
 */
public class IdentifierFor3and5 {
    
    /*15. feladat: Készítsünk olyan függvényt, amely meghatározza egy tömbben hány 3-al osztható, 
    de 5-el nem osztható szám van! */
    
    static int valueFinder(int[] arrays){
        int count = 0;
        for (int i = 0; i < arrays.length; i++) {
            if(arrays[i] % 3 == 0 && arrays[i] % 5 !=0){
                count++;
            }           
        }
        return count;
    }
    
    public static void main(String[] args) {
        int array[] = {3, 6, 7, 9, 13, 18};
        int result = valueFinder(array);                    //megkapja a count-ot
        System.out.println("A 3-al oszthato, de 5-tel nem oszthato ertekek darabszama a tombben: "+ result);
    }
}
