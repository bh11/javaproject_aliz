/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

/**
 *
 * @author Aliz
 */
public enum HappinessFactor {
    SAD(0), //ezzel itt bevezetünk egy sorrendet az ordinal helyett
    HAPPY(1),
    NEUTRAL(2);
    
    private final int level;

    private HappinessFactor(int level) {
        this.level = level;
    }
       
    public static HappinessFactor nextLevel(HappinessFactor factor){
        //keressük meg az eggyel nagyobbat
        if(factor == HAPPY) {
            return HAPPY;
        }
        Optional<HappinessFactor> nextLevel = Arrays.stream(HappinessFactor.values())   //a values odaadja az összes értéket egy tömbben
                .sorted((x ,y) -> y.level - x.level)    //növekvő amikor a másodikból vonjuk ki az elsőt
                .filter(f -> f.level < factor.level + 1)
                .findAny();
        return nextLevel.orElse(HAPPY);
    }
}
