/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi0611;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class CelciustoFahrenheit {
    
    static double calculator(double celsius){
        double result = celsius+273.15;
        result = (result*9/5)-459.67;
        
        return result;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Kerem adja meg a Celsius-fokot!");
        double fahrenheit = sc.nextDouble();
        System.out.println("A megadott Celsius-fok Fahrenheitban: "+calculator(fahrenheit));
    }
}
