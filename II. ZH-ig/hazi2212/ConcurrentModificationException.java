/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class ConcurrentModificationException {

    //Például nem engedélyezett, hogy az egyik szál módosítson egy Kollekciót, míg egy másik szál közben iterál rajta.
    public static void main(String[] args) {

        List<String> list = new ArrayList<>(3);
        list.add("kacsa");
        list.add("liba");
        list.add("hattyu");

        for (String element : list) {
            list.remove(0);
        }

    }
}
