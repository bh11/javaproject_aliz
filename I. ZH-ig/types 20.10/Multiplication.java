/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

/**
 *
 * @author Aliz
 */
public class Multiplication {

    public static void main(String[] args) {

//        //10-es
//        int i = 1;
//        while (i < 10) {
//            System.out.printf("10 x %d = %d\n", i, i * 10);
//            i++;
//        }

        //1.....10-es tablak
        int m = 1;
        while (m <= 10) {
            int n = 1;
            while (n <= 10) {           // kulso csak 1-et lep, belso ciklus 10-et, pl. 8 x 1, aztan 8 x 2
                System.out.printf("10 x %d = %d\n", m, n, m * n);
                n++;
            }
            m++;
            System.out.println();
        }
        System.out.println("+");
        
        //while(true);  -- vegtelen ciklus, ha ezutan van valami akkor egyszer se fut le

    }
}
