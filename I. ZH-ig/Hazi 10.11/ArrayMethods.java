/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1011;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ArrayMethods {

    /*Írjon programot (Arrays.java), ami egy felhasználó által bekért nagyságú tömböt hoz létre. Ezt a
tömböt feltölti random elemekkel a következő intervallumban: [-129, 350).
Írja meg a következő metódusokat (angol névvel), és a program futása végén írja ki a felhasználó
számára a tömb elemeit, és a kérdésekre a válaszokat:
 összegzés(): mennyi a tömbelemek összege?
 eldöntés(): van-e a tömbben 17-tel osztható szám?
 kiválasztás(): melyik az első páros elem?
 lineárisKeresés(): van-e a sorozatban 126 (és mi az indexe)?
 megszámlálás(): mennyi a páratlan elemek darabszáma?
 maximumKiválasztás(): melyik a tömb legkisebb eleme (és mi az indexe)?
 rendez(): hogy néz ki a tömb növekvő sorrendben?*/
    static Scanner scanner = new Scanner(System.in);

    static int getInt(Scanner scanner) {

        while (!scanner.hasNextInt()) {
            System.out.println("Ez nem szam.");
            scanner.nextLine();
        }
        return scanner.nextInt();
    }

    static int generateRandomNumber(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    static void fillArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = generateRandomNumber(-129, 350);
        }
    }

    static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("A tomb elemei: " + array[i]);
        }
    }

    static int sumElementsOfArray(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    static boolean dividableBy17(int[] array) {
        boolean dividable = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 17 == 0) {
                dividable = true;
                System.out.println("A 17-el oszthato elem: " + array[i]);
            }
        }
        if (dividable == false) {
            System.out.println("Sajnos nincs 17-el oszthato elem.");
        }
        return dividable;
    }

    static int findFirstEvenNumber(int[] array) {
        int even = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                even = array[i];
            }
        }
        return even;
    }

    static int find126inArray(int[] array) {
        int index = 0;
        int number = 126;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                System.out.println("A keresett szam benne van a tombben.");
                index = array[i];
            }
        }
        if (index == 0) {
            System.out.println("Sajnos a keresett szam nem talalhato.");
        }

        return index;
    }

    static int countOdd(int[] array) {
        int odd = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                odd++;
            }
        }

        if (odd == 0) {
            System.out.println("Sajnos nem talaltam paratlan szamot a tombben.");
        } else {
            System.out.println("A paratlan szamok darabszama: " + odd);
        }
        return odd;
    }

    static int findSmallestElement(int[] array) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        System.out.println("A tomb legkisebb eleme: " + min);
        return min;
    }

    static int[] sortAscending(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j] > array[j] + 1) {      //ha az aktualis elem nagyobb mint a kovetkezo
                    int tmp = array[j];             //tegyuk el ideiglenes valtozoba az aktualis elem erteket
                    array[j] = array[j + 1];        //aktualis elem legyen egyenlo a kovetkezo elem ertekevel
                    array[j + 1] = tmp;             //kovetkezo elem erteke legyen az ideiglenes erteke
                }
            }
        }
        System.out.println("A tomb elemei novekvo sorrendben:");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]+" ");          
        }
        
        return array;
    }

    public static void main(String[] args) {
        System.out.println("Kerem adja meg a tomb meretet.");
        int size = getInt(scanner);
        int[] myArray = new int[size];
        fillArray(myArray);
        printArray(myArray);
        System.out.println("A tomb elemeinek osszege: " + sumElementsOfArray(myArray));
        dividableBy17(myArray);
        System.out.println("Az elso paros szam a tombben: " + findFirstEvenNumber(myArray));
        find126inArray(myArray);
        countOdd(myArray);
        findSmallestElement(myArray);
        sortAscending(myArray);
    }
}
