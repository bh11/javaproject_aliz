/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2711;

/**
 *
 * @author Aliz
 */
public class BirthdayCake extends Cake{
   
    private String birthdayWish;

    //ugyanaz, mint Cake + szulinapi kivansag
    public BirthdayCake(String birthdayWish, String fantasyName, int calories, String[] ingredients) {
        super(fantasyName, calories, ingredients);
        this.birthdayWish = birthdayWish;
        //itt korlátozzuk hogy a cake-nek hány összetevője lehet
        setIngredientsLength(1000);
    }
    

    public String getBirthdayWish() {
        return birthdayWish;
    }

    public void setBirthdayWish(String birthdayWish) {
        this.birthdayWish = birthdayWish;
    }

    
    
    
}
