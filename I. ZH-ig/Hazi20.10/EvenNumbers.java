/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

/**
 *
 * @author Aliz
 */
public class EvenNumbers {
    public static void main(String[] args) {
        
        //6. Írd ki a páros számokat 55 és 88 között. Oldd meg ezt a feladatot while és for felhasználásával is.
        
        int i = 0;
        int number= 55;
        
        while(number >= 55 && number <= 88) {
            System.out.println("A szamok while-al: "+number);
                    number++;
        }  
        for (i = 55; i <= 88; i++) {
            System.out.println("A szamok for-ral: "+i);          
        }
    }
}
