/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import employee.Boss;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import model.Model;
import view.View;

/**
 *
 * @author Aliz
 */
public class MyController implements Controller{

private final View view;
    private final Model model;
    private Boss boss;
    
    /**
     * This methods sets the Controller into View.
     * @param view
     * @param model 
     */
    public MyController(View view, Model model) {
        this.view = view;
        this.model = model;
        this.view.setController(this);
     }

    @Override
    public boolean selectBoss(String name) {
        Boss boss = this.model.getBoss(name);
        if(boss == null) {
            return false;
        }
        //ha sikerült kiválasztani akkor azt állítja be
        this.boss = boss;
        view.showSelectedBoss(name);
        return true;
    }

    @Override
    public void getAllBosses() {
        List<String> listOfBosses = model.getAllBosses().stream()
                .map(b -> b.getName())
                .collect(Collectors.toList());
        view.showBosses(listOfBosses);
    }

    @Override
    public void getWorkers() {
        List<String> list = new ArrayList<>();
        if(this.boss != null) {
            //feltöltjük a listát: a Boss munkásait adjuk át szövegesen
            list.addAll(this.boss.getWorkers().stream()
            .map(w -> w.toString())
            .collect(Collectors.toList()));
        } 
        view.showWorkers(list);
    }

    @Override
    public void addBoss(String name) {
        model.addBoss(name);
        selectBoss(name);
    }

    @Override
    public void addWorker(String name, int age) {
        if(this.boss == null){
            return;
        }
        model.addWorker(this.boss, name, age);
    }

    @Override
    public void save(String fileName) {
        model.save(fileName);
    }

    @Override
    public void load(String fileName) {
        model.load(fileName);
    }
     
}
