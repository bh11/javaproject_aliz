/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing_1512;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class MyFrame2 extends JFrame {

    private final JButton button = new JButton("Convert!");
    private final JTextField from = new JTextField(25);     //új mező
    private final JTextField to = new JTextField(25);
    private final JTextField character = new JTextField(1);

    public MyFrame2() {
        buildWindow();
    }

    //felépítjük a paneleket és hozzárendeljük a labeleket, és a gombot
    private JPanel buildNorth() {
        JLabel fromLabel = new JLabel("FROM: ");
        JPanel northPanel = new JPanel();

        northPanel.add(fromLabel);
        northPanel.add(from);
        northPanel.add(character);

        return northPanel;
    }

    private JPanel buildSouth() {
        JLabel toLabel = new JLabel("TO: ");
        JPanel southPanel = new JPanel();

        southPanel.add(toLabel);
        southPanel.add(to);

        return southPanel;
    }

    private JPanel buildCenter() {
        JPanel centerPanel = new JPanel();
        centerPanel.add(button);

        return centerPanel;
    }

    //hozzáadjuk a felépített elemeket
    private void buildWindow() {
        button.addActionListener(e -> new MyController().copyText(from, to));

        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
        add(buildSouth(), BorderLayout.SOUTH);

        pack();     //újrarendezi az ablakot
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

}


