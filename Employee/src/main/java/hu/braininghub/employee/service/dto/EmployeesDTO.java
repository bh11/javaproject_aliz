/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.service.dto;

import hu.braininghub.employee.repository.JobHistory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 *
 * @author Aliz
 */

public class EmployeesDTO implements Serializable {
//DTO-ból kitöröljük az annotációkat, ez javaBean vagy POJO
    
    //14.02.
    private JobHistory id;
    
    private Date endDate;
    
    private String jobId;
    
    private long departmentId;
    
    
    private static final long serialVersionUID = 1L;

    private Integer employeeId;
 
    private String firstName;

    private String lastName;
   
    private String email;
    
    private String phoneNumber;
    
    private Date hireDate;
    
    private BigDecimal salary;
    
    private BigDecimal commissionPct;
   
    private Collection<EmployeesDTO> employeesCollection;
    
    //private EmployeesDTO managerId;
    
    //14.02.
    private List<JobHistory> jobHistory;    //itt eager a fetch, tehát betölti a hozzátrtozó JobHistoryId-t is

    public List<JobHistory> getJobHistory() {
        return jobHistory;
    }

    public void setJobHistory(List<JobHistory> jobHistory) {
        this.jobHistory = jobHistory;
    }
    
    

    public EmployeesDTO() {
    }

    public EmployeesDTO(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public EmployeesDTO(Integer employeeId, String lastName, String email, Date hireDate, BigDecimal salary) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.email = email;
        this.hireDate = hireDate;
        this.salary = salary;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public BigDecimal getCommissionPct() {
        return commissionPct;
    }

    public void setCommissionPct(BigDecimal commissionPct) {
        this.commissionPct = commissionPct;
    }

    public Collection<EmployeesDTO> getEmployeesCollection() {
        return employeesCollection;
    }

    public void setEmployeesCollection(Collection<EmployeesDTO> employeesCollection) {
        this.employeesCollection = employeesCollection;
    }

    /*public EmployeesDTO getManagerId() {
        return managerId;
    }

    public void setManagerId(EmployeesDTO managerId) {
        this.managerId = managerId;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeId != null ? employeeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeesDTO)) {
            return false;
        }
        EmployeesDTO other = (EmployeesDTO) object;
        if ((this.employeeId == null && other.employeeId != null) || (this.employeeId != null && !this.employeeId.equals(other.employeeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.braininghub.employee.service.dto.EmployeesDTO[ employeeId=" + employeeId + " ]";
    }
    
}
