/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1711.Feladat2;

/**
 *
 * @author Aliz
 */
public class AccountingUnit {
    private String id;
    private String country;
    private Account[] accounts = new Account[10];   //10 szamla egy konyvelesi egysegben
    private int accountIndex = 0;
    
    public AccountingUnit(String country, String id){
        this.setCountry(country);
        this.setId(id);
    }
    
    public void addAccounts(Account account){
        if(accountIndex < 10){
            this.accounts[accountIndex] = account;
            this.accountIndex++;
        }       
    }
    
    public void multiplyAmounts(double indicator){
        for(int i = 0; i < accountIndex; i++){
            Account account = accounts[i];
            int newAmount =(int)(account.getAmount() * indicator);
            account.setAmount(newAmount);           //kapott egy erteket, modositja es beallitja
        }
    }
    
    
    public void printAccounts(){
        for (int i = 0; i < accountIndex; i++) {
            System.out.println(accounts[i].toString());     //szoveget iratunk ki
        }
    }
    

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
   
    
}
