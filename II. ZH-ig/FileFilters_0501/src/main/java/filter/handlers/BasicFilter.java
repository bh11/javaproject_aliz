/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Aliz
 */
public abstract class BasicFilter implements Filter{

    @Override
    public boolean test(File file) {
        if(file.getParent().contains("Downloads")){
            System.out.println("Listing files from Downloads directory is not permitted.");           
            return false;
        }
        return true;
    }

    
    
}
