/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Ship {
    private static final int MAX_SIZE = 4;
    private static final int INDEX_IN_NAME = 1;
    
    private final List<AbstractHero> heroes = new ArrayList<>();
    
    public void add(AbstractHero hero){
        if(isEmptySeat()){
                heroes.add(hero);
    }
        orderBySecondCharacterOfName();
    }
    
    //johet e még hős
    public boolean isEmptySeat(){
        return heroes.size() < MAX_SIZE;
    }

    @Override
    public String toString() {
        return "Ship{" + "heroes=" + heroes + '}';
    }
    
    private void orderBySecondCharacterOfName(){
        /*anonymus osztályt csinálunk és példányosítunk a jobb oldalon
        Comparator<AbstractHero> cmp = new Comparator<AbstractHero>() {
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME);
            }
        };
            
        Collections.sort(heroes, cmp);*/
        //lambda típusú:
        heroes.sort((h1, h2) ->  {
            return h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME);
        });
    }
    
    //2. verzió: egybeolvasztás paraméterként való átadással
    private void orderBySecondCharacterOfName2(){
        Collections.sort(heroes, new Comparator<AbstractHero>() {
            public int compare(AbstractHero h1, AbstractHero h2) {
                return h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME);
            }
        });      
    }
    
    //3. verzió: lambda
    private void orderBySecondCharacterOfName3(){
        Collections.sort(heroes, (AbstractHero h1, AbstractHero h2) -> {
            return h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME);           
        });    
    } 
    
    //4. verzió: lambda, nem kellenek a típusok, return sem kell a void miatt
    private void orderBySecondCharacterOfName4(){
        Collections.sort(heroes, (h1, h2) -> 
            h1.getName().charAt(INDEX_IN_NAME) - h2.getName().charAt(INDEX_IN_NAME));    
    }  

    public List<AbstractHero> getHeroes() {
        return heroes;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.heroes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        return true;
    }
    
    
}
