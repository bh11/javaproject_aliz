/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2911_BankingHub;

import java.time.LocalDate;

/**
 *
 * @author Aliz
 */
public class StudentBankAccount extends BankAccount{
    //diákszámla
    private int yearOfBirth;
    //csak 18 éven aluliak nyithatják

    /**
     * 
     * @param number
     * @param amount
     * @param yearOfBirth 
     */
    public StudentBankAccount(int number, int amount, int yearOfBirth) {
        super(number, amount);
        this.yearOfBirth = yearOfBirth;
        
        if(this.calcAge() > 18){
            throw new IllegalStateException();
        }
    }

    public int calcAge() {
        //a mostani évből kivonjuk a korát, így megkapjuk hogy hány éves
        return LocalDate.now().getYear() - this.yearOfBirth;
    }
    
    

    @Override
    public void monthlyFeeCharge() {
    }

    @Override
    protected int getTransferFee(int amount) {
        return 0;
    }
}
