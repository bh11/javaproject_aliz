/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi0412_Enum;

/**
 *
 * @author Aliz
 */
//Weekday alapján hasonlítjuk össze
public enum Weekday {
   /*has a method which takes a Weekday as the argument and prints a 
    message depending on whether the Weekday is a holiday or not.*/
    
    //a compareTo nem működik a sequence alapján 
    MONDAY(1), 
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);
    
    private final int sequence;
    
    //a napok sorrendje legyen fix
    private Weekday(int sequence){
        this.sequence = sequence;
    }
       
    
    public boolean isWeekday(){
         return !isHoliday();
    }
    
    public boolean isHoliday(){
        return this == SATURDAY || this == SUNDAY;
    }
    
    public static void decideDay(Weekday day){
         //itt a toString a konstans nevét adja vissza
        System.out.print("Ez egy ilyen nap: "+day);
        if(day.isHoliday()){
            System.out.println(", hetvege.");
        } else {
            System.out.println(", hetkoznap.");
        }
    }
    
    /**
     * Le tudjuk kérdezni, hogy egy nap hányadik a héten.
     * @return hányadik
     */
    public int getSequence() {
        return sequence;
    }

}
