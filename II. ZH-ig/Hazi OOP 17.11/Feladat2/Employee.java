/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1711.Feladat2;

/**
 *
 * @author Aliz
 */
public class Employee {
    private String name;
    private String systemId;
    private String country;
    private AccountingUnit[] units = new AccountingUnit[2];
    private static final double RAISE_INDICATOR = 1.1;
    private static final double LOWER_INDICATOR = 0.9;

    
    public Employee (AccountingUnit unit1, AccountingUnit unit2){
        this.units[0] = unit1;
        this.units[1] = unit2;
    }
    
    public void raiseAmountOfAccounts(){
        for(AccountingUnit unit : this.units){
            unit.multiplyAmounts(RAISE_INDICATOR);
        }
    }
    
    public void lowerAmountOfAccounts(){
        for(AccountingUnit unit : this.units){
            unit.multiplyAmounts(LOWER_INDICATOR);
        }
    }
       
    public void printAccountingUnit(){
        System.out.println("Accounts of "+this.getName()+":");
        for (AccountingUnit accountingUnit : this.units) {      //ahany account van a unitban, annyi accountot fogunk kiprintelni
             accountingUnit.printAccounts();
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSystemId() {
        return this.systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public AccountingUnit[] getUnits() {
        return this.units;
    }

    
}
