/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.List;
import model.Log;

/**
 *
 * @author Aliz
 */
public interface View {
    
    public void setController(Controller controller);
    
    public void build();
    
    public void update(List<Log> logs);
    
}
