/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class EvenDivider {

    public static void main(String[] args) {

        //1. Kérj be egy számot, írd ki a páros osztóit!
        Scanner sc = new Scanner(System.in);

        System.out.println("Kerem adjon meg egy egesz szamot.");
        
        while (!sc.hasNextInt()) {
            System.out.println("Ez nem szam.");   
            sc.nextLine();
        } 
        int number = sc.nextInt();

        int i = 1;
        while (i <= number) {
            if (number % i == 0 && i % 2 == 0) {
                System.out.println(i);
            }
            i++;

        }
    }
}

