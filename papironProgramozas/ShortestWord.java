/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class ShortestWord {
    /*2.	Egy paraméterül kapott String listában válasszuk ki a legrövidebb szót.*/
    public static String chooseShortestWord(List<String> list){
        int minLength = Integer.MAX_VALUE;
        String shortest= "";
        Iterator<String> it = list.iterator();
        while(it.hasNext()){
            String s = it.next();
            if(s.length() < minLength){
                minLength = s.length(); //elmentjük a hosszt
                shortest = s;   //elmentjük a szót
            }
            
        }
        return shortest;
    }
    
    public static void main(String[] args) {
        List<String> myList = new ArrayList<>();
        myList = Arrays.asList("eper","alma","mag","korte","szilva", "licsi");
        
        System.out.println(chooseShortestWord(myList));
        
    }
}
