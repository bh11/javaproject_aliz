/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.List;
import java.util.Scanner;
import model.Log;

/**
 *
 * @author Aliz
 */
public class ConsoleView implements View {

    private Controller controller;

    private static final String STOP = "EXIT";

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void build() {
        try (Scanner scanner = new Scanner(System.in)) {
            String line;
            do {
                System.out.println("Please type 'GO' for logging an event.");
                System.out.print("> ");
                line = scanner.nextLine();
                if (line != null) {
                    this.handler(line);
                }
            } while (!STOP.equals(line));
        }
    }

    public void handler(String command) {
        if (command.equals("GO") || command.equals("GO".toLowerCase())) {
            controller.handleButton();
        } else if (command.equals(STOP)) {
            System.out.print("");
        } else {
            System.out.println("Please type 'GO' or 'EXIT'");
        }
    }

    @Override
    public void update(List<Log> logs) {
        logs.forEach(System.out::println);
    }

}
