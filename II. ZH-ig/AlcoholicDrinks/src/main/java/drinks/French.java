/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author Aliz
 */
public class French extends Drink {
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private FrenchState frenchState;

    public French(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price, FrenchState frenchState) {
        super(drinkType, manufacturerer, alcoholVolume, price);
        this.frenchState = frenchState;
    }

    public FrenchState getFrenchState() {
        return frenchState;
    }

    public void setFrenchState(FrenchState frenchState) throws FrenchStateException {
        if (this.frenchState == FrenchState.FROZEN && frenchState == FrenchState.DELIVERABLE) {
            throw new FrenchStateException();
        }
        this.frenchState = frenchState;
    }

}
