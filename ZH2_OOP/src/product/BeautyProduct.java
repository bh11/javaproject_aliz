/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author Aliz
 */
public class BeautyProduct extends AbstractProduct{
    
    private int weight;
    
    public BeautyProduct(String manufacturer, int price, Type type, int weight) {
        super(manufacturer, price, type);
        
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.weight + super.hashCode();  //kiegészítés
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BeautyProduct other = (BeautyProduct) obj;
        //kiegészítés
        if (super.equals(other)) {
            return false;
        }
        //
        if (this.weight != other.weight) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BeautyProduct{" + "weight=" + weight + "} " + super.toString(); //kiegészítés
    }

   
    
}
