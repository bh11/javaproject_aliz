/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_oop;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import product.AbstractProduct;

/**
 *
 * @author Aliz
 */
public class ProductStore {
    
    private final List<AbstractProduct> products = new ArrayList<>();
    
    public void add (AbstractProduct abstractProduct){
        products.add(abstractProduct);
    }
    
    public void remove(int barCode){
        Optional<AbstractProduct> product = products.stream()
                .filter(p -> p.getBarCode().getNumber() == barCode)
                .findAny();
        
        if(product.isPresent()){
            products.remove(product.get()); //a prodct értéke kell az Optional helyett
        }
    }

    public List<AbstractProduct> getProducts() {
        return products;
    }
    
    
}
