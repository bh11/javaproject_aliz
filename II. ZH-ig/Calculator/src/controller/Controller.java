/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Model;
import model.Operator;
import save.Save;
import view.View;

/**
 *
 * @author Aliz
 */
    //1.
public class Controller {

    private View v;
    private Model m;

    public Controller(View v, Model m) {
        this.v = v;
        this.m = m;

        v.setController(this);
        m.setController(this);
    }

    //2. itt hozzáadunk 3 metódust (MVC step 4): setNumber(), handleOperator(), notifyView()
    public void setNumber(int number){
        //System.out.println(number);
        //(MVC step5 a modell után)
        if(m.getOperator() == null) {
            m.setNumber1(number);
        } else {
            m.setNumber2(number);
        }
        
        m.setExpression(generateExpression());
    }
    
    //(MVC step6)
    public void handleOperator(Operator operator){
        //System.out.println(operator);
        if(m.getNumber1() != 0 && operator != Operator.EQ && operator != Operator.MEMORY) {
            m.setOperator(operator);
            m.setExpression(generateExpression());            
        } 
        else if(operator == Operator.MEMORY) {
            Save.save(m.getExpression(), "memory.txt");
        }
        else if (m.getNumber1() != 0 && m.getNumber2() != 0 && m.getOperator() != null && operator == Operator.EQ){
            reset();
        } 
    }
    
    public void notifyView(){
        v.setText(m.getExpression());
    }
    
    //(MVC step6)
    private void reset(){
        m.setExpression(generateExpression() + Operator.EQ + m.getOperator().calc(m.getNumber1(), m.getNumber2()));
        
        m.setNumber1(0);
        m.setNumber2(0);
        m.setOperator(null);
    }
    
    //(MVC step5)
    private String generateExpression(){
        StringBuilder sb = new StringBuilder(String.valueOf(m.getNumber1()));
        
        if(m.getOperator() != null) {
            sb.append(m.getOperator().getCharacter());  
        }
        
        if(m.getOperator() != null && m.getNumber2() != 0) {
            sb.append(m.getNumber2());
        }
        
        return sb.toString();
    }
    
}
