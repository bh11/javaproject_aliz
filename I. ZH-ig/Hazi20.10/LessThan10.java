/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class LessThan10 {

    public static void main(String[] args) {

        //5. Írj programot, mely addig olvas be számokat a billentyűzetről, ameddig azok kisebbek, mint tíz. 
        //Írd ki ezek után a beolvasott számok összegét!
        Scanner sc = new Scanner(System.in);

        System.out.println("Kerem adjon meg 10-nel kisebb szamokat.");

        int number = 0;
        int sum = 0;
        while (true) {
            number = sc.nextInt();
            if (number >= 10) {
                break;
            }
            sum += number;
            number++;

        }
        System.out.println("Az eredmeny " + sum);
    }
}
