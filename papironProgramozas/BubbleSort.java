/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

import java.util.Arrays;

/**
 *
 * @author Aliz
 */
public class BubbleSort {
    /*6.	Implementáld le a buborék rendezést / bináris keresést / gyors rendezést!*/
    
    
    
    //a binarySearch implementációja
    public static int searchBinaryValue(int[] array, int searchedNumber){
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if(array[i] == searchedNumber){
                index = i;
                break;      //azért kell, hogy ne nézze a következő elemet, különben végigmegy
            }          
        }
        return index;
    }
        
    /*Buborékrendezés:
      kulso ciklus arra, hogy egyre rovidebbig menjen
      a resztombok hosszat igazitja
      nem kell ralepnunk az utolso elemre, csak az utolso elottire ezert nem <=
        
      belso ciklus, hogy a megfelelot megtalalja
      a 0. indextol addig setal el amig a kulso index engedi es cserel
      novekvo sorrend
    */
    
    public static void main(String[] args) {
        
        
        int[] testArray = {20, 9, 3, 16, -5, 66};
        for (int i = testArray.length - 1; i >= 0; i--) {   //visszaszamol hogy meddig rendezze
            for (int j = 0; j < i; j++) {                   //mindig egyel kevesebbig nezi
                if (testArray[j] > testArray[j + 1]) {
                    int tmp = testArray[j];  //segédváltozóval megcseréljük a két elemet
                    testArray[j] = testArray[j + 1];
                    testArray[j + 1] = tmp;
                }
            }
        }
        for (int i = 0; i < testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }
        System.out.println("");
        
        //Rendezés beépített metódussal: sort
        int[] testArray2 = {20, 9, 3, 16, -5, 66};
        Arrays.sort(testArray2);
        
        for (int i = 0; i < testArray2.length; i++) {
            System.out.print(testArray2[i]+" ");           
        }
        System.out.println("");
        
        //keresés beépített metódussal: binarySearch
        int i = Arrays.binarySearch(testArray2, 16);
        System.out.println("Beépített binarySearch szerinti index: "+i);   //az indexszamat adja igy vissza
                
        //saját binarySearch
        System.out.println("Saját binarySearch szerinti index: "+searchBinaryValue(testArray2, 16));
        
        
    }
    
}
