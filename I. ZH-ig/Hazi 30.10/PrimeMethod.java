/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class PrimeMethod {

    /*5. feladat: Írj függvényt, amely a paramétereként átvett pozitív, egész számról eldönti, 
    hogy az prím-e! Logikai visszatérése legyen, amely akkor IGAZ, ha a paraméterként átvett szám prím, 
    különben HAMIS.  Írj programot, amely a felhasználótól bekér egy számot, és a fenti függvény segítségével 
    kiírja a képernyőre az annyiadik prímszámot.  */

    

    static boolean isPrime(int number) {
        if(number == 2){
            return true;
        }
        if(number % 2 == 0){
            return false;
        }                                           //Ha a gyokeig nem volt oszto akkor utana sem lesz
        for (int i = 3; i*i < number; i+=2) {       //ugyanaz mint i<Math.sqrt(number), kettesevel nezzuk a paratlanokat
            if (number % i == 0) {
                return false;
            } 

        } 
        return true;
    }

    public static void main(String[] args) {
           Scanner sc = new Scanner(System.in);
           System.out.println("Kerem adjon meg egy szamot.");
           int number = AbsolutMethod.getInt(sc);               //Absolutmethod class-ban definialtam static method
           
           if(isPrime(number)){
               System.out.println("Ez prim szam.");
           }else {
               System.out.println("Ez nem prim szam.");
           }
    }
}
