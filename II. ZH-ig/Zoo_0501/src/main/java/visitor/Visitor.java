/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;

/**
 *
 * @author Aliz
 */
public interface Visitor {
    //annyi metódus kell ahány dolgot meglátogathatunk
    void act(AbstractAnimal animal);
    
    void feed(AbstractAnimal animal);
}
