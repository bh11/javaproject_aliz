/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import java.util.List;

/**
 *
 * @author Aliz
 */
public interface Model {
    
    public void setController(Controller controller);
    
    /**
     * Request Logs.
     * @return list with logs.
     */
    public List<Log> getLogs();
    
    /**
     * Add Log, and notify View.
     * @param log is a representation of an ActionEvent.
     */
    public void add(Log log);
}
