/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

/**
 *
 * @author Aliz
 */
public class StringLastThree {
    /*1.	Egy metódus paraméterül kap egy String tömböt és visszatér az utolsó 3 elemmel. 
    A nem létező elemek null értékűek*/
    
    public static String[] lastThreeChars(String[] array){
        int resultIndex = 2;
        String[] result = new String[3];
        for (int i = array.length-1; i >= 0; i--) { //legutolsó elemtől indul
            if(array[i] != null){
                result[resultIndex] = array[i];     
                if(resultIndex == 0) break;
                resultIndex--;
            }  
        }
       return result; 
    }
    
    public static void main(String[] args) {
        String[] myArray = {"abc", "lapat", "aso", "so", "cukor", null, "banan"};
        String[] myResultArray = lastThreeChars(myArray);

        for (String myResult : myResultArray) {
            System.out.println(myResult);
        }
    }
}
