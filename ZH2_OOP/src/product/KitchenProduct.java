/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import action.LiftUp;
import action.TurnOn;

/**
 *
 * @author Aliz
 */
public class KitchenProduct extends AbstractProduct implements TurnOn, LiftUp{
    
    public KitchenProduct(String manufacturer, int price, Type type) {
        super(manufacturer, price, type);
    }

    @Override
    public void turnOn() {
        System.out.println("The product is turned on.");
    }

    @Override
    public void liftUp() {
        System.out.println("The product is lifted up.");
    }
    
}
