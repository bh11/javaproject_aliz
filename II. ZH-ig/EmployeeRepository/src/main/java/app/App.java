/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import controller.MyController;
import model.Model;
import model.RegistryModel;
import view.ConsoleView;
import view.SwingView;
import view.View;

/**
 *
 * @author Aliz
 */
public class App {
    public static void main(String[] args) {
        View v;
        if(args.length != 0 && args[0].equals("console")){
            v = new ConsoleView();
        }else{
            v = new SwingView();
        }
        Model m = new RegistryModel();
        Controller c = new MyController(v, m);
        
        v.start();
    }
}
