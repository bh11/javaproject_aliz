/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.LocalDateTime;
import java.util.Collections;
import model.Log;
import model.LogStore;
import model.Model;
import view.View;

/**
 *
 * @author Aliz
 */
public class MyController implements Controller{
    //ismeri a modelt és a viewt, Savet
    
    private final Model store;
    private final View view;
    
    private final Saver saver = new Saver();
    
    private int counter = 0;

    public MyController(Model store, View view) {
        this.store = store;
        this.view = view;
        
        //beálítjuk a Controllert
        store.setController(this);
        view.setController(this);      
    }

    @Override
    public void handleButton() {
        Log log = new Log(counter++, "Button pressed.", LocalDateTime.now());
        store.add(log);
        saver.save(Collections.unmodifiableList(store.getLogs()));    //store.getLogs() helyett, ne lehessen módosítani
    }

    @Override
    public void notifyView() {
        view.update(Collections.unmodifiableList(store.getLogs()));     //store.getLogs() helyett, ne lehessen módosítani
    }  
    
}
