/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import filter.handlers.ContainsA;
import filter.handlers.Filter;
import filter.handlers.SizeLimited2MBFilter;
import filter.handlers.SmallLetterConatinedFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Aliz
 */
public class FilterAggregator {
    
    private final List<Filter> filters = new ArrayList<>();
    
    //a konstruktorba beregisztráljuk a filtereket
    public FilterAggregator(){
        filters.add(new SizeLimited2MBFilter());
        filters.add(new SmallLetterConatinedFilter());
        filters.add(new ContainsA());
    }
    
    //a megszűrt fájlok listáját adja vissza
    //az egyik listát filterezni kell
    public List<File> filter(List<File> files){
        return files.stream()
                .filter(this::allFiltersMatched) //f -> allFiltersMatched(f)
                .collect(Collectors.toList());
    }
    
    //Nézzük meg egy fájlra
    private boolean allFiltersMatched(File f){
        for (Filter filter : collectRelevantFilters(f)) {
            if(!filter.filter(f)) {
                return false;
            }
        }
        
        if(collectRelevantFilters(f).size() == 0){
            return false;   //if no filter is enabled (all test return false) it should not allow to list files
        }
        
        return true;
    }
    
    private List<Filter> collectRelevantFilters(File file){ //szükségünk van a fájlra, és csak azokra szűrjünk ami 2 napnál fiatalabb
        return filters.stream()
                .filter(f -> f.test(file))
                .collect(Collectors.toList());
    }
}

