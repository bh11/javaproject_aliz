/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class SwingView extends JFrame implements View{
    
    private Controller controller;

    private final JButton executeButton = new JButton("Execute");
    private final JTextField commandField = new JTextField(25);
    private final JTextArea displayResultArea = new JTextArea();
    private final JScrollPane scroll = new JScrollPane(displayResultArea);

    private static final String DELIMITER = " ";

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void showBosses(List<String> bosses) {
        this.displayResultArea.append("Bosses: \n");
        bosses.stream()
                .forEach(e -> this.displayResultArea.append(e + "\n"));
    }

    @Override
    public void showWorkers(List<String> workers) {
        this.displayResultArea.append("Workers: \n");
        workers.stream()
                .forEach(e -> this.displayResultArea.append(e + "\n"));
    }

    @Override
    public void showSelectedBoss(String name) {
        this.displayResultArea.append("Seleted boss: " + name + "\n");
    }

    @Override
    public void start() {
        this.displayResultArea.setEditable(false);

        //padding, border
        this.displayResultArea.setBorder(BorderFactory.createCompoundBorder(
                this.displayResultArea.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5))
        );

        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(this.commandField, BorderLayout.CENTER);
        northPanel.add(this.executeButton, BorderLayout.EAST);

        //buttonhoz létrehozunk ActionListenert
        executeButton.addActionListener(e -> {
            this.handler(this.commandField.getText());   //beolvassuk a szövegmezőből a parancsot
            this.commandField.setText("");               //töröljük
        });

        add(this.scroll, BorderLayout.CENTER);
        add(northPanel, BorderLayout.NORTH);

        setMinimumSize(new Dimension(500, 400));
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void scrollDown() {
        JScrollBar vertical = this.scroll.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
    }

    private void handler(String command) {
        String[] commands = command.split(DELIMITER);
        /*command példa: 
        select <bossname>
        add boss <name>
        add worker <name> <year>
        save <filename>
        load <filename>
        list boss
        list worker
        help
         */
        switch (commands[0].toLowerCase()) {
            case "select":
                if (!this.checkCommmandMinLength(commands, 2, "select <bossname>")) {
                    break;
                }
                //ha nem sikerült kiválasztania a főnököt
                if (!this.controller.selectBoss(commands[1])) {
                    this.displayResultArea.append("Not found.\n");
                }
                break;

            case "add":
                this.handleAdd(commands);
                break;

            case "save":
                if (!this.checkCommmandMinLength(commands, 2, "save <filename>")) {
                    break;
                }
                this.controller.save(commands[1]);
                break;

            case "load":
                if (!this.checkCommmandMinLength(commands, 2, "load <filename>")) {
                    break;
                }
                this.controller.load(commands[1]);
                break;

            case "list":
                this.handleList(commands);
                break;

            case "help":
            default:
                this.displayResultArea.append("Examples:\n"
                        + "select <bossname>\n"
                        + "add boss <name>\n"
                        + "add worker <name> <year>\n"
                        + "save <filename>\n"
                        + "load <filename>\n"
                        + "list boss\n"
                        + "list worker\n"
                        + "help\n");
                break;
        }
        scrollDown();
    }

    private void handleAdd(String[] commands) {
        if (!this.checkCommmandMinLength(commands, 3, "add boss <name> OR add worker <name> <year>")) {
            return;
        }

        switch (commands[1].toLowerCase()) {
            case "worker":
                if (!this.checkCommmandMinLength(commands, 4, "add worker <name> <year>")) {
                    break;
                }
                this.controller.addWorker(commands[2], Integer.parseInt(commands[3]));
                break;

            case "boss"://boss
                this.controller.addBoss(commands[2]);
                break;

            default:
                //biztosan hibára fog futni
                this.checkCommmandMinLength(commands, Integer.MAX_VALUE, "add boss <name> OR add worker <name> <year>");
                break;
        }
    }

    private void handleList(String[] commands) {
        if (!this.checkCommmandMinLength(commands, 2, "list boss OR list worker")) {
            return;
        }

        switch (commands[1].toLowerCase()) {
            case "worker":
                this.controller.getWorkers();
                break;

            case "boss"://boss
                this.controller.getAllBosses();
                break;

            default:
                //biztosan hibára fog futni
                this.checkCommmandMinLength(commands, Integer.MAX_VALUE, "list boss OR list worker");
                break;
        }
    }

    /**
     * Checks if a given command is correct
     *
     * @param commands
     * @param minLength
     * @param exampleMessage
     * @return true if correct otherwise false
     */
    private boolean checkCommmandMinLength(String[] commands, int minLength, String exampleMessage) {
        if (minLength > commands.length) {
            this.displayResultArea.append("Wrong command! Example: " + exampleMessage + "\n");
            return false;
        }
        return true;
    }

}
