use school;
create table student(nev varchar(45), neptunkod varchar(20), szul_ev Date, student_id int primary key);
insert into student values("Kiss Maria", "EEYTY0", '1992-03-25', 1);
insert into student values("Nagy Gergely", "TZ06RT", '1993-04-09', 2), ("Molnár Timea", "FGH34Z", '1989-09-12', 3);
create table tantargy(subject_id int, nev varchar(45), leiras varchar(200), student_id int, primary key (subject_id), foreign key (student_id) references student(student_id));
insert into tantargy values (110, "Biologia", "Biologiai ismeretek", 1);
insert into tantargy values (111, "Tortenelem", "Tortenelmi ismeretek", 2), (112, "Irodalom", "Irodalmi ismeretek", 2);
insert into tantargy values (113, "Matematika", "Matematikai ismeretek", 3);




