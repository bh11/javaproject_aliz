/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.neptun.servlet;

import hu.braininghub.neptun.dto.StudentDto;
import hu.braininghub.neptun.service.Neptun;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author czirjak
 */
@WebServlet (name = "StudentDetailsServlet", urlPatterns = "/details")
public class StudentDetailsServlet extends HttpServlet{
    
    @Inject
    Neptun neptun;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(req.getParameter("id"));  //DB-ben lévő id van a DTO-ban, visszafele azonosítani tudjuk amikor lenyomjuk a gombot
        
        //id -> service -> dao -> service -> servlet -> jsp
        StudentDto studentDto = neptun.getStudentById(id);
        req.setAttribute("student", studentDto);
        
        
        
        req.getRequestDispatcher("/WEB-INF/student_detail.jsp").forward(req, resp);
    }
}
