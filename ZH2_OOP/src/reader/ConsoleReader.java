/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import handler.CommandHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Aliz
 */
public class ConsoleReader {
    private static final String STOP = "exit";
    
    private CommandHandler handler = new CommandHandler();
    
    public void read() {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            while(true) {
                System.out.println("Please add, remove or report on products.");
                System.out.print("> ");
                String line = br.readLine();
                if(line == null || STOP.equals(line)) {
                    break;
                }
                handler.process(line);
            }
        } catch (IOException ex) {
            System.out.println(ex);
                        
        }
    }
}
