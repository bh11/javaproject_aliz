/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ConsoleReader {
    
    public String read(){ //a try with resources-al a scanner le is lesz zárva
        try(Scanner scanner = new Scanner(System.in)){  
            return scanner.nextLine();
        }
    }
}
