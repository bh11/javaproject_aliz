<%-- 
    Document   : employees
    Created on : 2020. jan. 22., 17:24:33
    Author     : Aliz
--%>

<%@page import="hu.braininghub.Employee"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <% List<hu.braininghub.Employee> employees = (List<Employee>) request.getAttribute("employees"); %>   
        <table> 
            <thead> 
                <tr>   
                    <th>Name</th>
                    <th>Address</th>
                    <th>Salary</th>
                </tr>
            </thead> 
            <tbody>
                <% for (int i = 0; i < employees.size(); i++) {%>
                <tr>
                    <td><%= employees.get(i).getName()%></td>
                    <td><%= employees.get(i).getAddress()%></td>
                    <td><%= employees.get(i).getSalary()%></td>
                </tr>
                <% }%>     
            </tbody>
        </table>
        <h2>Add new employee</h2>
        <form method="post" action="AddEmployeeServlet">
            <fieldset>
                <legend>Add new employee</legend>
                <p>Name:</p><input type="text" name="name" /><br>
                <p>Address:</p><input type="text" name="address" /><br>
                <p>Salary:</p><input type="number" name="salary" /><br>
                <input type="submit" value="Add employee" />
            </fieldset>
        </form>
         <h2>Delete employee</h2>
        <form method="post" action="DeleteEmployeeServlet">
            <fieldset>
                <legend>Add new employee</legend>
                <p>Name:</p><input type="text" name="name" /><br>
                <input type="submit" value="Delete employee" />
            </fieldset>
        </form>
    </body>
</html>
