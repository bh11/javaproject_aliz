/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Worker implements Serializable{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private Boss boss;
    private String name;
    private int age;
    

    protected Worker(Boss boss, String name, int age) {
        this.boss = boss;
        this.name = name;
        this.age = age;
    }
    
    

    @Override
    public int hashCode() {        
        return Objects.hash(this.age, this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Worker)) {
            return false;
        }
        final Worker other = (Worker) obj;
        //Objects visgálja a nullt is
        return this.age == other.age && Objects.equals(this.name, other.name);
    }

    public Boss getBoss() {
        return boss;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.age;
    }
}
