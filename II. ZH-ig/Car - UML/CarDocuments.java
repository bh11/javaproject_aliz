/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author Aliz
 */
public class CarDocuments {
    private String numberOfLicense;
    private String detailsOfCar;
    private String ownerOfData;
    private int validityDate;

    public CarDocuments() {
    }
    
    public CarDocuments(String numberOfLicense, String detailsOfCar, String ownerOfData, int validityDate) {
        this.numberOfLicense = numberOfLicense;
        this.detailsOfCar = detailsOfCar;
        this.ownerOfData = ownerOfData;
        this.validityDate = validityDate;
    }

   
    
    public String getNumberOfLicense() {
        return this.numberOfLicense;
    }

    public void setNumberOfLicense(String numberOfLicense) {
        this.numberOfLicense = numberOfLicense;
    }

    public String getDetailsOfCar() {
        return this.detailsOfCar;
    }

    public void setDetailsOfCar(String detailsOfCar) {
        this.detailsOfCar = detailsOfCar;
    }

    public String getOwnerOfData() {
        return this.ownerOfData;
    }

    public void setOwnerOfData(String ownerOfData) {
        this.ownerOfData = ownerOfData;
    }

    public int getValidityDate() {
        return this.validityDate;
    }

    public void setValidityDate(int validityDate) {
        this.validityDate = validityDate;
    }      
}
