/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2011_Animal;

/**
 *
 * @author Aliz
 */
public class MainApp {

      static int randomNumberGenerator(int max, int min){
        int number = (int)(Math.random()*(max - min)+min);
        return number;
    }
      
    public static void fillAnimalArray(Animal[] animals) {
        for (int i = 0; i < animals.length; i++) {
            int number = randomNumberGenerator(0, 3);
            switch (number) {
                case 0:
                    animals[i] = new Dolphin("uszik");
                    break;
                case 1:
                    animals[i] = new Dog("barna");
                    break;
                default:
                    animals[i] = new Cat(true);
                    break;                   
            }
            
        }
    }

    public static void main(String[] args) {

        
        Cat kitty = new Cat(true);
        kitty.say();

        Dog sparky = new Dog("grey");
        sparky.say();
        System.out.println(sparky);

        Animal jackDolphin = new Dolphin("uszik");
        jackDolphin.say();

        Animal[] animals = new Animal[10];
        fillAnimalArray(animals);

        for (Animal specialAnimal : animals) {
            specialAnimal.say();
        }
    }

}
