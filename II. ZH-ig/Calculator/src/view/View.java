/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.Operator;

/**
 *
 * @author Aliz
 */
    //3.
public class View extends JFrame{
    private Controller controller;
    
    private JTextField jt = new JTextField(30);

    public void setController(Controller controller) {
        this.controller = controller;
    }
    
     public void showWindow() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
         
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
     
     //beállítjuk a kapott szöveget a textfieldbe (MVC step5)
     public void setText(String expr){
         jt.setText(expr);
     }
     
     //2. itt a panelek létrehozása és a showWindow-ba tesszük (MVC step3)
     private JPanel buildNorth(){
         jt.setEditable(false);
         JPanel jp = new JPanel();
         jp.add(jt);
         
         return jp;
     }
     
     private JPanel buildCenter(){
         JPanel jp = new JPanel();
         jp.setLayout(new GridLayout(5, 3));
         
         int value = 0;
         for (int i = 0; i < 10; i++) {
            JButton jb= (new JButton(String.valueOf(value++)));
            jp.add(jb);

            //itt a 2. lépés az esemény hozzáadása
            jb.addActionListener(l -> {
                JButton button = (JButton) l.getSource();  //melyik gombot nyomták meg
                int nr = Integer.parseInt(button.getText());   //ebből kiolvassuk a számot

                controller.setNumber(nr);  //átadjuk controllernek, aki kiírja
            });
         }
         
         addOperators(jp);
         
         return jp;
     }
     
     private void addOperators(JPanel jp){
         JButton plus = new JButton("+");
         JButton multiply = new JButton("*");
         JButton minus = new JButton(Operator.MINUS.getCharacter());
         JButton divide = new JButton(Operator.DIVIDE.getCharacter());
         JButton equals = new JButton("=");
         JButton memory = new JButton(Operator.MEMORY.getCharacter());
         
         //itt is beregisztráljuk az eseményeket
         plus.addActionListener(l -> controller.handleOperator(Operator.ADD));
         multiply.addActionListener(l -> controller.handleOperator(Operator.MULTIPLY));
         equals.addActionListener(l -> controller.handleOperator(Operator.EQ));
         minus.addActionListener(l -> controller.handleOperator(Operator.MINUS));
         divide.addActionListener(l -> controller.handleOperator(Operator.DIVIDE));
         memory.addActionListener(l -> controller.handleOperator(Operator.MEMORY));
         
         jp.add(plus);
         jp.add(multiply);        
         jp.add(minus);
         jp.add(divide);
         jp.add(memory);
         jp.add(equals);
         
     }

}
