/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;
import java.util.regex.Pattern;

/**
 *
 * @author Aliz
 */
public class ContainsA extends BasicFilter{

    @Override
    public boolean filter(File file) {  
        String wholeParentPath = file.getParent(); // e.g.: C:/user/Alice/folder
        if(wholeParentPath == null){    // if no parent e.g.: parent of C:/
            return false;
        }
        String[] temp = wholeParentPath.split(Pattern.quote(file.separator));  // e.g.: ["C:", "user", "Alice"]
  
        return temp[temp.length-1].contains("A");   // Alice contains A (last is the parent)
    }

    //default true test
    
}
