/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

/**
 *
 * @author Aliz
 */
public class ReverseArray {
    
    /*8. feladat Írj függvényt, ami egy tömböt átvesz paraméterként és hátulról indulva kiírja 
    minden második elemét! Ügyelj arra, hogy nehogy túl/alulindexeld a tömböt! */
    
    static void reverse(int array[]){
        for (int i = array.length -2; i >= 0; i-=2) {           //kezdje az utolso elottivel
            
            System.out.print(array[i]+" ");
        }
        
    }
    
    public static void main(String[] args) {
        int array[] = {2, 4, 6, 9, 11, 14, 15, 29, 34, 56};
        reverse(array);
        
    }
}
