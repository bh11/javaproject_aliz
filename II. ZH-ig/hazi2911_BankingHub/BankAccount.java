/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2911_BankingHub;

import static hazi2911_BankingHub.RetailBankAccount.FEE_OF_TRANSFER;

/**
 *
 * @author Aliz
 */
public abstract class BankAccount {
    private int number;
    public static final int OPENING_FEE = 500;
    //mennyi összeg van a számlán
    private int amount;
    //Pénz befizetése ingyenes, a készpénzfelvétel díja és a kártyás kézpénzfelvételével megegyezik

    public BankAccount(int number, int amount) {
        this.number = number;
        this.amount = amount - OPENING_FEE;
    }
    
    /**
     * Monthly fee of account
     */
    public abstract void monthlyFeeCharge();
    
    /**
     * Ad-hoc transfer
     * @param account is the account from were you want to transfer
     * @param amount is the amount which you want to transfer
     * @return actual balance
     */
    public int transfer(BankAccount toAccount, int amount) {
        //from
        this.takeMoney(amount, this.getTransferFee(amount));    
        //to
        toAccount.placeDeposit(amount, 0);
        return this.getAmount();
    }
    
    /**
     * Fee of transfer
     * @param amount is the amount to transfer
     * @return fee of transfer
     */
    protected abstract int getTransferFee(int amount);
    
    /**
     * @param amount initiated amount of deduction
     * @param fee is the fee of deduction: it will be deducted
     * @return actual balance
     * @throws IllegalStateException if amount is smaller than 0.
     */
    protected int takeMoney(int amount, int fee){
        if(amount < 0){
            throw new IllegalStateException();
        }
        this.amount = this.amount - amount - fee;
        
        return this.amount;
    }
    
    /**
     * @param amount amount of deposit to the account
     * @param fee is the fee of deduction: it will be deducted
     * @return actual balance
     * @throws IllegalStateException if amount is smaller than 0.
     */
    protected int placeDeposit(int amount, int fee){
        if(amount < 0){
            throw new IllegalStateException();
        }
        this.amount = this.amount + amount - fee;
        
        return this.amount;
    }

    public int getNumber() {
        return number;
    }

    public int getAmount() {
        return amount;
    }
    
    
}
