/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Aliz
 */
public class Boss implements Serializable{
     //saját UID
    private static final long serialVersionUID = 1L;
    
    private Set<Worker> workers;
    private String name;

    public Boss(String name) {
        //a főnöknek nincs főnöke
        this.name = name;
        this.workers = new HashSet<>();
    }

    //csak a Factory érje el
    /**
     * 
     * @param e
     * @return If this set already contains the element, the call leaves the set unchanged and returns false. 
     */
    protected boolean add(Worker e) {
        return workers.add(e);
    }

    //az unmodifiable azért kell, hogy csak a Factoryval lehessen módosítani a Set-et
    public Set<Worker> getWorkers() {
        return Collections.unmodifiableSet(workers);
    }

   @Override
    public int hashCode() {        
        return Objects.hash(this.name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Boss)) {
            return false;
        }
        final Boss other = (Boss) obj;
        //Objects visgálja a nullt is
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "Boss: " + name;
    }

    public String getName() {
        return name;
    }
}
