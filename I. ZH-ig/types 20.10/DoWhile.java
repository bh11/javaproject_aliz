/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class DoWhile {
    public static void main(String[] args) {
        do  {
            System.out.println("Legalabb egyszer lefut.");
        } while (false);
        
        //2. pelda: Addig kerjunk be, amig 0-at nem ad.
        Scanner sc = new Scanner(System.in);
        int stop_number = 0;
        int number = 0;
        
        do {
            number = sc.nextInt();
            if (number != stop_number) {
            System.out.println("Out: "+number);
            }
        } while (number != 0);
        
        
    }
}
