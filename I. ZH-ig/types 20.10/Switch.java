/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package types;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Switch {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        if (sc.hasNextInt()) {      //olyan tipust olvasunk-e be amit szeretnenk?

            int number = sc.nextInt();

            switch (number) {
                case 5:
                    System.out.println("Otos.");
                    break;
                case 4:
                    //int a = 4;
                    System.out.println("Negyes.");
                    break;
                case 3:
                    System.out.println("Harmas.");
                    //int a = 10;
                    //a++
                    break;
                case 2:
                    System.out.println("Kettes.");
                    break;
                default:
                    System.out.println("Egyes.");
            }
            //2. 
                     switch (number) {
                case 5:  
                case 4:
                    //int a = 4;
                    System.out.println("Megfelelt.");
                    break;
                case 3: //nem tortenik semmi
                case 2:
                default:
                    System.out.println("Nem felelt meg.");
            }


            if (number == 5) {
                System.out.println("Otos.");
            } else if (number == 4) {
                System.out.println("Negyes.");
            } else if (number == 3) {
                System.out.println("Harmas.");
            } else if (number == 2) {
                System.out.println("Kettes.");
            } else {
                System.out.println("Egyes.");
            }
        }

        //Hazi feladat switch-el
        System.out.println("Kerem adjon meg ket szamot.");
        int numberAsked1 = sc.nextInt();
        int numberAsked2 = sc.nextInt();

        System.out.println("Kerem adja meg az elvegzendo muveletet.");
        String operation = sc.next();

        switch (operation) {
            case "0":
                System.out.println("Figyelem, nullaval nem osztunk es nem kezunk maradekot.");
            case "+":
                int sum = numberAsked1 + numberAsked2;
                System.out.println("Az osszeg: " + sum);
                break;
            case "-":
                int minus = numberAsked1 - numberAsked2;
                System.out.println("A kivonas: "+minus);
                break;
            case "*":
                int multiple = numberAsked1 * numberAsked2;
                System.out.println("A szorzas: "+multiple);
                break;
            case "/":
                double division = 1.0 * (numberAsked1 / numberAsked2);
                System.out.println("Az osztas: "+division);
                break;
            case "%":
                int modulus = numberAsked1 % numberAsked2;
                System.out.println("A modulus: "+modulus);
                break;          
            default:
                System.out.println("Ervenytelen.Kerem adjon meg egy muveletet.");

        }
    }
}
