/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import action.TurnOn;
import exception.CantTurnOn;

/**
 *
 * @author Aliz
 */
public class FunProduct extends AbstractProduct implements TurnOn{
    
    private static final int MAX_VALUE_OF_TURN_ON = 5;
    private int counter; 
            
    public FunProduct(String manufacturer, int price, Type type) {
        super(manufacturer, price, type);
    }

    @Override
    public void turnOn() {
        if(counter == MAX_VALUE_OF_TURN_ON){
            throw new CantTurnOn("The product reached the end of life.");
        }
        counter++;
    }

    public int getCounter() {
        return counter;
    }
    
    
}
