/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

/**
 *
 * @author Aliz
 */
public class App {
    public static void main(String[] args) {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.add(new Employee("Krisz", 43, 400000, new Department("Kobanya", "IT")));   //0
        employeeRepository.add(new Employee("István", 42, 520000, new Department("Rozsahegy", "Accounting")));  //1
        employeeRepository.add(new Employee("Lilla", 33, 320000, new Department("Kobanya", "IT")));     //2
        employeeRepository.add(new Employee("Rózsi", 43, 420000, new Department("Kobanya", "IT")));     //3
        employeeRepository.add(new Employee("Józsi", 35, 320000, new Department("Rozsahegy", "IT")));   //4
        
        //szerializálás
        employeeRepository.serializeEmployee();
        employeeRepository.deSerializeEmployee();
        
        //módosítás
        employeeRepository.add(new Employee("Rózsi", 43, 440000, new Department("Kobanya", "IT"))); //3
        System.out.println(employeeRepository.get(3));
        
        System.out.println("");
        //keresés department alapján
        employeeRepository.searchByDepartment(new Department(null, "IT")).stream()
                .forEach(System.out::println);
                
    }  
}
