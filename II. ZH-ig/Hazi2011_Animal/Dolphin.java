/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2011_Animal;

/**
 *
 * @author Aliz
 */
public class Dolphin extends Animal {

    private String favouriteAttribute;
    

    public Dolphin(String favouriteAttribute) {
        this.setFavouriteAttribute(favouriteAttribute);
    }


    @Override
    public void say() {
        System.out.println("Fitty-firitty.");
    }

    public String getFavouriteAttribute() {
        return this.favouriteAttribute;
    }

    public void setFavouriteAttribute(String favouriteAttribute) {
        this.favouriteAttribute = favouriteAttribute;
    }

    @Override
    public String toString() {
        return "Dolphin{" + "favouriteAttribute=" + favouriteAttribute + '}';
    }

}
