/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Aliz
 */
public class EmployeeRepository2 {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository2.class.getName());

    //ez a hely ahova szerializálunk
    public static final String DEFAULT_FILENAME = "employees.data";
    //itt fogjuk tárolni az Employee-kat
    private Set<Employee> store = new TreeSet<Employee>(new EmployeeComparator());

    //belső osztály, azért hogy működjön a szerializáció 
    // -nem lehet lambdát használni a TreeSetnél
    // -ne kelljen módosítani az Employeet a comparator miatt
    public static class EmployeeComparator implements Serializable, Comparator<Employee> {
        
        private static final long serialVersionUID = 1L;
        
        @Override
        public int compare(Employee a, Employee b) {
            if (!a.getName().equals(b.getName())) {
                return a.getName().compareToIgnoreCase(b.getName());    //kis és nagybetűérzéketlen összehasonlítás String
            }
            return Integer.compare(a.getAge(), b.getAge()); //ez csak akkor fut le ha a név ugyanaz
        }
    }

    //konstruktor
    public EmployeeRepository2() {
    }

    /**
     * Add new or exisiting Employee
     *
     * @param emp
     * @return {@code true} if this set did not already contain the specified
     * element
     */
    public boolean add(Employee emp) {
        return store.add(emp);
    }

    /**
     * Remove the employee by name and age.
     *
     * @param name the name of Employee
     * @param age the age of Employee
     */
    public void remove(String name, int age) {
        store.remove(new Employee(name, age, 0, null));
    }

    /**
     * Get the employee by name and age.
     *
     * @param name the name of Employee
     * @param age the age of Employee
     * @return Optional of Employee
     */
    public Optional<Employee> get(String name, int age) {
        Employee searched = new Employee(name, age, 0, null);
        return store.stream()
                .filter(e -> e.equals(searched))
                .findFirst();
    }

    /**
     *
     * @return no. of Employees
     */
    public int count() {
        return store.size();
    }

    /**
     * save
     */
    public void serializeEmployee() {
        try (FileOutputStream fs = new FileOutputStream(DEFAULT_FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(store);          //kiiratjuk a store-t
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex); //logoljuk az exceptiont
        } catch (IOException exIO) {
            LOGGER.log(Level.SEVERE, null, exIO);
        }
    }

    /**
     * read and load
     */
    public void deSerializeEmployee() {
        try (FileInputStream fi = new FileInputStream(DEFAULT_FILENAME);
                ObjectInputStream oi = new ObjectInputStream(fi)) {
            store = (Set<Employee>) oi.readObject();   //beolvassuk a Set-et
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, null, io);
        } catch (ClassNotFoundException cnf) {
            LOGGER.log(Level.SEVERE, null, cnf);
        }
    }

    /**
     * Search which Employees belong to a department
     *
     * @param department by String adress and name
     * @return set of Employees
     */
    public Set<Employee> searchByDepartment(Department department) {
        return this.store.stream()
                .filter(emp -> emp.getDepartment()
                .equals(department))
                .collect(Collectors.toSet());
    }

}
