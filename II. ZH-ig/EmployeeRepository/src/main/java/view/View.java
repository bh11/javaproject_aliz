/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.List;

/**
 *
 * @author Aliz
 */
public interface View {
    public void setController(Controller controller);
    
    public void showBosses(List<String> bosses);
    
    public void showWorkers(List<String> workers);
    
    public void showSelectedBoss(String name);
    
    public void start();
}
