/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2510;

/**
 *
 * @author Aliz
 */
public class FirstArray {

    public static void main(String[] args) {

        /*1. tomb, 50 elem, [-100, 100] értékkészlet, írjuk ki a következőket: 
            - maximum hely és érték,
            - minimum hely és érték, 
            - átlag 
            - összeg*/
        
        //A tomb feltoltese az elemmekkel
        int[] array = new int[50];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 200 - 100);
            System.out.println(array[i] + " ,");
        }
       
        //A max ertek keresese   
        int maxValue = array[0];
        int maxIndex = 0;
        for (int j = 0; j < array.length; j++) {
            if (maxValue < array[j]) {
                maxValue = array[j];
                maxIndex = j;
            }
        }
        System.out.println("A max ertek: " + maxValue + " Helye: " + maxIndex);
        
        //A min ertek keresese
        int minValue = array[0];
        int minIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if(minValue > array[i]) {
                minValue = array[i];
                minIndex = i;
            }           
        }
        System.out.println("A min ertek: " + minValue + " Helye: " + minIndex);
        
        //Osszeg es atlag
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];    
        }
        System.out.println("Osszes elem osszege: "+sum);
        double average = (double)sum/array.length;
        System.out.println("Osszes elem atlaga: "+average);

    }

}
