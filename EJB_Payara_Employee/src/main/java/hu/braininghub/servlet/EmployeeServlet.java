/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.servlet;

import hu.braininghub.service.EmployeeService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import hu.braininghub.repository.datatransferobject.Employee;

/**
 *
 * @author Aliz
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"/EmployeeServlet"})
public class EmployeeServlet extends HttpServlet {

    @Inject
    private EmployeeService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            List<String> names = service.getNames();           
            request.setAttribute("names", names);           
            request.getRequestDispatcher("/WEB-INF/empnames.jsp").forward(request, response);
    }
    
     @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Employee> partOfNameList = service.getEmployees(request.getParameter("partOfName"));
        
        request.setAttribute("employees", partOfNameList);   //ez kell a jsp-hez
        request.getRequestDispatcher("WEB-INF/empfilter.jsp").forward(request, response);
    }
}
