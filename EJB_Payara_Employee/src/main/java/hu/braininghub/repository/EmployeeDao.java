/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.repository;

import hu.braininghub.repository.datatransferobject.Employee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.sql.DataSource;

/**
 *
 * @author Aliz
 */
@Singleton
public class EmployeeDao {
    
    @Resource(lookup = "jdbc/bh11")
    private DataSource ds;
    
    @PostConstruct
    public void init(){
        System.out.println("It has been initialized.");
    }
    
    @PreDestroy
    public void destroy(){
        System.out.println("It has been destroyed.");
    }
    
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();

        try (Statement stm = ds.getConnection().createStatement()) {
            String sql = "SELECT first_name, last_name, salary FROM employees LIMIT 10";

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                employees.add(Employee.of(
                        rs.getString("first_name"), 
                        rs.getString("last_name"), 
                        rs.getInt("salary")
                ));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
      return employees;
   }

    public List<Employee> findByName(String str) {
        List<Employee> employees = new ArrayList<>();
        String sql = "SELECT first_name, last_name, salary FROM employees where first_name like ?";
        
        try (PreparedStatement pstm = ds.getConnection().prepareStatement(sql)) {
            
            pstm.setString(1, "%" + str + "%");
           
            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                employees.add(Employee.of(
                        rs.getString("first_name"), 
                        rs.getString("last_name"), 
                        rs.getInt("salary")
                ));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return employees;
    }
}
