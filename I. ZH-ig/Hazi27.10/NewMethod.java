/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2710;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class NewMethod {
    /*5. Hf
Írj olyan metódust, ami paraméterül kap egy stringet és egy karaktert, majd visszatér egy számmal. 
A visszatérési érték azt jelenti, hogy az adott karakter hányszor fordul elő a tömbben. 
*/
    
    
    static int method(String string, char character){
        int counter = 0;
        for (int i = 0; i < string.length(); i++) {
            if(character == string.charAt(i)){
            counter++;
            }
            
        }
            
        return counter;
    }
    
    public static void main(String[] args) {
        
        String text = "Ez egy proba text.";
        char test = 'e';
        
        System.out.println("Eredmeny: "+method(text, test));
    }
}
        