/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.department;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
public class DeleteDepartmentServlet extends HttpServlet {

    DepartmentService departmentService = new DepartmentService();
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        DepartmentService.deleteDepartmentByName(request.getParameter("name"));
        request.setAttribute("departments", DepartmentService.getDepartments());
        request.getRequestDispatcher("departments.jsp").forward(request, response);
    }

    

}
