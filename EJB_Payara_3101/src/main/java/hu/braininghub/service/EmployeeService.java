/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.service;

import hu.braininghub.repository.EmployeeDao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Aliz
 */
@Stateless
public class EmployeeService {
//olyan mint egy controller
    
    @Inject
    private EmployeeDao dao;
    
    public List<String> getNames(){
        try {
            return dao.getNames();  //visszamennek a nevek
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            
            return new ArrayList<>();   //üres lista
        }
    }
    
    public List<String> searchEmployee(String name){
        List<String> emptyList = new ArrayList<>();
        
        try {
            return dao.getNames().stream()
                    .filter(e -> e.toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        return emptyList;    
    }
}
