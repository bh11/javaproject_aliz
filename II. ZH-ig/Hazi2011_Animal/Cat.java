/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2011_Animal;

/**
 *
 * @author Aliz
 */
public class Cat extends Animal {
    private boolean domestic;

    public Cat(boolean domestic) {
        this.setDomestic(domestic);
    }
    
    
    @Override
    public void say(){
        System.out.println("Mijau.");
    }

    public boolean isDomestic() {
        return this.domestic;
    }

    protected void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

    @Override
    public String toString() {
        return "Cat{" + "domestic=" + domestic + '}';
    }
    
    
    
}
