/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import drinks.Drink;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aliz
 */
public class SaveLoad {
    private static final Logger LOGGER = Logger.getLogger(SaveLoad.class.getName());
    
    public static final String DEFAULT_FILENAME = "drinks.txt";

    private String filename;
    
    public SaveLoad() {
        this.filename = DEFAULT_FILENAME;
    }
    
    /**
     * @param filename Custom file name
     */
    public SaveLoad(String filename) {
        this.filename = filename;
    }
    
    
     /**
     * save - csak akkor mentsük el ha bor
     */
    public void saveDrink(List<Drink> listOfDrinks) {
        try (FileOutputStream fs = new FileOutputStream(this.filename);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(listOfDrinks);
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);            
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, null, io);
        }
    }

    /**
     * load
     */
    public List<Drink> loadDrink() {
        try (FileInputStream fs = new FileInputStream(this.filename);
                ObjectInputStream ou = new ObjectInputStream(fs)) {
           return (List<Drink>) ou.readObject();
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);          
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);           
        } catch (ClassNotFoundException ex) {
            //automatikusan javaolja a Netbeans a try után
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();   //ha nem sikerült betölteni, akkor üres listát ad
    }
    
}
