/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1011;

/**
 *
 * @author Aliz
 */
public class GenerateBigger {

    /*Feladat: generaljunk ugy 100 szamot hogy amit generalunk az mindig nagyobb legyen, mint 
    amit korabban generaltunk.
    Egy megoldas: tobbszor generaljunk, a generalt szam +1 kozott van a kovetkezo*/
    public static void main(String[] args) {
        int min = 0;
        int max = 5;

        for (int i = 0; i < 100; i++) {
            int number = (int) (Math.random() * (max - min) + min);
            min = max;
            max += 5;
            System.out.print(number + " ");
        }

    }
}
