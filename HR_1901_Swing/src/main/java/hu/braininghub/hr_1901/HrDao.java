/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.hr_1901;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aliz
 */
public class HrDao {

    private static final String URL = "jdbc:mysql://localhost:3306/hr?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "Aliz";
    private static String PW = "Cdinnye592";

    /*public HrDao(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    //-	Adjuk meg a legtöbbet kereső dolgozó nevét!
    public List<String> findEmployeeWithMaxSalary() {
        String sql = "SELECT * \n"
                + "FROM hr.employees e \n"
                + "WHERE e.salary = (SELECT max(i.salary) FROM hr.employees i)";
        List<String> listOfSalaries = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                listOfSalaries.add((rs.getString("first_name") + " " + rs.getString("last_name")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfSalaries;
    }

    //saját
    //-	Adjuk meg annak a department-nek a nevét, ahol a legtöbben dolgoznak!
    public List<String> mostPopulatedDepartment() {
        List<String> listOfDepartments = new ArrayList<>();
        String sql1
                = "SELECT d.department_name \n"
                + "FROM hr.departments d,"
                + "(SELECT e.department_id id, count(*) nr \n"
                + "        FROM hr.employees e \n"
                + "        GROUP BY e.department_id \n"
                + "        ORDER BY nr DESC \n"
                + "        LIMIT 1) i \n"
                + "WHERE d.department_id = i.id";

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {
            ResultSet rs1 = stm.executeQuery(sql1);

            while (rs1.next()) {
                listOfDepartments.add((rs1.getString("department_name")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfDepartments;
    }

    public List<String> findEmployeeByIdUnsafe(String id) {
        List<String> listOfId = new ArrayList<>();
        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.employee_id = " + id;

        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                Statement stm = connection.createStatement()) {

            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {
                listOfId.add((rs.getString("first_name") + " " + rs.getString("last_name")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfId;
    }

    public List<String> findEmployeeByFirstName(String name) {
        List<String> listOfFirstName = new ArrayList<>();
        String sql = "SELECT * \n"
                + "FROM employees e \n"
                + "WHERE e.first_name = ?";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                listOfFirstName.add((rs.getString("first_name") + " " + rs.getString("last_name")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfFirstName;
    }

    //-	Listázzuk ki azokat a dolgozókat, akik többet keresnek az átlagnál!
    public List<String> findEmployeeEarnsMoreThanAvg() {
        List<String> moreThanAvgList = new ArrayList<>();
        String sql = "SELECT * \n"
                + "FROM hr.employees e \n"
                + "WHERE e.salary > (SELECT avg(i.salary) FROM hr.employees i)";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                moreThanAvgList.add((rs.getString("first_name") + " " + rs.getString("last_name")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return moreThanAvgList;
    }

    //-	Kik léptek be 1990. január 1. után?
    public List<String> freshEmployees() {
        List<String> listOfFresh = new ArrayList<>();
        String sql = "SELECT * \n"
                + "FROM hr.employees e \n"
                + "WHERE e.hire_date > '1990.01.01'";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                listOfFresh.add((rs.getString("first_name") + " " + rs.getString("last_name")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfFresh;
    }

    //-	Listázzuk ki azokat a title attribútumokat, amelyekben szerepel az „clerk” szó. 
    //Az eredményhalmaz legyen rendezett, és minden eredményt csak egyszer tartalmazzon!
    public List<String> titleContainsClerk() {
        List<String> listOfClerks = new ArrayList<>();
        String sql = "SELECT DISTINCT j.job_title title \n"
                + "FROM hr.jobs j \n"
                + "WHERE j.job_title \n"
                + "LIKE '%Clerk%' \n"
                + "ORDER BY title";
        try (Connection connection = DriverManager.getConnection(URL, USER, PW);
                PreparedStatement stm = connection.prepareStatement(sql)) {

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                listOfClerks.add((rs.getString("title")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HrDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listOfClerks;
    }
}
