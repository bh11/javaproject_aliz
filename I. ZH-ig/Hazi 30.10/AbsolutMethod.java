/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class AbsolutMethod {
    
    /*6. feladat. Írj függvényt, amely paraméterként átvesz két egész számot és visszaadja azt, 
    amelyiknek az abszolút értéke nagyobb! */
    
    static int biggerNumber(int a, int b){
        if(Math.abs(a) > Math.abs(b)){           
            return a;
        } else {           
            return b;
        }
    }
    
    static int getInt(Scanner scanner){
        
        while(!scanner.hasNextInt()){                   //addig keri be amig nem szam
            System.out.println("Ez nem szam.");
            scanner.nextLine();
        }
        return scanner.nextInt();                       //ugy ter vissza, hogy bekeri a szamot
    }
    
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Kerem adjon meg ket egesz szamot, mely lehet negativ is.");
     
            int a = getInt(sc);
            int b = getInt(sc);
            System.out.println("Az kovetkezo szam abszolut erteke nagyobb :"+biggerNumber(a,b));
                
    }
}
