/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PotZH1;

/**
 *
 * @author Aliz
 */
public class Tomb_2b {

    /*2. Egy metódus paraméterül kap egy tömböt. Írjuk ki azokat az elemeket,
amelyeknek mindkét szomszédja kisebb, mint maga a szám.*/
    static void bothNeighboursSmaller(int[] array) {

        for (int i = 1; i < array.length - 1 - 1; i++) {
            int leftNeighbour = array[i - 1];
            int rightNeighbour = array[i + 1];

            if (array[i] > leftNeighbour && array[i] > rightNeighbour) {
                System.out.println(array[i]);
            }
        }
    }

    /*3. Az előző feladat eredményei közül melyik a legnagyobb? 
Add meg a számok átlagát is.*/
    public static int[] countNeighboursSmaller(int[] array) {
        int count = 0;
        int[] temporaryArray = new int[array.length];

        for (int i = 1; i < array.length - 1 - 1; i++) {    //kihagyjuk az elsot es az utolsot
            int leftNeighbour = array[i - 1];
            int rightNeighbour = array[i + 1];

            if (array[i] > leftNeighbour && array[i] > rightNeighbour) {
                System.out.println(array[i]);
                temporaryArray[count] = array[i];       //pl. nulladik eleme legyen array[i] erteke
                count++;
            }
        }

        int[] newArray = new int[count];

        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = temporaryArray[i];
        }
        return newArray;
    }

    public static void maxandAverage(int[] array) {
        int sum = 0;
        double average = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            if (array[i] > max) {
                max = array[i];
            }
        }
        average = ((double) sum) / array.length;
        System.out.print("A kivalasztott elemek kozul a legnagyobb: " + max);
        System.out.println("");
        System.out.print("A kivalasztott elemek atlaga: " + average);
        System.out.println("");
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 1, 2, 5, 6, 8, 9, 7, 8, 9, 3, 4, 5, 6, 1, 2, 3};
        //bothNeighboursSmaller(array);
        int[] newArray = countNeighboursSmaller(array);
        maxandAverage(newArray);

    }
}
