/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Aliz
 */
public class AgeLimitedFilter extends BasicFilter{
    private static final String USER_HOME = System.getProperty("user.home");  //user home directory ha létezik

    @Override
    public boolean filter(File file) {
        //milliszekundumot napra váltani
        return convertMilliSecondtoDay(file.lastModified()) < 2D ;  //2 napnál fiatalabb
    }
    private double convertMilliSecondtoDay(long value){
        return value / 1000D / 3600D / 24D;
    }

    @Override
    public boolean test(File file) {  
            if(super.test(file) == false){
                return false;
            }
            return file.getPath().contains(USER_HOME);
    }
    
    
}
