/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Aliz
 */
public class ConsoleReader {
    private LineParser parser = new LineParser();
    private static final int MAX_NUMBER_OF_LINES= 100;
    
     public void read(){
        int lineCounter = 0;
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
            while(lineCounter< MAX_NUMBER_OF_LINES) {             
                String line = br.readLine();
                if(line == null) {
                    break;
                }
                parser.parser(line);
                lineCounter++;
            }
        } catch (IOException ex) {
            System.out.println(ex);
                        //itt kiírjuk
        }       
        
    }   
}
