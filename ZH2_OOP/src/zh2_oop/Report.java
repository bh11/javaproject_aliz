/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh2_oop;

import java.util.List;
import product.AbstractProduct;
import product.FunProduct;

/**
 *
 * @author Aliz
 */
public class Report {
    
    public void generateReports(List<AbstractProduct> products){
        numberOfStoredItems(products);
        numberOfAvailableItems(products);      
    }
    
    //Hány termékünk van?
    private void numberOfStoredItems(List<AbstractProduct> products){
        System.out.println("Number of stored items: " + products.size());
    }
    
    //Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?
    private void numberOfAvailableItems(List<AbstractProduct> products){
        long counter = products.stream()
                .filter(p -> p instanceof FunProduct)   //így megkapjuk a AbstractProduct Fun termékeket
                .map(e -> (FunProduct)e)    //kasztolás Fun termékre
                .filter(f -> f.getCounter() < 4)    //még legalább 2-t bír az 5 bekapcsolásig
                .count();
        
        System.out.println("Number of available products: " + counter);
    }
}
