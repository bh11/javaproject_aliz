/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Aliz
 */
public class AgeLimitedFilter implements Filter{
    private static final String USER_HOME = System.getProperty("user.home");  //user home directory ha létezik

    @Override
    public boolean filter(File file) {
        //milliszekundumot napra váltani
        return file.lastModified() / 1000D / 3600D / 24D <2D;  //2 napnál fiatalabb
    }

    @Override
    public boolean test(File file) {      
            return file.getPath().contains(USER_HOME);
    }
}
