/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter.handlers;

import java.io.File;

/**
 *
 * @author Aliz
 */
public class SizeLimited2MBFilter extends BasicFilter{

    @Override
    public boolean filter(File file) {
        //mivel byteot kapunk length-ből ezért el kell osztani hogy MB legyen
        //ez a kifejezés kisebb-e mint 2
        return convertToMegabyteFromByte(file.length()) < 2D;
    }
    
    private double convertToMegabyteFromByte(long value){
        return value / 1024D / 1024D;
    }

    //default true test
    
}
