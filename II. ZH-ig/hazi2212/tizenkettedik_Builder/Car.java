/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212.tizenkettedik_Builder;

/**
 *
 * @author Aliz
 */
public class Car {
    private String plate;
    private final int id;
    private String color;
    private int speed;

    public Car(int id){
        this.id = id;
    }
    public Car(String plate, int id, String color, int speed) {
        this.plate = plate;
        this.id = id;
        this.color = color;
        this.speed = speed;
    }

    public String getPlate() {
        return plate;
    }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Car{" + "plate=" + plate + ", id=" + id + ", color=" + color + ", speed=" + speed +  '}';
    }

    
}
