/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class CapitalLettersMethod {
    
    /*9. feladat: Írj egy függvényt, ami a paraméterként átvett kis betűnek megfelelő nagybetűt adja vissza!*/
    
    static char CreateCapital(char a){
        char capital = Character.toUpperCase(a);       
        return capital;
    }
            
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "cp1250");
        System.out.println("Kerlek adj meg egy kisbetut.");
        char small = sc.next().charAt(0);
        System.out.println("A nagybetu megfeleloje ennek a kisbetunek: "+CreateCapital(small));
    }
}
