/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firstprogram;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Hazi1810 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

//1. feladat
        int number1 = sc.nextInt();
        int number2 = sc.nextInt();
        int number3 = sc.nextInt();

        double average = (double)(number1 + number2 + number3) / 3;
        System.out.println("Atlag: "+average);

        if (number1 > number2 && number1 > number3) {
            System.out.println("Legnagyobb szam: "+number1);
        } else if (number2 > number1 && number2 > number3) {
            System.out.println("Legnagyobb szam: "+number2);
        } else {
            System.out.println("Legnagyobb szam: "+number3);
        }

        if ((number1 + number2) > number3) {
            System.out.println("Ez egy haromszog");
        } else if ((number2 + number3) > number1) {
            System.out.println("Ez egy haromszog");
        } else if ((number1 + number3) > number2) {
            System.out.println("Ez egy haromszog");
        } else {
            System.out.println("Ez nem egy haromszog.");
        }

//2. feladat
        System.out.println("Kerem adjon meg ket szamot.");
        int numberAsked1 = sc.nextInt();
        int numberAsked2 = sc.nextInt();

        System.out.println("Kerem adja meg az elvegzendo muveletet.");
        String operation = sc.next();

        if (operation.equals("+")) {
            int sum = numberAsked1 + numberAsked2;
            System.out.println("Az osszeg: "+sum);
        } else if (operation.equals("-")) {
            int minus = numberAsked1 - numberAsked2;
            System.out.println("A kivonas: "+minus);
        } else if (operation.equals("*")) {
            int multiple = numberAsked1 * numberAsked2;
            System.out.println("A szorzas: "+multiple);
        } else if (operation.equals("/")) {       
            if (number2 == 0) {
                System.out.println("Nullaval nem osztunk.");
            } else if (number2 != 0){
                double division = 1.0 * (numberAsked1 / numberAsked2);
                System.out.println("Az osztas: "+division);
            }
        } else if (operation.equals("%")) {
            int modulus = numberAsked1 % numberAsked2;
            if (number1 == 0 || number2 == 0) {
                System.out.println("Nullaval nem kepzunk maradekot.");
            } else {
                System.out.println("A modulus: "+modulus);
            }
        } else {
            System.out.println("Ervenytelen.Kerem adjon meg egy muveletet.");
        }
    }
}
