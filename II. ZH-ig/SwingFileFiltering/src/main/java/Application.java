
import controller.Controller;
import controller.FileController;
import model.FileStore;
import model.Model;
import view.ConsoleView;
import view.SwingView;
import view.View;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aliz
 */
public class Application {
    public static void main(String[] args) {
        
        View v;
        if(args.length != 0 && args[0].equals("console")){
            v = new ConsoleView();
        }else{
            v = new SwingView();
        }
        Model m = new FileStore();
        
        Controller c = new FileController(v, m);    
        
        v.enableView();
    }
}
