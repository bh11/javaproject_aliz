/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi0611;

/**
 *
 * @author Aliz
 */
public class RecursiveSumNumbers {
    
    /*eredeti megoldas
    static int sumNumbers(int num){
        int res = 0;
        
        while(num>0){
            res += num % 10;        //utolso szamjegyet adja vissza
            num /= 10;              //ha megvan, akkor levalasztjuk
        }
        return res;
    }*/
    
    //rekurziv megoldas
    static int sumNumbersRecursive(int number){        
        if(number > 0){
            int modulus = number % 10;                  //maradek 1 szamjegyet eltaroljuk
            int sum = sumNumbersRecursive(number/10);  //levalasztas  
            return sum+modulus;
        } 
        return 0;
    }
    
    public static void main(String[] args) {
        System.out.println("Eredmeny: "+sumNumbersRecursive(257));
        
    }
}
