/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.birthday.service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;

/**
 *
 * @author Aliz
 */
@Stateless
public class MyService {

    @PostConstruct
    public void init() {
        System.out.println("This is the beginning");
    }

    public String printBirthday(String input) {
        if (input == null || input.length() < 2) {
            return "";
        }
        return input.substring(input.length() - 2);
    }

    @PreDestroy
    public void destroy() {
        System.out.println("This is the end.");
    }

}
