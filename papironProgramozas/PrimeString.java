/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

/**
 *
 * @author Aliz
 */
public class PrimeString {
    /*7.	Egy számokat tartalmazó Stringben add meg, hány prímszám van!*/
    //karakterenként nézi, hogy prím szám-e
    public static int countOfNumbersInString(String numbersIncluded){
        int count = 0;
        for (char characters : numbersIncluded.toCharArray()) {
            int number = Integer.parseInt(String.valueOf(characters));  //a karakterek átalaktása számmá
            if(isPrime(number)){
                count++;
            }
        }
        return count;
    }
    
     static boolean isPrime(int number) {
        if(number == 1){
            return false;
        }
        if(number == 2){
            return true;
        }
        if(number % 2 == 0){
            return false;
        }                                           //Ha a gyokeig nem volt oszto akkor utana sem lesz
        for (int i = 3; i*i <= number; i+=2) {      //ugyanaz mint i<Math.sqrt(number), kettesevel nezzuk a paratlanokat
            if (number % i == 0) {
                return false;
            } 

        } 
        return true;
    }
     
     public static void main(String[] args) {
        String myStringCintainsOnlyNumbers = "12345678910";
         System.out.println(countOfNumbersInString(myStringCintainsOnlyNumbers));
    }
}
