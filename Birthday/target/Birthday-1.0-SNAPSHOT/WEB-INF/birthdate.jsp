<%-- 
    Document   : birthdate
    Created on : 2020. márc. 29., 9:20:11
    Author     : Aliz
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Birthday page</title>
        <jsp:include page="/WEB-INF/html_head.jsp"/>
    </head>
    <body>
        <h1>Birthday</h1>
        <form method="GET">
            <div class="form-group col-md-2">
                <label for="name">Your birthday:</label>
                <input type="text" class="form-control" id="birthDate" name="birthDate">
                <button type="submit" class="btn btn-outline-dark">Send</button>
            </div>
            <div class="card">
                <div class="card-body">
                    <p class="card-text">The last 2 digits of your birthday: <c:out value="${birthday}" /></p>
                </div>
            </div>                
        </form>
    </body>
</html>
