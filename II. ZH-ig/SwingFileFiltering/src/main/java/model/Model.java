/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ModelController;
import java.io.File;
import java.util.List;

/**
 *
 * @author Aliz
 */
public interface Model {
    //a kapcsolatot tudjuk beállítani
    void setController(ModelController controller);
    
    List<File> getFiles();  //a fájlok elérést biztosítja
    
    void setFiles(List<File> files);
    
    void saveFiles();
}
