/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;

import stone.StoneType;

/**
 *
 * @author Aliz
 */
public abstract class AbstractHero {
    
    private final String name;
    private final int power;
    private StoneType stone;        //ne legyen final, ha setter-t akarunk adni neki
    
    public AbstractHero(String name, int power, StoneType stone){
        this.name = name;
        this.power = power;
        this.stone = stone;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public StoneType getStone() {
        return stone;
    }
    public void setStone(StoneType stone){
        this.stone = stone;
    }

    @Override
    public String toString() {
        return "AbstractHero{" + "name=" + name + ", power=" + power + ", stone=" + stone + '}';
    }
    
    
    
    
}
