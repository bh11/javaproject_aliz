/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import country.Country;
import java.util.Set;

/**
 *
 * @author Aliz
 */
public interface Model {
    
    public void setController(Controller controller);
    
    public Set<Country> getAllCountries();
    
    public void load(String fileName);
    
    public void filterCountries(String name);
}
