/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class Save {
    //HF 15.12
    //átalakítja a hősöket String-é és menti egy szöveges fájlba
    
   
   public static void save(List<AbstractHero> heroes, String fileName){
       try (FileWriter fw = new FileWriter(fileName);
             PrintWriter pw = new PrintWriter(fw);) //kiírjuk egy fájlba
             
        {
            for (AbstractHero hero : heroes) {
                pw.println(hero);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
   }
   
   
   
}
