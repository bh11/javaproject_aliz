/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi0412_Enum;

/**
 *
 * @author Aliz
 */
public class App {
    
    /*We suggest that the main method loops over all values in the Weekday enum and sends them as argument to the method.*/
           
    public static void main(String[] args) {
        Weekday sat = Weekday.SATURDAY;
        //végigiterálunk rajta és kiírjuk, hogy hétköznap vagy hétvége     
        for (Weekday weekday : Weekday.values()) {
            Weekday.decideDay(weekday);
            //összehasonlítjuk a szombatot az összes nappal
            System.out.println(weekday.compareTo(sat));
        }
        
        
        
    }
}
