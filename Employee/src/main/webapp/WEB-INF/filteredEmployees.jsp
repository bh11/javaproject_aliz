<%-- 
    Document   : filteredEmployees
    Created on : 2020. febr. 5., 19:22:52
    Author     : czirjak
--%>
<%@page import="hu.braininghub.employee.service.dto.EmployeesDTO"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Filtered Employees!</h1>

        <table>
            <%
                List<EmployeesDTO> employees = (List<EmployeesDTO>) request.getAttribute("employees");

                for (EmployeesDTO e : employees) {
                    out.print("<tr><td>" 
                            + e.getFirstName() + "</td><td>"
                            + e.getLastName() + "</td><td>"
                            + e.getJobHistory().size() + "</td></tr>");
                }
            %>
        </table>
    </body>
</html>
