/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author Aliz
 */
public class German extends Drink{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private Label label;
    //be kell állítani
    public German(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price, Label label) {
        super(drinkType, manufacturerer, alcoholVolume, price);
        this.label = label;
    }

    public Label getLabel() {
        return label;
    }
    
}
