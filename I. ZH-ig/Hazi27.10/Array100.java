/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2710;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Array100 {

    public static void main(String[] args) {
        /*2. Hf (5. feladat:)
Olvassunk be egész számokat 0 végjelig egy maximum 100 elemű tömbbe (a tömböt 100 eleműre
deklaráljuk, de csak az elejéből használjunk annyi elemet, amennyit a felhasználó a nulla végjelig beír).
- Írjuk ki a számokat a beolvasás sorrendjében.
- Írjuk ki az elemek közül a legkisebbet és a legnagyobbat, tömbindexükkel együtt.
- Írjuk ki az elemeket fordított sorrendben.
         */
        Scanner sc = new Scanner(System.in);

        int[] array = new int[100];
        int counter = 0;

        System.out.println("Kerem adja meg a tomb elemeit!");
        while (true) {
            if (!sc.hasNextInt()) {
                System.out.println("Nem jo.");
                sc.nextLine();
                continue;                           //atugorjuk az utana levo kodsort, folytatjuk a ciklust
            }
            int temp = sc.nextInt();
            if (temp == 0) {                        //kilepunk a whilebol ha a temp 0
                break;
            }
            array[counter] = temp;
            counter++;
        }
        
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int minIndex = 0;
        int maxIndex = 0;
        
        System.out.println("A tomb elemei: ");
        for (int j = 0; j < counter; j++) {           //addig megyunk amig a counter index tart            
            System.out.print(array[j]+" ");
            if(array[j]< min) {
                min = array[j];                       //az indexen levo ertek
                minIndex = j;                         //index eltarolasa
                
            }
            if (array[j]> max) {
                max = array[j];
                maxIndex = j; 
            }
        }
        System.out.println("");
        
        System.out.println("Minimum ertek: "+min);
        System.out.println("A minimum ertek helye: "+minIndex);
        System.out.println("Maxmimum ertek: "+max);
        System.out.println("A maximum ertek helye: "+maxIndex);
        
        System.out.println("A tomb elemei: ");
        for (int i = counter - 1; i >= 0; i--) {            
            System.out.print(array[i]+" ");    
        }
        System.out.println("");
    }
}
