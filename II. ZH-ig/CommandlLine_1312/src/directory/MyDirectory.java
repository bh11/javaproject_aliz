/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directory;

import java.io.File;
import java.io.IOException;
import parser.Commands;

/**
 *
 * @author Aliz
 */
public class MyDirectory {
    
    //working directory
    private File file = new File(System.getProperty("user.dir"));
    
    /**
     * Print working directory: get Pathname
     */
    public void pwd(){
        try {
            System.out.println(file.getCanonicalPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
    /**
     * Show files or directories
     */
    public void ls(){
        File[] files = file.listFiles();
        for (File f : files) {
            System.out.print(f.getName());
            System.out.println(f.isFile() ? " F " + f.length() : " D");     //file vagy directory
        }
    }
    
    /**
     * Step into another directory
     * @param command according to this: .. to parent, or . to another directory
     */
    public void cd(String command) {
        if(Commands.PARENT_DIRECTORY.equals(command)){  //ha ez egy parent
            cdParent();
        } else {
            cdDirectory(command);
        }
    }
    
    /**
     * 
     * @param from is the old File name. Example: new.txt
     * @param to is the new File name. Example: new2.txt
     */
    public void mv(String from, String to) {  //mit mire akarunk átnevezni
        File oldFile = new File(file, from);
        File newFile = new File(file, to);        
        oldFile.renameTo(newFile);
    }
    
    /**
     * Get path of Parent, helper of method cd
     */
    private void cdParent() {
        file = file.getParentFile();
    }
    
    /**
     * Helper of method cd
     * @param directory is the chosen directory to go to
     */
    private void cdDirectory(String directory) {
        File to = new File(file, directory);  //konstruktorában meg tudunk adni egy új fájlt és valamit amit hozzáfűzhetünk
        
        if(to.exists() && to.isDirectory()){
            file = to;
        }
    }
}
