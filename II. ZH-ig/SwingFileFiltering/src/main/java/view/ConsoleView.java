/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ViewController;
import java.io.File;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ConsoleView implements View{

    private ViewController controller;
    
    private static final String STOP = "exit";
    
    @Override
    public void setController(ViewController controller) {
        this.controller = controller;
    }


    @Override
    public void enableView() {
        try (Scanner scanner = new Scanner(System.in)) {        
            String line;                                        
            do{
                System.out.println("Please paste your directory below:");
                System.out.print("> ");
                line = scanner.nextLine();
                if(line != null){
                    this.controller.handleGoButton(line);
                }
            } while (!STOP.equals(line));
        }
    }
    
    @Override
    public void update(List<File> files) {
        files.forEach(System.out::println);
    }
    
}
