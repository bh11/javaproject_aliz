/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.service;

import hu.braininghub.employee.repository.EmployeeDAO;
import hu.braininghub.employee.repository.entity.Employees;
import hu.braininghub.employee.service.dto.EmployeesDTO;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.beanutils.BeanUtils;

@Stateless
public class EmployeeService {

    @Inject
    private EmployeeDAO dao;

    public List<String> getNames() {
            return dao.getEmployees().stream()
                    .map(Employees::getFirstName)
                    .collect(Collectors.toList());
    }

    //itt nem adjuk ki az entityt (Employees), hanem DTO-t hozunk létre
    public List<EmployeesDTO> getEmployees(String str) {
        List<EmployeesDTO> result = new ArrayList<>();
        
        for (Employees e : dao.findByName(str)) {
            EmployeesDTO dto = new EmployeesDTO();
            
            try {
                BeanUtils.copyProperties(dto, e);   //ez van a beanutils függőség miatt
                result.add(dto);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            return result;
    }

    //mapper, ehelyett van a beanUtils
    private EmployeesDTO convert(Employees e){
        EmployeesDTO dto = new EmployeesDTO();
        dto.setEmail(e.getEmail());
        //...stb
        
        return dto;
    }

    
}
