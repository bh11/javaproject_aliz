/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Aliz
 */
public class CloseWindow extends WindowAdapter {
//házi: szerializáljon amikor bezárjuk az ablakot SwingViewban
    private final Runnable runnable;
    
    public CloseWindow(Runnable runnable) {
        this.runnable = runnable;
    }
    
    @Override
    public void windowClosing(WindowEvent event){
       this.runnable.run();
       System.exit(0);
    }

    
}
