/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.hr_1901;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ConsoleReader {

    private static final String STOP = "Exit";

    HrDao hrDao = new HrDao();

    public void start() {
        try (Scanner scanner = new Scanner(System.in)) {
            String line;
            do {
                line = scanner.nextLine();
                switch (line) {
                    case "1":
                        hrDao.findEmployeeWithMaxSalary();
                        break;
                    case "2":
                        hrDao.mostPopulatedDepartment();
                        break;
                    case "3":
                        System.out.println("Please give me an ID:");
                        String id = scanner.nextLine();
                        hrDao.findEmployeeByIdUnsafe(id);
                        break;
                    case "4":
                        System.out.println("Please give me the fist name of the employee:");
                        String name = scanner.nextLine();
                        hrDao.findEmployeeByFirstName(name);
                        break;
                    case "5":
                        hrDao.findEmployeeEarnsMoreThanAvg();
                        break;
                    case "6":
                        hrDao.freshEmployees();
                        break;
                    case "7":
                        hrDao.titleContainsClerk();
                        break;
                }
            } while (!STOP.equalsIgnoreCase(line));
        }
    }
}
