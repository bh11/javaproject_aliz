/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

/**
 *
 * @author Aliz
 */
public class App2 {
    public static void main(String[] args) {
        EmployeeRepository2 employeeRepository = new EmployeeRepository2();
        employeeRepository.add(new Employee("Krisz", 43, 400000, new Department("Kobanya", "IT")));   
        employeeRepository.add(new Employee("István", 42, 520000, new Department("Rozsahegy", "Accounting")));  
        employeeRepository.add(new Employee("Lilla", 33, 320000, new Department("Kobanya", "IT")));     
        employeeRepository.add(new Employee("Rózsi", 43, 420000, new Department("Kobanya", "IT")));     
        employeeRepository.add(new Employee("Józsi", 35, 320000, new Department("Rozsahegy", "IT")));   
        
        //szerializálás
        employeeRepository.serializeEmployee();
        employeeRepository.deSerializeEmployee();
        
        //módosítás
        employeeRepository.add(new Employee("Rózsi", 43, 440000, new Department("Kobanya", "IT"))); 
        System.out.println(employeeRepository.get("Rózsi", 43));
        employeeRepository.remove("Lilla", 33);
        
        System.out.println("Az IT-n dolgozók: ");
        //keresés department alapján
        employeeRepository.searchByDepartment(new Department(null, "IT")).stream()
                .forEach(System.out::println);
                
    }  
}
