/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Consumer;

/**
 *
 * @author Aliz
 */
public class Bag<T> implements Iterable<T>{

    private final ArrayList<T> arrayList = new ArrayList<>();

    public Iterator<T> iterator() {
        return arrayList.iterator();
    }

    public void forEach(Consumer<? super T> action) {
        arrayList.forEach(action);
    }

    public boolean add(T element) {
        return arrayList.add(element);
    }

    public int indexOf(T element) {
        return arrayList.indexOf(element);
    }

    public T get(int index) {
        return arrayList.get(index);
    }

    public T remove(int index) {
        return arrayList.remove(index);
    }

    /**
     * 
     * @param element is an instance of Type
     * @return true when element is contained
     */
    public boolean remove(T element) {
        return arrayList.remove(element);
    }
    
    public static void main(String[] args) {
        Bag<String> bag = new Bag<>();
        bag.add("alma");
        bag.add("korte");
        bag.add("korte");
        for (String element : bag) {
            System.out.println(element);
        }
    }
    
}
