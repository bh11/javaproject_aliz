/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.department;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class DepartmentService {
    
    private static List<Department> departments = new ArrayList<Department>();
    
    public static List<Department> getDepartments(){
        return departments;
    }
    
    public static void addDepartment(Department department){
        departments.add(department);
    }
    
    public static void deleteDepartmentByName(String name){
        int index = getIndexByName(name);
        if(index != -1){
            departments.remove(index);
        }
    }
    
     private static int getIndexByName(String name) {
        for (int i = 0; i < departments.size(); i++) {
            if (name.equals(departments.get(i).getName())) {
                return i;
            }
        }
        return -1;
    }
}
