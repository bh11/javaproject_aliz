/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import java.util.ArrayList;
import java.util.List;
import visitor.Visitor;

/**
 *
 * @author Aliz
 */
public class Zoo {
    
    private final List<Cage> cages;
    private final List<Visitor> visitors;
    private final Report report;

    public Zoo() {
        this.cages = new ArrayList<>();
        this.visitors = new ArrayList<>();
        this.report = new Report(this);
    }
    
    public void addCage(Cage cage){
        cages.add(cage);
    }
    
    public void addVisitor(Visitor visitor){
        visitors.add(visitor);
    }
    
    public void addAnimal(AbstractAnimal abstractAnimal){
        report.findCageForAnimalToPlace(abstractAnimal).add(abstractAnimal);
    }
    
    public void removeAnimal(AbstractAnimal abstractAnimal){
        report.findCageOfAnimalBelongs(abstractAnimal).remove(abstractAnimal);
    }

    public List<Cage> getCages() {
        return cages;
    }

    public List<Visitor> getVisitors() {
        return visitors;
    }

    public Report getReport() {
        return report;
    }
    
    
}
