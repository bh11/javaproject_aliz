/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2911_BankingHub;

/**
 *
 * @author Aliz
 */
public class RetailBankAccount extends BankAccount{
    //lakossági folyószámla
    
    public static int MONTHLY_FEE = 1500;
    public static int MONTHLY_FEE_DISCOUNT = 500;
    //ez: ha a számlán az adott hónap végén több mint 250.000 Ft-nyi összeg van, kedvezményesen csak 500 Ft
    public static double FEE_OF_TRANSFER = 0.05;
    
    //Átutalási díj: az utalt összeg 5%-a

    public RetailBankAccount(int number, int amount) {
        super(number, amount);
    }

    @Override
    public void monthlyFeeCharge() {
        if(getAmount() >= 250_000){
            this.takeMoney(0, MONTHLY_FEE_DISCOUNT);
        } else {
            this.takeMoney(0, MONTHLY_FEE);
        }
    }

    @Override
    protected int getTransferFee(int amount) {
        return (int) (amount * FEE_OF_TRANSFER);
    }
    


}
