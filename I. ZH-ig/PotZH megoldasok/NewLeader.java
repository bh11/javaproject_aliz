/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zh1pot;

import java.util.Arrays;


/**
 *
 * @author Aliz
 */
public class NewLeader {
        static int[] findTheLeaders(int[] array) {
        int[] tempArray = new int[array.length];
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            boolean goodNumber = true;
            for (int j = i + 1; j < array.length - 1; j++) {
                if (array[i] < array[j]) {
                    goodNumber = false;
                    break;
                }
            }
            //csak akkor rakjuk bele, ha i-edik helyen nagyobb érték van mint a j.-edik elemek
            //counter: az uj tömbbe sorban belerakjuk ha volt találat
            if(goodNumber){
                tempArray[counter] = array[i];
                counter++;
            }
        }
 
       
        /*int[] resultArray = new int[counter];
            for (int i = 0; i < counter; i++) {
                resultArray[i] = tempArray[i];
            }
            return resultArray;*/
        
        return Arrays.copyOf(tempArray, counter);
    }

    public static void main(String[] args) {
        int[] myArray = {3, 1, 2, 0, 40, 1, 2, 3, 4, 5, 6, 3, 1, 2, 0};
        int[] newArray = findTheLeaders(myArray);

        for (int i = 0; i < newArray.length; i++) {
            System.out.println(newArray[i]);
        }

    }
}
