/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1711.Feladat2;

/**
 *
 * @author Aliz
 */
public class App {
    
    
    //metodus
    public static void fillAccountingUnit(AccountingUnit accountingUnit, int max, int multiplier){
        //max jelenti hogy mennyi accountja van
        for (int i = 1; i <= max; i++) {
            int amount = multiplier * i;
            
            String uniqueNumber = Double.toString(Math.random());
            
            Account newAccount = new Account(amount, uniqueNumber);     //osszeg es sorszam
            accountingUnit.addAccounts(newAccount);
        }
    }
    
    
    public static void main(String[] args) {
        
        AccountingUnit asia = new AccountingUnit("Asia", "A123");
        fillAccountingUnit(asia, 8, 1052);
        
        AccountingUnit europe = new AccountingUnit("Europe", "B345");
        fillAccountingUnit(europe, 10, 125);
        
        AccountingUnit australia = new AccountingUnit("Australia", "C456");
        fillAccountingUnit(australia, 5, 10);
        
        AccountingUnit america = new AccountingUnit("America", "A478");
        fillAccountingUnit(america, 9, 10099);
                  
        
        Employee employee1 = new Employee(europe, asia);
        employee1.setName("Simon Daves");
        employee1.printAccountingUnit();
        employee1.raiseAmountOfAccounts();
        employee1.printAccountingUnit();
        
        Employee employee2 = new Employee(america, australia);
        employee2.setName("Aliz Molnar");
        employee2.printAccountingUnit();
        employee2.lowerAmountOfAccounts();
        employee2.printAccountingUnit();
        
        
        
    }
}
