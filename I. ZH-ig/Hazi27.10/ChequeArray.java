/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2710;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ChequeArray {

    public static void main(String[] args) {

        /*Egy busztársaság szeretné tudni, a napok mely óráiban hány ellenőrt érdemes terepmunkára küldenie.
Ehhez összesítik a bírságok adatait. Egy adat a következőkből áll: „óra perc típus összeg”, ahol a típusnál
h a helyszíni bírság, c pedig a csekk. „9 45 c 6000” jelentése: egy utas 9:45-kor csekket kapott 6 000
forintról. A helyszíni bírságokat az ellenőrök begyűjtik; a csekkes bírságoknak átlagosan 80%-át tudják
behajtani. (Vagyis egy 6 000-es csekk a társaság számára csak 4 800-at ér.) Az adatsor végén 0 0 x 0
szerepel.
Olvassa be a C programod ezeket az adatokat! Készítsen kimutatást arról, hogy mely napszakban mennyi
a pótdíjakból a bevétel! Példakimenet:
16:00-16:59, 14800 Ft
17:00-17:59, 12000 Ft
         */
        Scanner sc = new Scanner(System.in);

        int hour = -1;
        int min = -1;
        char charge = 'a';
        int amount = 0;

        int[][] array = new int[24][1440];

        do {
            System.out.println("Kerem adja meg az óra időpontot először óra, majd a perc megadásával.");
            if (sc.hasNextInt()) {
            hour = sc.nextInt();
            
                min = sc.nextInt();

                System.out.println("Kerem adja meg a bírság összegét");
                amount = sc.nextInt();

                System.out.println("Kerem adja meg, hogy 'c' csekk, vagy 'h' helyszíni bírság.");
                charge = sc.next().charAt(0);
                if (charge == 'c') {
                    amount *= 0.8;
                    System.out.println("A csekken fizetendo osszeg: " + amount);
                } else if (charge == 'h') {
                    System.out.println("A helyi bírság szerinti teljes összeg: " + amount);
                } else {
                    System.out.println("Ervenytelen bemenet.");
                }
            } else {
                System.out.println("Ervenytelen bemenet.");

            }
            if (hour >= 0 && hour < 24) {
                array[hour][min] = amount;
            }
        } while (hour != 0 || min != 0 || charge != 'x' || amount != 0);

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i][j] != 0) {
                    System.out.printf("%d:00 - %d:59 --> %d Ft\n", i, j, array[i][j]);
                }
            }
        }
    }
}
