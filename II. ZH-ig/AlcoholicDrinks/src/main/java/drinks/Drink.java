/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public abstract class Drink implements Serializable{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private DrinkType drinkType;
    private final String manufacturerer;
    private final int barcode;
    private int alcoholVolume;
    private int price;

    public Drink(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price) {
        this.drinkType = drinkType;
        this.manufacturerer = manufacturerer;
        this.alcoholVolume = alcoholVolume;
        this.price = price;
        this.barcode = (int)(Math.random() * 99999-10000) + 10000;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public int getAlcoholVolume() {
        return alcoholVolume;
    }

    public int getPrice() {
        return price;
    }

    public String getManufacturerer() {
        return manufacturerer;
    }

    public int getBarcode() {
        return barcode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.barcode);
    }

    @Override
    public boolean equals(Object obj) {
               if (this == obj) {
            return true;
        }
        if (!(obj instanceof Drink)) {
            return false;
        }
        final Drink other = (Drink) obj;
        //Objects visgálja a nullt is
        return Objects.equals(this.barcode, other.barcode);
    }
    
    
}
