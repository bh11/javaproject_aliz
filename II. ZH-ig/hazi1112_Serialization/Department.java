/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Department implements Serializable{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private String address;
    private String name;

    //generált konstruktor
    public Department(String address, String name) {
        this.address = address;
        this.name = name;
    }
    
    //generált setterek, getterek

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    //generált toString

    @Override
    public String toString() {
        return "Department{" + "address=" + address + ", name=" + name + '}';
    }
    
    //generált hashCode és Equals 
    //csak név alapján legyen ugyanaz a Department
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Department other = (Department) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
   
    
}
