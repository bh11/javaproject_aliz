/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212.tizenharmadik_UML;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Z {
    
    protected List<P> p = new ArrayList<>();
    //az alábbi már implementáció
    public Z(P... p){
        Objects.requireNonNull(p);
        
        if(p.length < 5) {
            throw new IllegalArgumentException();
        }
        this.p.addAll(Arrays.asList(p));       
    }
}
