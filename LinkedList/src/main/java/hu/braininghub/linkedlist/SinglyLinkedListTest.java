/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.linkedlist;

import hu.braininghub.linkedlist.SinglyLinkedList;

/**
 *
 * @author Aliz
 */
public class SinglyLinkedListTest {
    
    public static void main(String[] args) {
        
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.append("BH");
        list.append("11");
        
        System.out.println("Content of list: " + list);
        System.out.println("Length of list: " + list.length());
        System.out.println("This list is empty: " + list.isEmpty());
        
    }
}
