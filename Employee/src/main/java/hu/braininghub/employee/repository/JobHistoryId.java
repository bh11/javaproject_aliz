/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.repository;

import hu.braininghub.employee.repository.entity.Employees;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Aliz
 */
@Embeddable
public class JobHistoryId implements Serializable {

    @Column(name = "employee_id")
    private long EmployeeId;
    
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    
    //14.02
    @ManyToOne(fetch = FetchType.LAZY)  //lazy alapon fogja megtalálni az Employee-t
    @JoinColumn(name = "employee_id")
    private Employees employees;

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }
    
    

    public long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(long EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (this.EmployeeId ^ (this.EmployeeId >>> 32));
        hash = 59 * hash + Objects.hashCode(this.startDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JobHistoryId other = (JobHistoryId) obj;
        if (this.EmployeeId != other.EmployeeId) {
            return false;
        }
        if (!Objects.equals(this.startDate, other.startDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JobHistoryId{" + "EmployeeId=" + EmployeeId + ", startDate=" + startDate + '}';
    }
    
}
