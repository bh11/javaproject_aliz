/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.ModelController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aliz
 */
public class FileStore implements Model, Serializable{
    private static final Logger LOGGER = Logger.getLogger(FileStore.class.getName());
     
    private ModelController controller;  //statikus része a változónak
    private List<File> files = new ArrayList<>();
    
    public static final String DEFAULT_FILENAME = "files.txt";

    @Override
    public void setController(ModelController controller) {
        this.controller = controller;
    }
    

    @Override
    public List<File> getFiles() {
        //a lista nem módosítható változatát adjuk oda, vagy a listát adjuk oda és később lehet beletenni elemeket?
        //itt most csak a files listát adjuk vissza
        return files;
    }

    @Override
    public void setFiles(List<File> files) {
        this.files = files;
        
        controller.notifyView();
    }

    /**
     * save
     */
    public void saveFiles() {
        try (FileOutputStream fs = new FileOutputStream(DEFAULT_FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(files);
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);            
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, null, io);
        }
    }

    /**
     * load
     */
    public void loadFiles() {
        try (FileInputStream fs = new FileInputStream(DEFAULT_FILENAME);
                ObjectInputStream ou = new ObjectInputStream(fs)) {
            files = (List<File>) ou.readObject();
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);          
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);           
        } catch (ClassNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }
    
}
