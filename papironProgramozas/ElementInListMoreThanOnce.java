/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class ElementInListMoreThanOnce {
    /*8.	Egy paraméterül kapott, egészeket tartalmazó lista elemei közül adjuk meg azokat, 
    amelyek 1-nél többször is előfordulnak!*/
    
    public static List<Integer> checkElements(List<Integer> list){
        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            for (int j = 0; j < list.size(); j++) {
                //get-tel kiszedjük az adott elemet
                if(list.get(i).equals(list.get(j)))
                   count++; 
            } 
            if (count > 1){ 
                resultList.add(list.get(i));
            }
        }
        return resultList;
    }
    
    public static void main(String[] args) {
        List<Integer> tempList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 7, 8);
        List<Integer> myList = new ArrayList<>(tempList);
        System.out.println(checkElements(myList));
        
    }
}
