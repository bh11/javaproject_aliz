/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

/**
 *
 * @author Aliz
 */
public class RemoveSpaces {
    /*4.	Egy paraméterül kapott String objektumból távolítsuk el a szóközöket ciklus használatával.*/
    public static String removeAllSpaces(String string){
        StringBuilder resultString = new StringBuilder();
        for (char character : string.toCharArray()) {
            if(character != ' '){
                resultString.append(character);
            }
        }       
        return resultString.toString();
    }
    
    public static void main(String[] args) {
        String myString = "Szer e tem - a - fa gy it .";
        System.out.println(removeAllSpaces(myString));
        
    }
}
