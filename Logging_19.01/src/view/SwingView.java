/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import model.Log;

/**
 *
 * @author Aliz
 */
public class SwingView extends JFrame implements View{
    
    private Controller controller;
    private JTextArea jTextArea;
    private JButton jButton;

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void build() {
        jTextArea = new JTextArea(30, 70);
        jButton = new JButton("GO!");
        
        JPanel panel1 = new JPanel();
        panel1.add(jTextArea);
        
        JPanel panel2 = new JPanel();
        panel2.add(jButton);
        
        add(panel1, BorderLayout.CENTER);
        add(panel2, BorderLayout.SOUTH);
        
        jButton.addActionListener(e -> controller.handleButton());
        
        setMinimumSize(new Dimension(500, 400));
        pack();
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    @Override
    public void update(List<Log> logs) {
        jTextArea.setText(logs.stream()
                .map(Log::toString)
                .collect(Collectors.joining("\n"))
        );
    }
   
}
