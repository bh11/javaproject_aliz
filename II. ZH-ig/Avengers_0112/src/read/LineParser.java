/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import exceptions.InvalidNameAvengersException;
import hero.AbstractHero;
import hero.HeroFactory;
import java.util.stream.Collectors;
import store.Fleet;
import report.Report;
import report.Save;
/**
 *
 * @author Aliz
 */
public class LineParser {
    //sorok feldolgozása
    private static final String DELIMITER = ";";
    private static final int MIN_CHARACTERS_OF_NAME = 2;
    public static final String fileName = "heroes.txt";
    
    private final Fleet store = new Fleet();
    
    private final Report reporting = new Report();
    
    public void process(String line) throws InvalidNameAvengersException{
        String[] parameters = line.split(DELIMITER);
        checkNameRestriction(parameters[0]);
        AbstractHero hero = HeroFactory.create(parameters);
        store.add(hero);
    }
    
    //kiirja a flottát
    public void print(){
        System.out.println(store.toString());
    }
    
    //Dobjunk Exceptiont, ha kettőnél rövidebb az Age.
    //EZ checked Exception
    public void checkNameRestriction(String name) throws InvalidNameAvengersException{
        if(name.length() < MIN_CHARACTERS_OF_NAME) {
            throw new InvalidNameAvengersException("Invalid name: "+name);
        }
    }
    
    public void report(){
        
    }
    
    //HF 15.12
    public void save(){
        Save.save(store.getShips()
                .stream()
                .map(s -> s.getHeroes())    //hero lista
                .flatMap(heroes -> heroes.stream()) //átalakítjuk stream-mé, csak hero-kat tartalmaz               
                .collect(Collectors.toList()), fileName);
    }
}
