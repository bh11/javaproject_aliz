/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1711.Feladat2;

/**
 *
 * @author Aliz
 */
public class Account {
    private int amount;
    private String number;          //sorszam
 
    //konstruktor
    

    public Account(int amount, String number) {
        this.setAmount(amount);
        this.setNumber(number);      
    }
    
     public Account(String number){
        this(1000, number);
    }
     
     
     //toString (Ctrl+space)
    @Override
    public String toString() {
        return "Amount: "+amount+" USD, Number: "+number;
    }
     
            
//

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
