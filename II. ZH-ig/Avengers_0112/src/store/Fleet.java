/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package store;

import hero.AbstractHero;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class Fleet {

    //hajokat tarolunk
    private final List<Ship> ships = new ArrayList<>();

    public void add(AbstractHero hero) {
        //el kell tárolni olyan hajóba ahol van üres hely
        //amikor betelt egy hajó, akkor hozunk létre újat
        findShip().add(hero);

    }

    @Override
    public String toString() {
        return "Fleet{" + "ships=" + ships + '}';
    }

    private Ship findLastShip() {
        //üres-e a lista
        if (ships.isEmpty()) {
            ships.add(new Ship());
        }
        //ami már az utolsó
        return ships.get(ships.size() - 1);
    }

    //melyik hajóba kell beraknunk a kapott hőst
    private Ship findShip() {
        //lekérjük a legutóbb berakott hajót
        Ship ship = findLastShip();
        //ha van hely akkor belerakjuk
        if (ship.isEmptySeat()) {
            return ship;
        } else {
            //az új hajóval térünk vissza
            ships.add(new Ship());
            //ami már az utolsó
            return ships.get(ships.size() - 1);
        }
    }

    public List<Ship> getShips() {
        return ships;
    }

}
