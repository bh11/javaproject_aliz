/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

import java.time.LocalDateTime;
import java.util.Objects;
import visitor.Adult;
import visitor.Kid;
import visitor.Visitor;
import visitor.Worker;
import zoo.shop.Food;

/**
 *
 * @author Aliz
 */
public abstract class AbstractAnimal {
    
    private final String name;   
    private final int requiredPlace;
    
    private LocalDateTime entryDate;
    private int visitors;
    private double hunger;
    private HappinessFactor happiness = HappinessFactor.SAD;

    public AbstractAnimal(String name, int requiredPlace) {
        this.name = name;
        this.requiredPlace = requiredPlace;
    }

    public LocalDateTime getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(LocalDateTime entryDate) {
        this.entryDate = entryDate;
    }

    public int getVisitors() {
        return visitors;
    }

    public void setVisitors(int visitors) {
        this.visitors = visitors;
    }

    public double getHunger() {
        return hunger;
    }

    public void setHunger(double hunger) {
        this.hunger = hunger;
    }

    public HappinessFactor getHappiness() {
        return happiness;
    }

    public void setHappiness(HappinessFactor happiness) {
        this.happiness = happiness;
    }

    public int getRequiredPlace() {
        return requiredPlace;
    }

    //látogatás: a látogató mondja meg hogy mit csinál egy adott állat esetén
    //visitor design pattern
    public void visit(Visitor visitor){
        visitor.act(this);
    }
    
    public void eat(Visitor visitor) {
       visitor.feed(this);
    }
    
    public abstract void eat(Food food);    //ezt implementálni kell Mammal és Reptile szinten
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractAnimal other = (AbstractAnimal) obj;
        if (this.requiredPlace != other.requiredPlace) {
            return false;
        }
        if (this.visitors != other.visitors) {
            return false;
        }
        if (Double.doubleToLongBits(this.hunger) != Double.doubleToLongBits(other.hunger)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.entryDate, other.entryDate)) {
            return false;
        }
        if (this.happiness != other.happiness) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
