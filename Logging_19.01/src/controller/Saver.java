/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;
import model.Log;

/**
 *
 * @author Aliz
 */
public class Saver {
    
    public static final String DEFAULT_FILENAME = "logs.txt";
    
    public void save(List<Log> logs){
        String toSave = logs.stream()
                .map(Log::toString)
                .collect(Collectors.joining("\n"));
        
         try (FileWriter fileWriter = new FileWriter(DEFAULT_FILENAME);
            PrintWriter printWriter = new PrintWriter(fileWriter)) 
        {
            printWriter.println(toSave);
        } catch (IOException ex) {
            ex.printStackTrace();
        }          
    }
}
