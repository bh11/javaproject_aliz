/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

/**
 *
 * @author Aliz
 */
public class Hungarian extends Drink implements Drinkable, DecreaseAlcohol{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private int alcoholVolume;
    
    public Hungarian(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price) {
        super(drinkType, manufacturerer, alcoholVolume, price);
        this.alcoholVolume = alcoholVolume; 
    }

    public int getAlcoholVolume() {
        return alcoholVolume;
    }

    @Override
    public void decreaseAlcohol(){
        if(alcoholVolume == 0){
            return;
        }
     alcoholVolume--;   
    }

    @Override
    public void drink() {
        System.out.println("I drank it!");
    }
    
    
}
