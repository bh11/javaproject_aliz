/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2510;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class SecondStars {

    public static void main(String[] args) {

        /*2. 
kérjünk be egy pozitív egész számot (ellenőrzés), majd írjunk ki csillagokat az alábbi formában a megadott szám alapján: 
a. 
szam = 7
*******
******
*****
****
***
**
*
         */
        Scanner sc = new Scanner(System.in);
        System.out.println("Kerem adjon meg egy 4 es 10 kozotti egesz szamot.");
        int number = sc.nextInt();

        for (int i = number; i > 0; i--) {
            for (int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }

        /* b. 
szam = 5 (ha páros a szám, vonjunk ki belőle egyet)
  *
 ***
*****
         */
        System.out.println("Kerem adjon meg egy 4 es 10 kozott.");
        int number2 = sc.nextInt();
        if (number2 % 2 == 0) {
            number2--;
        }
        for (int i = 0; i < number2; i += 2) {
            for (int j = 0; j < (number2 - i)/2; j++) {         //szokoz egyre kevesebb, nincs tizedes
                System.out.print(" ");
            }
            for (int j = 0; j <= i; j++) {                      //j egyre nagyobb (*)
                System.out.print("*");
            }
            System.out.println("");
        }

    }
}
