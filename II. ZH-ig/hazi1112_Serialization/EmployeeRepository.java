/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Aliz
 */
public class EmployeeRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepository.class.getName());

    //ez a hely ahova szerializálunk
    public static final String DEFAULT_FILENAME = "employees.data";
    //itt fogjuk tárolni az Employee-kat
    private List<Employee> store = new ArrayList<>();

    //konstruktor
    public EmployeeRepository() {
    }

    /**
     *
     * @param e instance of Employee
     * @return the index of the Employee to be able to remove or add him (-1:
     * not found)
     */
    public int indexOf(Employee e) {
        return store.indexOf(e);
    }

    /**
     * Update employee based on index no.
     *
     * @param index
     * @param e instance of Employee
     */
    public void update(int index, Employee e) {
        store.set(index, e);
    }

    /**
     * Add new or exisiting Employee
     *
     * @param emp
     * @return the index of the added employee (its needed for deletion)
     */
    public int add(Employee emp) {
        int index = this.indexOf(emp);
        if (index == -1) {   //ha még nincs ilyen akkor adjuk hozzá
            store.add(emp);
        } else {
            this.update(index, emp);
        }
        return store.lastIndexOf(emp);
    }

    /**
     *
     * @param index based on which we can delete an Employee
     */
    public void remove(int index) {
        store.remove(index);
    }

    /**
     *
     * @param index based on which we can get an Employee
     * @return Employee if found
     * @throws IndexOutOfBoundsException if out of ArrayList
     */
    public Employee get(int index) {
        return store.get(index);
    }

    /**
     *
     * @return no. of Employees
     */
    public int count() {
        return store.size();
    }

    /**
     * save
     */
    public void serializeEmployee() {
        try (FileOutputStream fs = new FileOutputStream(DEFAULT_FILENAME);
                ObjectOutputStream ou = new ObjectOutputStream(fs)) {
            ou.writeObject(store);          //kiiratjuk a store-t
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex); //logoljuk az exceptiont
        } catch (IOException exIO) {
            LOGGER.log(Level.SEVERE, null, exIO);
        }
    }
    
    /**
     * read and load
     */
    public void deSerializeEmployee() {
        try (FileInputStream fi = new FileInputStream(DEFAULT_FILENAME);
                ObjectInputStream oi = new ObjectInputStream(fi)) {
            store = (List<Employee>) oi.readObject();   //beolvassuk a listát
        } catch (FileNotFoundException ex){
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (IOException io) {
            LOGGER.log(Level.SEVERE, null, io);
        } catch (ClassNotFoundException cnf){
            LOGGER.log(Level.SEVERE, null, cnf);
        }
    }

    /**
     * Search which Employees belong to a department
     * @param department by String adress and name
     * @return list of Employees
     */
    public List<Employee> searchByDepartment(Department department) {
        return this.store.stream()
                .filter(emp -> emp.getDepartment()
                .equals(department))
                .collect(Collectors.toList());
    }

}
