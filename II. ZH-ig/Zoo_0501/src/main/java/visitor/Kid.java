/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import animal.AbstractAnimal;
import animal.HappinessFactor;

/**
 *
 * @author Aliz
 */
public class Kid implements Visitor{

    @Override
    public void act(AbstractAnimal animal) {
        animal.setVisitors(animal.getVisitors()+ 2);
        
        animal.setHunger(animal.getHunger() - 1);   //1-el éhesebb lesz, és így sajnos negatív szám is lehet az éhsége
        animal.setHappiness(HappinessFactor.nextLevel(animal.getHappiness()));  //1-el boldogabb lesz
    }

    @Override
    public void feed(AbstractAnimal animal) {
        //itt lehetne saját kivételosztály is
        throw new UnsupportedOperationException("Not supported yet."); 
    }

   
    
}
