use iskola;
create table diakok(dkod int, vnev varchar(20), knev varchar(20)); 
insert into diakok values(1001, 'Kiss', 'János'), (1002, 'Nagy', 'János'), (1003, 'Nagy', 'Katalin'),
(1004, 'Szabó', 'Kinga'), (1005, 'Kiss', 'Hajnalka'), (1006, 'Nagy', 'Benedek'),
(2001, 'Szabó', 'Péter'), (2002, 'Kiss', 'Katalin'), (2003, 'Nagy', 'János'), (2004, 'Szabó', 'Benedek');
create table regisztracio(dkod int, okod varchar(10), datum date);
alter table regisztracio modify okod enum ('nem01', 'nem02', 'ang01', 'ang02', 'ang03');
insert into regisztracio values (1001, 'nem01', '2002-05-07'), (1002, 'ang03', '2002-05-14'),
(1003, 'ang01', '2002-05-14'), (1004, 'nem01', '2002-05-07'), (1005, 'ang01', '2002-05-09'),
(1006, 'ang03', '2002-05-14'), (2001, 'ang03', '2002-05-09'), (2002, 'ang01', '2002-05-07'),
(2003, 'nem01', '2002-05-14'), (2004, 'ang03', '2002-05-09');	
create table orak(okod enum ('nem01', 'nem02', 'ang01', 'ang02', 'ang03'), targy enum('angol', 'nemet'),
szint tinyint, nap varchar(10), kezdete time, vege time);
insert into orak values('ang01', 'angol', 1, 'Hétfő', '08:00:00', '10:00:00'), ('ang02', 'angol', 1, 'Hétfő', '10:00:00',
'12:00:00'), ('ang03', 'angol', 2, 'Kedd', '08:00:00', '10:00:00'), ('nem01', 'német', 2, 'Kedd', '10:00:00', '12:00:00');
create table tandijak(okod enum ('nem01', 'nem02', 'ang01', 'ang02', 'ang03'), tandij int);
insert into tandijak values ('ang01', 5000), ('ang02', 10000), ('ang03', 4000), ('nem01', 6000), ('nem02', 7000);
select * from diakok;
select dkod, vnev from diakok where diakok.vnev = 'Nagy';
select okod from regisztracio where okod like '0%';
select count(knev) from diakok;
select count(*) from diakok;
select avg(tandij) from tandijak;
select diakok.vnev, diakok.knev, regisztracio.dkod from regisztracio inner join diakok on regisztracio.dkod=diakok.dkod;
select count(diakok.knev), regisztracio.dkod, regisztracio.okod from regisztracio inner join diakok on regisztracio.dkod=diakok.dkod
group by dkod;
select diakok.vnev, diakok.knev, regisztracio.dkod from regisztracio inner join diakok on regisztracio.dkod=diakok.dkod 
having count(diakok.vnev) > 3;
select diakok.knev from diakok where diakok.knev <> 'János' order by diakok.knev asc;
select diakok.knev from diakok where diakok.knev='Katalin' or 'Kinga';
select diakok.knev, diakok.dkod, regisztracio.okod from ((diakok inner join regisztracio on diakok.dkod=regisztracio.dkod)
inner join tandijak on regisztracio.okod=tandijak.okod) inner join orak on tandijak.okod=orak.okod;	-- ?
select tandijak.tandij, regisztracio.okod from regisztracio inner join tandijak on regisztracio.okod=tandijak.okod
group by tandij;







