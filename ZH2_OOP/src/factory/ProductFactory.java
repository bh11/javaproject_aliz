/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import product.AbstractProduct;
import product.BeautyProduct;
import product.FunProduct;
import product.KitchenProduct;
import product.Type;

/**
 *
 * @author Aliz
 */
public class ProductFactory {
    //vonalkód, típus, gyártó, ár, speciális_csoport
    
    private static final String KITCHEN = "kitchen";
    private static final String BEAUTY = "beauty";
    private static final String FUN = "fun";
    
    private static final int DEFAULT_WEIGHT_OF_BEAUTY = 10;
    
    //String bemenet, int vagy Type kimenet ahol szükséges
    public static AbstractProduct create(String barCode, String type, String manufacturer, String price, String group){
        int code = Integer.parseInt(barCode);
        Type productType = Type.valueOf(type.toUpperCase());
        int productPrice = Integer.parseInt(price);
        
        switch(group){
            case KITCHEN: 
                return new KitchenProduct(manufacturer, productPrice, productType);
            case BEAUTY: 
                return new BeautyProduct(manufacturer, productPrice, productType, DEFAULT_WEIGHT_OF_BEAUTY);
            case FUN: 
                return new FunProduct(manufacturer, productPrice, productType);
        }
        return null;
    }
}
