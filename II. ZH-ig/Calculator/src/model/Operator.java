/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Aliz
 */
    //7., MVC step 4
    //kibővítjük, MVC step 5
public enum Operator {
    ADD("+") {
        public int calc(int a, int b) {
            return a + b;
        }
    },
    MULTIPLY("*") {
        public int calc(int a, int b) {
            return a * b;
        }
    },
    EQ("=") {
        public int calc(int a, int b) {
            return 0;
        }
    },
    MINUS("-") {
        public int calc(int a, int b){
        return a - b;
        }
    },
    DIVIDE("/") {
         public int calc(int a, int b) {
            return a / b;
        }
    },
    MEMORY("M+") {
         public int calc(int a, int b) {
            return 0;
        }
    };

    
    private final String character;

    private Operator(String character) {
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }
    
    //(MVC step 6)
    //ezt az absztrakt metódust minden enum példánynál implementálni kell
    public abstract int calc(int a, int b);
        

    @Override
    public String toString() {
        return character;
    }
    
    
}
