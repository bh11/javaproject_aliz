/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class ConsoleView implements View{
    
    private Controller controller;
    
    private static final String STOP = "exit";
    private static final String DELIMITER = " ";
    
    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    public void start(){
        try (Scanner scanner = new Scanner(System.in)) {        
            String line;                                        
            do{
                System.out.print("> ");
                line = scanner.nextLine();
                if(line != null){
                    this.handler(line);
                }
            } while (!STOP.equals(line));
        }
    }
    
    private void handler(String command){
        String[] commands = command.split(DELIMITER);
        /*command példa: 
        select <bossname>
        add boss <name>
        add worker <name> <year>
        save <filename>
        load <filename>
        list boss
        list worker
        help
        exit
        */
        switch(commands[0].toLowerCase()){
            case "select":
                if(!this.checkCommmandMinLength(commands, 2, "select <bossname>")) break;
                //ha nem sikerült kiválasztania a főnököt
                if(!this.controller.selectBoss(commands[1])){
                    System.err.println("Not found.");
                }                    
            break;
            
            case "add":
                this.handleAdd(commands);
            break;
            
            case "save":
                if(!this.checkCommmandMinLength(commands, 2, "save <filename>")) break;
                this.controller.save(commands[1]);
            break;
            
            case "load":
                 if(!this.checkCommmandMinLength(commands, 2, "load <filename>")) break;
                 this.controller.load(commands[1]);
            break;
            
            case "list":
                this.handleList(commands);
            break;
            
            case "help":
            default:
                System.out.println("Examples:\n" +
                        "select <bossname>\n" +
                        "add boss <name>\n" +
                        "add worker <name> <year>\n" +
                        "save <filename>\n" +
                        "load <filename>\n" +
                        "list boss\n" +
                        "list worker\n" +
                        "help\n" +
                        "exit");
                break;
                
            case "exit":
                //nothing
                break;              
        }
    }
    
    private void handleAdd(String[] commands){
         if(!this.checkCommmandMinLength(commands, 3, "add boss <name> OR add worker <name> <year>")) 
             return;
         
        switch(commands[1].toLowerCase()){
            case "worker":
                if(!this.checkCommmandMinLength(commands, 4, "add worker <name> <year>")) break;
                this.controller.addWorker(commands[2], Integer.parseInt(commands[3]));
                break;

            case "boss"://boss
                this.controller.addBoss(commands[2]);
                break;

            default:
                //biztosan hibára fog futni
                this.checkCommmandMinLength(commands, Integer.MAX_VALUE, "add boss <name> OR add worker <name> <year>");
                break;
        }
    }
    
    private void handleList(String[] commands){
         if(!this.checkCommmandMinLength(commands, 2, "list boss OR list worker")) 
             return;
         
         switch(commands[1].toLowerCase()){
            case "worker":
                this.controller.getWorkers();
                break;

            case "boss"://boss
                this.controller.getAllBosses();
                break;

            default:
                //biztosan hibára fog futni
                this.checkCommmandMinLength(commands, Integer.MAX_VALUE, "list boss OR list worker");
                break;
        }
    }
    
    /**
     * Checks if a given
     * command is correct
     * @param commands
     * @param minLength
     * @param exampleMessage
     * @return true if correct otherwise false
     */
    private boolean checkCommmandMinLength(String[] commands, int minLength, String exampleMessage){
        if(minLength > commands.length){
            System.out.println("Wrong command! Example: " + exampleMessage + "\n");
            return false;
        }
        return true;
    }

    @Override
    public void showBosses(List<String> bosses) {
        System.out.println("Bosses:");
        bosses.stream()
                .forEach(System.out::println);
    }

    @Override
    public void showWorkers(List<String> workers) {
        System.out.println("Workers:");
        workers.stream()
                .forEach(System.out::println);
    }

    @Override
    public void showSelectedBoss(String name) {
        System.out.println("Seleted boss: "+name);
    }
}
