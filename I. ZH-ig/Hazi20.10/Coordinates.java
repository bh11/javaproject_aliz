/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Coordinates {

    public static void main(String[] args) {

        //8. Olvass be 2 számot. Határozd meg, hogy a 2 szám által meghatározott koordináta melyik síknegyedben van.
        /*X≥0 és Y≥0 → Síknegyed=1
        X≥0 és Y<0 → Síknegyed=4
        X<0 és Y≥0 → Síknegyed=2
        X<0 és Y<0 → Síknegyed=3*/
        Scanner sc = new Scanner(System.in);

        System.out.println("Kerem adjon meg ket egesz szamot.");

        while (!sc.hasNextInt()) {
            System.out.println("Ez nem szam.");
            sc.nextLine();
        }
        int x = sc.nextInt();

        while (!sc.hasNextInt()) {
            System.out.println("Ez nem szam.");
            sc.nextLine();
        }

            int y = sc.nextInt();

            int siknegyed = 0;

            if (x >= 0 && y >= 0) {
                siknegyed = 1;
            } else if (x >= 0 && y < 0) {
                siknegyed = 4;
            } else if (x < 0 && y >= 0) {
                siknegyed = 2;
            } else {
                siknegyed = 3;
            }
            System.out.println("A siknegyed: " + siknegyed);
        }
    }

