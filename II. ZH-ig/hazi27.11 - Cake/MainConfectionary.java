/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2711;

/**
 *
 * @author Aliz
 */
public class MainConfectionary {

    public static int countCalories(Sweet[] sweets) {
        int sum = 0;
        // Sweet típusú, sweet nevű, sweets tömbből jön
        for (Sweet sweet : sweets) {
            if(sweet != null)
            sum += sweet.getCalories();
        }
        return sum;
    }

    public static void printWishes(Sweet[] sweets) {
        for (Sweet sweet : sweets) {
            if (sweet instanceof BirthdayCake) {
                BirthdayCake bcake = (BirthdayCake) sweet;
                System.out.println("A süti jókívánsága: " + bcake.getBirthdayWish());
            }
        }
    }

    public static void main(String[] args) {

        Sweet[] sweets = new Sweet[100];
        for (int i = 0; i < 20; i+=5) {
            sweets[i] = new Cake("Feketeerdo", 260, new String[]{"liszt", "so", "tejszin"});
            sweets[i + 1] = new Cake("Diotorta", 380, new String[]{"liszt", "so", "dio"});
            sweets[i + 2] = new Cake("Velvet", 300, new String[]{"liszt", "so", "szirup", "cseresznye"});
            sweets[i + 3] = new BirthdayCake("Fuled erjen bokaig", "citromtorta", 290, new String[]{"vaj", "citrom"});
            sweets[i + 4] = new BirthdayCake("Isten eltessen", "pudingtorta", 320, new String[]{"vaj", "pudingpor"});
        }

        System.out.println("Az összes süti száma: " + sweets.length);
        System.out.println("Az összes süti kalóriája: " + countCalories(sweets));
        printWishes(sweets);

    }
}
