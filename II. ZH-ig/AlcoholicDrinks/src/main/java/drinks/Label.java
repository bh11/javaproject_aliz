/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drinks;

import java.io.Serializable;

/**
 *
 * @author Aliz
 */
public class Label implements Serializable{
    //saját UID
    private static final long serialVersionUID = 1L;
    
    private final Colors c;
    private final int year;
    
    public Label(Colors c, int year){
        this.c = c;
        this.year = year;
    }
}
