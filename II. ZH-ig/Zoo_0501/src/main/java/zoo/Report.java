/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import animal.AbstractAnimal;
import animal.HappinessFactor;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Aliz
 */
public class Report {
    /*Napi riportot szeretnénk az állatkertről, a boldogtalan és éhes állatokat szeretnénk tudni. 
    Ketrecenként hány gyerek látogató volt?*/
    
    private Zoo zoo;

    public Report(Zoo zoo) {
        this.zoo = zoo;
    }

    public Zoo getZoo() {
        return zoo;
    }
    
    
    public Cage findCageForAnimalToPlace(AbstractAnimal abstractAnimal){
        return zoo.getCages().stream()
                .filter(s -> s.isEnoughSpace(abstractAnimal.getRequiredPlace())).findAny().orElse(new Cage());
    }
    
    public Cage findCageOfAnimalBelongs(AbstractAnimal abstractAnimal){
        return zoo.getCages().stream()
                .filter(a -> a.getAnimals().contains(abstractAnimal)).findAny().orElseThrow();
    }
    
    public List<AbstractAnimal> getHungryandUnhappyAnimals(){
        return zoo.getCages().stream().flatMap(a -> a.getAnimals().stream())
                .filter(a -> a.getHunger() < 5 && a.getHappiness().equals(HappinessFactor.SAD))
                .collect(Collectors.toList());
    }
    
    //TODO: gyereklátogatók száma
}
