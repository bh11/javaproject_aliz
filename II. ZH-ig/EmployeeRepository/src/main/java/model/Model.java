/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import employee.Boss;
import employee.Worker;
import java.util.Collection;

/**
 *
 * @author Aliz
 */
public interface Model {
     /**
     * If we add an already existing Boss, the te previous should be deleted.
     *
     * @param name
     * @return
     */
    public Boss addBoss(String name);

    public Worker addWorker(Boss boss, String name, int age);

    public Boss getBoss(String name);

    public Collection<Boss> getAllBosses();

    public void save(String fileName);

    public void load(String fileName);
}
