/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import read.ConsoleReader;

/**
 *
 * @author Aliz
 */
public class MainApp {

    public static void main(String[] args) {
        System.out.println("How to use:");
        System.out.println("CREATE type name_of_manufaturerer price volume_of_alcohol nationality(spec1, spec2)");
        System.out.println("SAVE");
        System.out.println("REPORT");
        ConsoleReader consoleReader = new ConsoleReader();
        consoleReader.read();
    }
}
