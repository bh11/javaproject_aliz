/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212.tizenkettedik_Builder;

/**
 *
 * @author Aliz
 */
public class CarBuilder{
    private String plate;
    private int id;
    private String color;
    private int speed;

    public CarBuilder(String plate, int id, String color, int speed) {
        this.plate = plate;
        this.id = id;
        this.color = color;
        this.speed = speed;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
    
    public Car build(){
        return new Car(plate, id, color, speed);
    }
}
