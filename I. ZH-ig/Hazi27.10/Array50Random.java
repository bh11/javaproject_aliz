/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2710;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Array50Random {

    public static void main(String[] args) {

        /*4. Hf (7. feladat:)
Állítsunk elő egy 50 elemű tömböt véletlen egész számokból (0-tól 9-ig terjedő számok legyenek).
- Írjuk ki a kigenerált tömböt a képernyőre.
- Számítsuk ki az elemek összegét és számtani középértékét - median.
- Olvassunk be egy 0 és 9 közötti egész számot, majd határozzuk meg, hogy a tömbben ez a szám
hányszor fordul elő.
         */
        Scanner sc = new Scanner(System.in);

        int[] array = new int[50];
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
            sum += array[i];
            System.out.print(array[i] + " ");
            if (i % 10 == 0 && i != 0) {            //0.-ik indexunel is beszur uj sort de az nem kell
                System.out.println("");
            }
        }
        System.out.println("");
        System.out.println("A tomb elemeinek osszege: " + sum);
        System.out.println("A tomb elemeinek kozepertek: " + 1.0 * sum / array.length);

        System.out.println("Kerem adjon meg egy egesz szamot.");
        int number;
        while (true) {
            if (!sc.hasNextInt()) {
                System.out.println("Nem jo az input.");
                sc.nextLine();
                continue;
            }
            number = sc.nextInt();
            break;
        }

        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {

                count++;
            }
        }
        System.out.println("A elem ennyiszer fordul elo a tombben: "+count);
    }
}
