/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2212.tizenharmadik_UML;

import java.util.TreeSet;

/**
 *
 * @author Aliz
 */
public class P {
    public M m;
    private TreeSet<Z> zoo = new TreeSet<>();   //vagy SortedSet
    //maximum 2 elem lehet benne, de ez már implementáció ezért nem kötelező ebben a feladatban
    public P (Z z1, Z z2){
        if(z1 != null){
            zoo.add(z1);
        }
        if(z2 != null){
            zoo.add(z2);
        }
    }
}
