/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import factory.ProductFactory;
import product.AbstractProduct;
import zh2_oop.ProductStore;
import zh2_oop.Report;

/**
 *
 * @author Aliz
 */
public class CommandHandler {

    //3 parancs
    private static final String ADD = "add";
    private static final String REMOVE = "remove";
    private static final String REPORT = "report";
    private static final String EXIT = "exit";

    private static final String DELIMITER = " ";

    private final ProductStore productStore = new ProductStore();
    private final Report report = new Report();
    
    //itt lehetne a Savert példányosítani, és case SAVE-t is készítenénk

    public void process(String line) {
        String[] input = line.split(DELIMITER);

        switch (input[0].toLowerCase()) {
            case ADD:
                if (!this.checkCommmandMinLength(
                        input, 
                        6, 
                        "ADD <barCode no> <type> e.g. cheap <manufacturer> <price> <group> e.g. kitchen")
                        ) {
                    break;
                }
                AbstractProduct newProduct = ProductFactory.create(input[1], input[2], input[3], input[4], input[5]);
                productStore.add(newProduct);
                break;
                
            case REMOVE: //kapunk egy vonalkódot
                productStore.remove(Integer.parseInt(input[1]));    //parseInt mert Stringet kapunk de a BarCode egy szám
                break;
                
            case REPORT:
                report.generateReports(productStore.getProducts());
                break;
                
            case EXIT:
                default:
                    //nothing
                break;
        }
    }

    /**
     * Checks if a given command has correct length.
     * @param commands
     * @param minLength
     * @param exampleMessage
     * @return true if correct otherwise false
     */
    private boolean checkCommmandMinLength(String[] commands, int minLength, String exampleMessage) {
        if (minLength > commands.length) {
            System.out.println("Wrong command! Example: " + exampleMessage + "\n");
            return false;
        }
        return true;
    }
}
