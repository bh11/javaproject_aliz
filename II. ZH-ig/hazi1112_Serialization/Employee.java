/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi1112_Serialization;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class Employee implements Serializable{
    //saját UID, ugyanaz mint Department-ben
    private static final long serialVersionUID = 1L;
    
    private String name;
    private int age;
    private int salary;
    private Department department;
    
    //generált konstruktor

    public Employee(String name, int age, int salary, Department department) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.department = department;
    }
    
    //generált setterek, getterek

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    
    //generált toString

    @Override
    public String toString() {
        return "Employee{" + "name=" + name + ", age=" + age + ", salary=" + salary + ", department=" + department + '}';
    }
    
    //ahhoz hogy megtaláljuk az Employeet
    //generált hashCode és equals
    //név és kor alapján legyen ugyanaz az Employee

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.age != other.age) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
}
