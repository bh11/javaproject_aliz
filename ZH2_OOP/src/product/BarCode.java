/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author Aliz
 */
public class BarCode {
    private final int number;

    public BarCode() {
        this.number = (int) (Math.random() * 2000) + (2000-1000);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.number;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BarCode other = (BarCode) obj;
        if (this.number != other.number) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BarCode{" + "number=" + number + '}';
    }
    
    
    
}
