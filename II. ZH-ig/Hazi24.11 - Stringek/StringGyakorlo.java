/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2411;

import java.util.Arrays;

/**
 *
 * @author Aliz
 */
public class StringGyakorlo {
    /*   1. Ez a string hány mondatból áll? 
         2. A fenti stringben hányszor szerepel az "it" részlet
         3. írjuk ki a fenti szövegben az első és az utolsó "it" közötti szövegrészletet
         4. írjunk ki minden második szót
         5. cseréljük le az 'a' betűket 'A' betükre
         6. írjuk ki fordítva (StringBuilder metódus segítségével)*/
    
    public static void main(String[] args) {
        
        //1.
        String str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        String[] sentences = str.split("\\.");
        System.out.println("A String "+sentences.length+" mondatbol all.");
        
        //2.
        int count = str.length() - str.replace("it","").length();   //regi String - "it" nelkuli hossz
        System.out.println("A Stringben "+count+"-szer szerepel az 'it' reszlet");
        
        //3.
        int firstIndex = str.indexOf("it");
        firstIndex = firstIndex+"it".length();  //azert adjuk hozza, hogy az elso "it" ne legyen benne
        
        int lastIndex = str.lastIndexOf("it");
               
        String newString = str.substring(firstIndex, lastIndex);    //kivagjuk a firsIndex es lastIndex kozotti reszt
        System.out.println(newString);
        
        //4.
        str = " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        String trimmed = str.trim();
        String[] words = trimmed.split(" ");
        System.out.println("Minden masodik szo: ");
        for (int i = 1; i < words.length; i+=2) {
            System.out.print(words[i]+"; ");           
        }
        System.out.println("");
        
        //5.
        String newStr = str.replace('a', 'A');
        System.out.println("A kis 'a' betuk felcserelve: "+newStr);
        
        //6.
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        System.out.println("A String visszafele: "+sb);      
        
    }
}
