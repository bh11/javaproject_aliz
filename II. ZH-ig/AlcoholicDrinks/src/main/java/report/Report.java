/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import drinks.Drink;
import drinks.DrinkType;
import drinks.French;
import drinks.German;
import drinks.Hungarian;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class Report {

    public Report() {
    }

    public void generateReports(List<Drink> allListOfDrinks, List<Drink> savedDrinks) {
        this.getCountOfSavedDrinks(savedDrinks);
        this.getCountOfDrinksByGroups(allListOfDrinks);
        this.getCountOfWines(allListOfDrinks);
        this.getNamesOfManufacturers(allListOfDrinks);
    }
    
    //a) Hány termék lett kimentve?
    private void getCountOfSavedDrinks(List<Drink> listOfDrinks){
        System.out.println("Number of saved drinks: "+listOfDrinks.size());
    }
    //b) Hány termék van speciális csoportonként?
    private void getCountOfDrinksByGroups(List<Drink> listOfDrinks){
        System.out.println("Number of hungarian drinks: ");
        System.out.println(listOfDrinks.stream()
                .filter(h -> h instanceof Hungarian)
                .count());
        System.out.println("Number of german drinks: ");
        System.out.println(listOfDrinks.stream()
                .filter(h -> h instanceof German)
                .count());
        System.out.println("Number of french drinks: ");
        System.out.println(listOfDrinks.stream()
                .filter(h -> h instanceof French)
                .count());       
    }
    
    //c) Hány palackozott bor van készleten?
    private void getCountOfWines(List<Drink> listOfDrinks){
        System.out.println("Number of wines on store: ");
        System.out.println(listOfDrinks.stream()
                .filter(w -> w.getDrinkType() == DrinkType.WINE)
                .count());
    }
    
    //d) Kik a gyártók a készletet nézve (fel kell sorolni őket)?
    private void getNamesOfManufacturers(List<Drink> listOfDrinks){
        System.out.println("The list of manufacturers: ");
        listOfDrinks.stream()
                .map(d -> d.getManufacturerer())
                .distinct()
                .forEach(System.out::println);
    }

}
