/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.util.Scanner;
import parser.CommandParser;

/**
 *
 * @author Aliz
 */
public class ConsoleReader {
    private static final String STOP = "exit";
    
    public void read(){
        CommandParser parser = new CommandParser();
        
        try (Scanner scanner = new Scanner(System.in)) {        //csak akkor nem dob Exceptiont ha konzolra csatlakozunk
            String line;                                        //ha fájlból kéne olvasni akkor InputStream legyen
            do{
                line = scanner.nextLine();
                if(line != null){
                    parser.parse(line);
                }
            } while (!STOP.equals(line));
        }
    }
}
