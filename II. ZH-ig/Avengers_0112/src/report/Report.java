/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package report;

import hero.AbstractHero;
import hero.BornOnEarth;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import stone.StoneType;
import store.Fleet;
import store.Ship;

/**
 *
 * @author Aliz
 */
public class Report {

    private Fleet fleet;
    
    public Report() {
    }

    public void generateReports() {
        

    }

    //Hány földi születésű hős indul a harcba? 
    public long getCountOfBornOnEarth(List<AbstractHero> heroes) {
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .count();
    }

    //Ki rendelkezik a legnagyobb erővel? 
    public AbstractHero strongestAvenger(List<AbstractHero> heroes) {
        return heroes.stream()
                .sorted((a, b) -> b.getPower() - a.getPower())
                .findFirst().get();
    }

    //Listázzuk ki a felhasznált útlevelek sorszámát!
    public List<Integer> serialNumberIdentityCards(List<AbstractHero> heroes) {
        return heroes.stream()
                .filter(h -> h instanceof BornOnEarth)
                .map(i -> ((BornOnEarth) i).getIdentityCard().getNumber()).collect(Collectors.toList());
    }

    //Hajónként melyik kőből mennyi van?
    private Map<StoneType, Integer> countOfStones(Ship ship) {
        return ship.getHeroes().stream() //a hósökből köveket csinálunk
                .map(AbstractHero::getStone) //a toMapnek 4 paramétere van, 2 kötelező, köveket ad vissza
                .collect(Collectors.toMap(k -> k,
                        v -> 1,
                        (v1, v2) -> v1 + v2,    //kulcsütközés: map-ben lévő értékhez hozzáadunk 1-et
                        HashMap::new    //visszaad egy HashMap-et
                        )
                    );
    }
    
    private Map<Ship, Map<StoneType, Integer>> countOfStonesByShips(){
        return fleet.getShips().stream()
                .collect(Collectors.toMap(
                        s -> s,     //hajót megtartjuk mint kulcsot
                        this::countOfStones,      //v -> cuntOfStones(v), az érték is Ship típus
                        (x, y) -> x                         //ha ütközik a kulcs, akkor tartsa meg a régit
                                )
                        );       
    }
    
    //megcsináljuk a kiiratását
    private void printCountOfStonesByShips(){
        countOfStonesByShips().entrySet().stream()
                .forEach(i->{System.out.println("Ship: "+i);
                i.getValue()
                        .forEach((j, k)-> System.out.println("Stone: "+j+" Count: "+k));
                });
    }
    
    //Bontsuk ketté a hajókat: 1. csoport csak földiek, 2. csoport legalább egy nem földi van a hajón
    private boolean containsOnlyBornonEarth(Ship ship){
        return ship.getHeroes().stream()
                .allMatch(p -> p instanceof BornOnEarth);
    }
    
    //Map értéke egy boolean, értéke lista a hajókról
    private Map<Boolean, List<Ship>> partitioningByContainsOnlyBornOnEarth(){
        return fleet.getShips().stream()
                .collect(Collectors.partitioningBy(
                        this::containsOnlyBornonEarth));
    }
    
    //hány BornOnEarth van a hajókon
    private long numberOfShipsContainingOnlyBornOnEarth() {
        return partitioningByContainsOnlyBornOnEarth().get(Boolean.TRUE).size();    //a get kulcsot vár paraméterül
    }
                
}
