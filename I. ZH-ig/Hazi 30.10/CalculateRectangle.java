/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi3010;

/**
 *
 * @author Aliz
 */
public class CalculateRectangle {
    
    /*14. feladat: Készítsünk olyan függvényt, amely egy téglalap két oldalának ismeretében kiszámítja 
    a téglalap területét! Az eredmény legyen a visszatérési érték. Ehhez írjunk programot, amiben ki 
    is próbáljuk a függvényt.*/
    
    static double calculateTerritory (double terra1, double terra2){
        double territory = terra1 * terra2;
        return territory;
    }
    
    public static void main(String[] args) {
        double a = 5.6;
        double b = 8.5;
        double eredmeny = calculateTerritory(a, b);
        
        System.out.println("A teglalap terulete: "+ eredmeny +" cm.");
    }
}
