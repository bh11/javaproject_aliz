/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.servlet;

import hu.braininghub.service.EmployeeService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
@WebServlet(name = "EmployeeServlet", urlPatterns = {"/EmployeeServlet"})
public class EmployeeServlet extends HttpServlet {

    @Inject
    private EmployeeService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<String> names;
        
        if(request.getParameter("name")!= null){
            String name = request.getParameter("name");
            names = service.searchEmployee(name);
        } else {
            names = service.getNames();
        }
             
            for(String s : service.getNames()){
                System.out.println(s);
            }  
            
            request.setAttribute("names", names);
            
            request.getRequestDispatcher("/WEB-INF/empnames.jsp").forward(request, response);
    }

}
