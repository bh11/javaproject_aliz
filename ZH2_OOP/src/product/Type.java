/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

/**
 *
 * @author Aliz
 */
public enum Type {
    CHEAP(3),
    AVERAGE(5),
    EXPENSIVE(10),
    LUXURY(12),
    POPULAR(15);
    
    private final int id;

    private Type(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    
}
