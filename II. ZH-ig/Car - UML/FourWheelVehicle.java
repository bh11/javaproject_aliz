/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author Aliz
 */
public class FourWheelVehicle extends Vehicle {
    private int seatCapacity;
    public static final int LOCKED_NUM_WHEELS = 4;

    //orokolt setNumWheels
    protected FourWheelVehicle() {
        this.setNumWheels(LOCKED_NUM_WHEELS);
    }
          

    public int getSeatCapacity() {
        return this.seatCapacity;
    }

    public void setSeatCapacity(int seatCapacity) {
        this.seatCapacity = seatCapacity;
    }  
}
