/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filefilter;

import collector.FileCollector;
import filter.FilterAggregator;
import java.io.File;
import java.util.List;
import reader.ConsoleReader;

/**
 *
 * @author Aliz
 */
public class Application {
    
    private final FileCollector fileCollector = new FileCollector();
    private final ConsoleReader consoleReader = new ConsoleReader();
    private final FilterAggregator filterAggregator= new FilterAggregator();
    
    public void process(){
        String path = consoleReader.read(); //consoleReaderen keresztül kapunk útvonalat
        List<File> files = fileCollector.collect(new File(path));   //új lista
        List<File> filteredFiles = filterAggregator.filter(files);
        
        printResult(filteredFiles);
        
    }
    
    private void printResult(List<File> files){
        files.forEach(System.out::println);
    }
    
    public static void main(String[] args) {
        System.out.println("Please provide your directory for listing files.");
        Application app = new Application();
        app.process();
    }
}
