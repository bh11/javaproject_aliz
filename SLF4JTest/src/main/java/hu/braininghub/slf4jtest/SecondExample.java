/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.slf4jtest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Aliz
 */
public class SecondExample {
    
    private static Logger logger = LoggerFactory.getLogger(SecondExample.class);

    public static void main(String[] args) throws InterruptedException {
        for(int i = 0; i<500; i++){
            logger.info("Hello World", i);
            Thread.sleep(10);
        }
    }
}
