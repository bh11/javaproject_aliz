/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package read;

import drinks.Colors;
import drinks.Drink;
import drinks.DrinkType;
import drinks.French;
import drinks.FrenchState;
import drinks.German;
import drinks.Hungarian;
import drinks.Label;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import report.Report;
import report.SaveLoad;


/**
 *
 * @author Aliz
 */
public class LineParser {

    private static final String DELIMITER = " ";
    private final List<Drink> listOfDrinks = new ArrayList<>();
    
    public void parser(String line) {
        String[] parameters = line.split(DELIMITER);
        process(parameters);
    }
    
    public void process(String[] parameters){
        switch(parameters[0].toLowerCase()) {
            case "create":
                create(parameters);
                break;
            case "save":
                SaveLoad save = new SaveLoad();
                List<Drink> saveList = this.listOfDrinks.stream().filter(d -> d.getDrinkType() == DrinkType.WINE).collect(Collectors.toList());
                save.saveDrink(saveList);
                break;
            case "report":
                SaveLoad load = new SaveLoad();
                Report report = new Report();
                report.generateReports(listOfDrinks, load.loadDrink());
                break;
            default:
                System.out.println("Invalid command.");
                break;
        }
    }
    
    public void create(String[] parameters){
        DrinkType drinkType;
        switch(parameters[1].toLowerCase()){
            case "beer":
                drinkType = DrinkType.BEER;
                break;
            case "wine":
                drinkType = DrinkType.WINE;
                break;
            case "brandy":
                drinkType = DrinkType.BRANDY;
                break;
            default:
                System.out.println("Invalid drinktype.");
                return;
        }
        String manufacturer = parameters[2];
        int price = Integer.parseInt(parameters[3]);
        int alcoholVolume = Integer.parseInt(parameters[4]);
  
        Drink drinkByNationality;
        switch(parameters[5].toLowerCase()){
            case "hungarian":
                drinkByNationality = new Hungarian(drinkType, manufacturer, alcoholVolume, price);
                break;
            case "german":
                drinkByNationality = this.createGerman(drinkType, manufacturer, alcoholVolume, price, parameters[6], parameters[7]);
                break;
            case "french":
                drinkByNationality = this.createFrench(drinkType, manufacturer, alcoholVolume, price, parameters[6]);
                break;
            default:
                System.out.println("Wrong name of drink nationality.");
                return;
        }
        
        listOfDrinks.add(drinkByNationality);
    }
    
    public German createGerman(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price, String rawColor, String rawYear){
        //Stringből Enumot
        Colors color = Colors.valueOf(rawColor.toUpperCase());
        int year = Integer.parseInt(rawYear);
        
        return new German(drinkType, manufacturerer, alcoholVolume, price, new Label(color, year));
    }
    
    public French createFrench(DrinkType drinkType, String manufacturerer, int alcoholVolume, int price, String rawFrenchState){
        //Stringből Enumot
        FrenchState frenchState = FrenchState.valueOf(rawFrenchState.toUpperCase());
        
        return new French(drinkType, manufacturerer, alcoholVolume, price, frenchState);
    }
    
}
