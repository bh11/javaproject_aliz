/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import country.Country;
import java.util.List;

/**
 *
 * @author Aliz
 */
public interface View {
    public void setController(Controller controller);
    
    public void showCountries(List<Country> countries);
    
    public void start();
}
