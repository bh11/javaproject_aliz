/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Controller;
import country.Country;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class SwingView extends JFrame implements View{

    private Controller controller;
    
    private final JTextField inputField = new JTextField(25);
    private final JButton button = new JButton("Search");
    private final JTextArea displayResultArea = new JTextArea();
    private final JScrollPane scrollPane = new JScrollPane(displayResultArea);
    

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void showCountries(List<Country> countries) {
        displayResultArea.setText("");
        countries.forEach(e -> displayResultArea.append(e.toString()+"\n"));   //hozzáfűzzük az új szöveget
    }

    @Override
    public void start() {
        this.displayResultArea.setEditable(false);
        
        //padding, border
        this.displayResultArea.setBorder(BorderFactory.createCompoundBorder(
                this.displayResultArea.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5))
        );
        
        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(this.inputField, BorderLayout.CENTER);
        northPanel.add(this.button, BorderLayout.EAST);
        
        //buttonhoz létrehozunk ActionListenert: átadjuk a szöveget amit beírtak
        button.addActionListener(e -> {
            this.controller.filterCountry(this.inputField.getText());
        });
        
        //scrollozható legyen
        add(this.scrollPane, BorderLayout.CENTER);
        add(northPanel, BorderLayout.NORTH);
        
        setMinimumSize(new Dimension(500, 400));
        pack();
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        controller.notifyView();    //controller adjon át egy listát
        
        setVisible(true);      
        
    }
    
}
