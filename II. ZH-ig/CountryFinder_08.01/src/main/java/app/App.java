/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.Controller;
import controller.MyController;
import model.CountryModel;
import model.Model;
import view.SwingView;
import view.View;

/**
 *
 * @author Aliz
 */
public class App {
    
    public static void main(String[] args) {
        
        View view = new SwingView();
        Model model = new CountryModel();
        Controller controller = new MyController(view, model);
        
        view.start();
    }
}
