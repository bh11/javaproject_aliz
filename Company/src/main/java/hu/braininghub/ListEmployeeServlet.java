/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
public class ListEmployeeServlet extends HttpServlet {

    private EmployeeService employeeService = new EmployeeService();
    
    //private String employeeName = "Ferenc";
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("employees", this.employeeService.getEmployees());
        request.getRequestDispatcher("employees.jsp").forward(request, response);   //a szöveg kezdődhetne WEB-INF-el is
                                                                                //employees.jsp-nek forwardolunk így 
    }

   

}
