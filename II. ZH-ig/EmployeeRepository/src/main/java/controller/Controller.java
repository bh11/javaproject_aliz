/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author Aliz
 */
public interface Controller {
     /**
     * We select a Boss
     * @param name
     * @return true: selection success, false: selection failed
     */
    public boolean selectBoss(String name);
    
    /**
     * Queries all bosses from Model
     */
    public void getAllBosses();
    
     /**
     * selectBoss() is prior needed, otherwise returns empty list
     */
    public void getWorkers();
    
    /**
     * If we add a Boss, then it will be automatically selected.
     * If we add an already existing Boss, the te previous will be deleted.
     * @param name 
     */
    public void addBoss(String name);
    
    /**
     * selectBoss() is prior needed
     * If the worker already exists then it will be not modified.
     * @param name
     * @param age 
     */
    public void addWorker(String name, int age);
    
    public void save(String fileName);
    
    public void load(String fileName);
}
