/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hero;


import stone.StoneType;


/**
 *
 * @author Aliz
 */
public class HeroFactory {
    //hősöket gyárt
    private static final String EARTH = "0";
    private static final String NOT_EARTH = "1";
    
    //megkapja a parametereket amikből készíteni kell
    //a String tömb a splittelt sor
    //név, erő, kő, földi-e
    //[Captain America, 3.5, SOUL, 0]
    public static AbstractHero create(String[] parameters){
        String name = parameters[0];
        int power = Integer.parseInt(parameters[1]);
        /*egyenlőre mindenki az időkövet kapja meg
        AbstractStone stone = new Time();*/
        
        //ugy kell megkapnunk a Stringet, hogy matchel-jen enumra
        StoneType stone = StoneType.valueOf(parameters[2]);
        
        //eldönti hogy milyen alosztályt hozzon létre
        if(EARTH.equals(parameters[3])){
            return new BornOnEarth(name, power, stone, new IdentityCard());
        } else {
            return new NotBornOnEarth(name, power, stone);
        }
    }
}
