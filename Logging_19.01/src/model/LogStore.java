/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Controller;
import controller.MyController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class LogStore implements Model{
    //model osztály
    
    private final List<Log> logs = new ArrayList<>();
    private Controller controller;
    
    
    @Override
    public List<Log> getLogs() {
        return logs;
    }

    @Override
    public void add(Log log) {
        logs.add(log);
        
        controller.notifyView();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
    
    
}
