/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi0611;


/**
 *
 * @author Aliz
 */
public class TwoDimensionArraySmallest {

    /*írjunk metódust, ami egy n*m-es kétdimenziós tömböt feltölt véletlenszerű elemekkel valamilyen határok között, 
     majd írjunk egy másik metódust, ami egy n*m-es kétdimenziós tömb legkisebb elemével és annak helyével tér vissza. 
     A visszatérés egy tömb legyen, az alábbi formában [sor, oszlop, érték]*/
    
    static void fillInArray(int[][] array) {
        int min = 0;
        int max = 100;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = randomNumberGenerator(max, min);
            }
        }
    }

    static int randomNumberGenerator(int max, int min) {
        int number = (int) (Math.random() * (max - min) + min);
        return number;
    }
    
    static int[] smallestNumber(int[][] array){
        int min = Integer.MAX_VALUE;
        int index = 0;
        int index2= 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
               if(array[i][j] < min){
                   min = array[i][j];
                   index = i;
                   index2 = j;
               }                
            }           
        }
        int[] result = new int[3];
        result[0] = index; 
        result[1] = index2;
        result[2] = min;
        
        return result;
    }

    public static void main(String[] args) {
        int[][] array = new int[3][4];
        fillInArray(array);
        int[] result = smallestNumber(array);
        System.out.println("index1: "+result[0]);
        System.out.println("index2: "+result[1]);
        System.out.println("min: "+result[2]);
        
    }
}
