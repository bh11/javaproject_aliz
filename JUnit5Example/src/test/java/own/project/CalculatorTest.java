/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package own.project;

import own.project.Calculator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 *
 * @author Aliz
 */
public class CalculatorTest {
    
    @BeforeAll
    public static void testPrintBeforeAll(){
        System.out.println("Printing before the class is loaded.");
    }
    
    @BeforeEach
    public void printCalculatorMethodDescription(){
        System.out.println("We are going to substract.");
    }
    
    @Test
    public void substractNumbersTest(){
        int op1 = 15;
        int op2 = 10;
        Calculator underTest = new Calculator(op1, op2);

        int result = underTest.substract();
        
        Assertions.assertEquals(5, result, () -> "Wrong result");  //a végén: Supplier<String> message
    }
    
    @Test
    public void multiplyNumbersTest(){
        int op1 = 15;
        int op2 = 10;
        Calculator underTest = new Calculator(op1, op2);

        int result = underTest.multiply();
        
        Assertions.assertEquals(150, result, () -> "Wrong result");
    }
    
    @Test
    public void addTest(){
        //given: a teszt bemenő paramétere
        int op1 = 10;
        int op2 = 15;
        Calculator underTest = new Calculator(op1, op2);
        
        //when
        int result = underTest.add();
        
        //then
        Assertions.assertEquals(op1 + op2 + 1, result, () -> "Wrong addition");
    }
    
    @AfterEach
    public void information(){
        System.out.println("We have substracted.");;
    }
    
    @AfterAll
    public static void closing(){
        System.out.println("The test is done.");
    }
}
