/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.hr_1901;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Aliz
 */
public class SwingView extends JFrame{
    private HrDao hrDao;
    
    private final JButton button1 = new JButton("findEmployeeWithMaxSalary");
    private final JButton button2 = new JButton("mostPopulatedDepartment");
    private final JButton button3 = new JButton("findEmployeeByIdUnsafe");
    private final JButton button4 = new JButton("findEmployeeByFirstName");
    private final JButton button5 = new JButton("findEmployeeEarnsMoreThanAvg ");
    private final JButton button6 = new JButton("freshEmployees");
    private final JButton button7 = new JButton("titleContainsClerk");
    
    private final JTextArea jTextArea = new JTextArea(20,20);
    
    private final JTextField inputField = new JTextField(50);
    
    private final JScrollPane scrollPane = new JScrollPane(jTextArea);

    
     public void buildView() {
        add(buildNorth(), BorderLayout.NORTH);
        add(buildCenter(), BorderLayout.CENTER);
         
        button1.addActionListener(a -> jTextArea.setText(hrDao.findEmployeeWithMaxSalary()
                                        .stream().collect(Collectors.joining("\n"))) );
        button2.addActionListener(a -> jTextArea.setText(hrDao.mostPopulatedDepartment()
                                        .stream().collect(Collectors.joining("\n"))));
        button3.addActionListener(a -> jTextArea.setText(hrDao.findEmployeeByIdUnsafe(inputField.getText())
                                        .stream().collect(Collectors.joining("\n"))));
        button4.addActionListener(a -> jTextArea.setText(hrDao.findEmployeeByFirstName(inputField.getText())
                                        .stream().collect(Collectors.joining("\n"))));
        button5.addActionListener(a -> jTextArea.setText(hrDao.findEmployeeEarnsMoreThanAvg()
                                        .stream().collect(Collectors.joining("\n"))));
        button6.addActionListener(a -> jTextArea.setText(hrDao.freshEmployees()
                                        .stream().collect(Collectors.joining("\n"))));
        button7.addActionListener(a -> jTextArea.setText(hrDao.titleContainsClerk()
                                        .stream().collect(Collectors.joining("\n"))));
                                        
        setMinimumSize(new Dimension(500, 400));
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
     
     
    
     private JPanel buildNorth(){
         JPanel panel1 = new JPanel();
         panel1.setLayout(new GridLayout(5, 3));
         
         panel1.add(button1);
         panel1.add(button2);
         panel1.add(button3);
         panel1.add(button4);
         panel1.add(button5);
         panel1.add(button6);
         panel1.add(button7);   
         
         return panel1;
     }
     
     private JPanel buildCenter(){
         JPanel panel2 = new JPanel();
         panel2.add(scrollPane);
         panel2.add(inputField);
     
         return panel2;
     }
}
