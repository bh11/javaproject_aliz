create database customer;
create table customers(customerID int primary key auto_increment, 
CustomerName varchar(50), ContactName varchar(50), Adress varchar(100),
City varchar(20), PostalCode int, Country varchar(20));
select CustomerName, City from customers;
select * from customers;
select distinct Country from customers;
select * from customers;
ALTER TABLE customers modify PostalCode varchar(10);
insert into customers values (1, "Alfreds Futterkiste", "Maria Sanders", "Obere Str. 57",
"Berlin", "12209", "Germany"), (2, "Ana Trujillo Emparedados y helados", "Ana Trujillo",
"Avda. de la Constitución 2222", "México D.F.", "05021", "Mexico"), 
(3, "Antonio Moreno Taquería", "Antonio Moreno", "Mataredos 2312", "México D.F.", "05023",
"Mexico"), (4, "Around the Horn", "Thomas Hardy", "120 Hanover Sq.", "London", "WA1 1DP",
"UK"), (5, "Berglunds snabbköp", "Christina Berglund", "Berguvsvagen 8", "Lulea", "S-958 22",
"Sweden");
select count(distinct Country) from customers;
select * from customers where Country like "%ex%";
select * from customers where Country like "Ger%" and City like "Ber%";
select * from customers where City = "Berlin" or "München";
select * from customers where Country = "Germany" or "Spain";
select * from customers where Country != "Germany";
select * from customers where Country = "Germany" and City = "Berlin" or "München";
select * from customers where Country != "Germany" or "USA";
select * from customers order by Country;
select * from customers order by Country DESC;
select * from customers order by Country and CustomerName;
select * from customers order by Country ASC, CustomerName DESC;
select * from customers where Adress = null;
select * from customers where Adress != null;
update customers set ContactName = "Kis Jozsi", City = "Budapest" where customerID = 1;
update customers set ContactName = "Juan" where Country like "Mex%";
SET SQL_SAFE_UPDATES = 0;
update customers set ContactName = "Juan" where Country like "Mex%";
delete from customers where CustomerName = "Alfreds Futterkiste";
SELECT * FROM Customers LIMIT 3;
select * from customers where CustomerName like "a%";
select * from customers where CustomerName like "%a";
select * from customers where CustomerName like "%or%";
select * from customers where CustomerName like "_r";
select * from customers where CustomerName like "a_%_%";
select * from customers where CustomerName like "a%o";
start transaction;
insert into customers values (6, "Alfreds Futtebjibrkiste", "Maria Sanders", "Obere Str. 57",
"Berlin", "12209", "Germany"), (7, "Ana Truiubuijillo Emparedados y helados", "Ana Trujillo",
"Avda. de la Constitución 2222", "México D.F.", "05021", "Mexico");
rollback;
select * from customers;
insert into customers values (6, "Alfreds Futtebjibrkiste", "Maria Sanders", "Obere Str. 57",
"Berlin", "12209", "Germany"), (7, "Ana Truiubuijillo Emparedados y helados", "Ana Trujillo",
"Avda. de la Constitución 2222", "México D.F.", "05021", "Mexico");
commit;
select * from customers;
SELECT * FROM Customers WHERE City='Berlin' OR City='München';
select * from customers limit 3;
select * from customers where CustomerName not like 'a%';
select * from customers where Country in ('Germany', 'France', 'UK');
select * from customers where Country not in ('Germany', 'France', 'UK');
create table product(ProductID int primary key auto_increment, ProductName varchar(30), 
SupplierID int, CategoryID int, Unit varchar(50), Price int);
insert into product values(1, 'Chais', 1, 1, '10 boxes x 20 bags', 18), (2, 'Chang', 1, 1,
'24 -12 oz bottles', 19), (3, 'Aniseed Syrup', 1, 2, '12 -550 ml bottles', 10),
(4, "Chef Anton's Cajun Seasoning", 2, 2, '48 -6 oz jars', 22),
(5, "Chef Anton's gumbo Mix", 2, 2, '36 boxes', 21.35);
select min(Price) from product;
use diakok;
select min(Price) as MinPrice from product;
select max(Price) as MaxPrice from product;
select count(ProductName) as CountOfProducts from product;
select avg(Price) as AvgPrice from product;
alter table product modify Price double;
select avg(Price) from product;
select * from product where Price between 10 and 20;
select * from product where Price not between 10 and 20;
select * from product where Price between 10 and 20 and not CategoryID in (1,2,3);
create table OrderDetailTable(OrderDetailID int primary key auto_increment,
orderID int, ProductID int, Quantity int);
insert into orderdetailtable (orderID, ProductID, Quantity) values(10248, 11, 12), 
(10248, 42, 10), (10248, 72, 5), (10249, 14, 9), (10249, 51, 40);
select sum(Quantity) from orderdetailtable;
create table OrderTable(OrderID int primary key auto_increment,
CustomerID int, EmployeeID int, OrderDate date, ShipperID tinyint);
alter table ordertable modify orderID int;
insert into ordertable values (10248, 90, 5, '1996-07-04', 3), (10249, 81, 6, '1996-07-05',
1), (10250, 34, 4, '1996-07-08', 2), (10251, 84, 3, '1996-07-09', 1), (10252, 76, 4, '1996-07-10',
2);
select * from ordertable where OrderDate between '1996-07-01' and '1996-07-30';
select count(CustomerName), Country from customers group by Country;
select count(CustomerName), Country from customers group by Country order by Country desc;
select count(CustomerName), Country from customers group by Country having count(CustomerName) > 5;
create table Employee(EmployeeID int primary key auto_increment, LastName varchar(20),
FirstName varchar(20), BirthDate date, Photo varchar(20));
alter table Employee modify Photo tinyblob;
insert into Employee (LastName, FirstName, BirthDate, Photo) values ('Davolio', 'Nancy', '1968-12-08', 'EmplD1.pic'),
('Fuller', 'Andrew', '1952-02-12', 'EmplD2.pic'), ('Leverling', 'Janet', '1963-08-30', 'EmplD3.pic');
-- multiple tables
select ordertable.OrderID, customers.CustomerName from ordertable inner join customers on ordertable.customerID = customers.customerID;
select customers.customerName, ordertable.OrderID from customers left join ordertable on customers.customerID = ordertable.customerID;
select A.CustomerName as CustomerName1, B.CustomerName as CustomerName2, A.City from customers A, customers B
where A.CustomerID <> B.CustomerID and A.City = B.City order by A.City;
select ordertable.OrderID, Employee.FirstName, Employee.Lastname from ordertable right join employee on employee.EmployeeID = ordertable.EmployeeID
order by ordertable.orderID;
-- select customers.CustomerName, ordertable.orderID from customers full outer join ordertable on customers.customerID=ordertable.customerID
-- order by customers.CustomerName;
select * from customers left join ordertable on customers.customerID=ordertable.CustomerID union select * from customers
right join ordertable on customers.customerID=ordertable.CustomerID;
select count(orderID), ShipperID from ordertable group by ShipperID order by ShipperID;
select ordertable.OrderID, Employee.FirstName, Employee.Lastname from ordertable right join employee on employee.EmployeeID = ordertable.EmployeeID
having count(ordertable.OrderID) > 5;
select employee.LastName, count(ordertable.orderID) as NumberOfOrders from ordertable
inner join employee on ordertable.EmployeeID = employee.EmployeeID where LastName = 'Davolio' or LastName = 'Fuller'
group by LastName having count(ordertable.orderID) > 25;
select * from employee, ordertable;
select employee.LastName, ordertable.orderID from ordertable inner join employee on ordertable.EmployeeID=employee.EmployeeID
where LastName = 'Davolio' group by orderID;
select employee.LastName, count(ordertable.orderID) from ordertable inner join employee on ordertable.EmployeeID=employee.EmployeeID
where LastName = 'Davolio';












