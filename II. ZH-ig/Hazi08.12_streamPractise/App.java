/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streamPractise;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aliz
 */
public class App {
    static List<Country> countries = new ArrayList<>();
    
    
    public static void main(String[] args) {
        generateData(); 
        //printCountOfCountriesWhereNameContains("S");
        //printCountryByCode("hu");
        //printAllCities();
        //sortAllOfCountry();
        //printContinents();
        //printAllCities();
        
        //HF:
        //printSortedCountriesByContinentDesc();
        //printSortedCountriesByCountOfCities();
        //printCountryWhichContainsPartOfCityName("b");
        printCountrySizes();
    }
    
    public static void generateData() {
        Country hungary = new Country("hu", "Hungary", "Europe", 93);
        hungary.addCity(new City("Budapest", 2000));
        hungary.addCity(new City("Szeged", 800));
        hungary.addCity(new City("Nyíregyháza", 200));
        hungary.addCity(new City("Tokaj", 30));
        
        Country spain = new Country("sp", "Span", "Europe", 110);
        spain.addCity(new City("Barcelona", 300));
        spain.addCity(new City("Madrid", 250));
        
        Country thailand = new Country("th", "Thailand", "Asia", 200);
        thailand.addCity(new City("Phuket", 50));
        thailand.addCity(new City("Bangkok", 3000));
        
        countries.add(hungary);
        countries.add(hungary);
        countries.add(spain);
        countries.add(thailand);
    }
    //basic feladat
    public static void printAllCountries() {
        //countries.stream().forEach((x)-> System.out.println(x));
        countries.stream().forEach(System.out::println);
    }
    //basic: egy ország egyszer szerepeljen
    public static void printDistinctCountries() {
        countries.stream().distinct().forEach((x)-> System.out.println(x));
    }
    //basic: benne van-e
    public static void printCountriesWhereNameContains(String str) {
        countries.stream().filter((x)->x.getName().contains(str)).forEach((country)->System.out.println(country));
    }
    //basic: hány country-t tartalmaz
    public static void printCountOfCountriesWhereNameContains(String str) {
        System.out.println(countries.stream().filter((x)->x.getName().contains(str)).count());
    }    
    //basic: itt ki kell iratni nem a streamet hanem a countryt
    public static void printCountryByCode(String code) {
        countries.stream().filter((x)->x.getCode().equals(code)).forEach((country)->System.out.println(country));
    }
    //basic
    public static void sortAllOfCountry() {
        countries.stream()
                //itt tudnia kell hogy mihez hasonlítson
                 .sorted((c1, c2)->c1.getName().compareTo(c2.getName()))
                 .forEach((x)->System.out.println(x));
    }
    //extra
    public static void printAllCities() {     
       countries.stream()
               .forEach((country)->country
                       .getCities()
                       //a benne lévő city-t átalakítjuk stream-mé
                            .stream()
                            .forEach((city)->System.out.println(city)));
    }
    //basic: kiprintelni a kontinenseket
    public static void printContinents() {
        //igy sajnos benne vannak az ismétlődések
        //countries.stream().forEach((country)->System.out.println(country));
        //getContinentig már csak a kontinenseket tartalmazza
        countries.stream()
                .map((country)->country.getContinent()) //minden countryból kiszedi a continentet, az új stream kontinenseket fog tartalmazni
                .distinct()     //levesszük a duplikációt
                .forEach((x)->System.out.println(x));
        
        //vagy kiszervezzük egy listába
        
        /*az egyik listából kigyűjti a kontinenseket, String listával tér vissza
        countries.stream().map((country)->country.getContinent()).collect(Collectors.toList());*/
    }
    //basic
    public static void printCountryNames() {
        countries.stream().forEach((country)->System.out.println(country.getName()));
    }
    //basic: saját megoldás
    public static void printCountryCodes() {
        countries.stream().forEach((country)->System.out.println(country.getCode()));
    }    
    //extra
    public static void printSumOfPopulation() {
        //minden country-hoz tartozó populáció összege
        countries.stream()
                .mapToInt((country)->country    
                        .getCities()
                        .stream()
                        .mapToInt(city->city.getPopulation())   //olyan dolgot kell meghivni ami integert ad vissza
                        .sum())                 //összeadjuk az egyes városok népességét
                .sum();                         //összeadjuk az egyes országok népességét
        /*countries.get(0).getCities()
                .stream()
                .mapToInt(c->c.getPopulation())
                .sum();*/
    }
    //extra
    public static void printSumOfEuropePopulation() {
        int population = countries
                .stream()
                .filter((c)->c.getContinent().equals("Europe"))
                .mapToInt(
                         (country)->country.getCities()
                            .stream()
                            .mapToInt((city)->city.getPopulation())
                            .sum())
                .sum();
        System.out.println("A népesség: "+population);
    }
    //extra
    public static void printAllOfCountriesPopulationLessThanX(int x) {
        int populationLessThanX = countries
                .stream()
                .mapToInt((country)->country.getCities()
                    .stream()
                    .mapToInt((city)->city.getPopulation())
                    .sum())
                .filter((pop)-> pop < x)
                .sum();
        System.out.println("Az országok száma hol kevesebb a népesség: "+populationLessThanX);
    } 
    
    //HF
    //kontinens neve alapján legyen abc sorrendben csökkenő
    public static void printSortedCountriesByContinentDesc() {
        countries.stream()
                .sorted((a, b) -> b.getContinent().compareTo(a.getContinent()))
                .distinct()
                 //kiírjuk az országok nevét és a hozzájuk tartozó kontinenst
                .forEach((element)->System.out.println(element.getName()+" ("+element.getContinent()+")"));                   
    }
    
    //városok darabszáma alapján legyen sorrendben
    public static void printSortedCountriesByCountOfCities() {
        countries.stream()
                .sorted((a,b)-> a.getCities().size() - b.getCities().size())
                .distinct()
                .forEach((element)->System.out.println(element.getName()+" ("+element.getCities().size()+")"));
    }

    
    //kap egy név részletét a városnak, írja ki azokat az országokat ahol ez megtalálható, ne legyen casesensitive
    public static void printCountryWhichContainsPartOfCityName(String partOfCityName) {
        countries.stream()
                        .filter((country) -> country.getCities()
                                .stream()
                                .anyMatch((city -> city.getName().toLowerCase().contains(partOfCityName.toLowerCase()))
                                )
                        )
                .distinct()
                .forEach((country)->System.out.println(country.getName()));                                            
    }

    //írja ki az országokhoz tartozó méreteket
    public static void printCountrySizes() {
        countries.stream()
                .distinct()
                .forEach((country)->System.out.println(country.getName()+" "+country.getSize()));      
    }
}
