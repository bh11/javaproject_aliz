/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ora3010;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class BombaJatek {
  /*Felhasznalo megad 2 egesz szamot 10 es 15 kozott.
        Ebbol keszitunk 2 dimenzios palyat.
        Generalnunk kell valamennyi bombat 8 es 20 kozott, felhasznalo adja meg.
        Ket tipusu bomba van, az egyiken meghalunk, a masikon csak eletet veszitunk.
        70% halal, 30% -1 elet. Kezdeti eletek szama 5.
        Van egy jatekosunk amivel csak jobbra es lefele tudunk lepni.
        Kezdetben a 0,0-an van.
        A cel, hogy az utolso sorba eljussunk ugy, hogy nem halunk meg.
        Az utolso sorban kiirjuk, hogy hany elet maradt es hany lepesbol oldottuk meg.
     */
     
     //1. lepes hogy a valtozokat kigyutjuk.
     
    //metodus annyiszor hasznalja a Scannert, ahanyszor akarja
    static final Scanner SCANNER = new Scanner(System.in);

    static final char GOOD_BOMB = 'D';
    static final char BAD_BOMB = '*';
    static final char PLAYER = '8';
    static final char GIFT = '+';

    static final int MIN_SIZE = 10;             //sor es oszlop, ezeknek nem valtozik az ertekekuk
    static final int MAX_SIZE = 15;

    static final int MIN_NUMBER_OF_BOMBS = 8;
    static final int MAX_NUMBER_OF_BOMBS = 20;
    
    static final int MAX_NUMBER_OF_GIFT = 3;

    static final char EMPTY = '\0';             //karakter aminek erteke 0

    static int lifePoints = 5;
    static int playerX = 0;                     //jatekos pozicioja
    static int playerY = 0;

    //palya letrehozasa, metodus addig keri a szamot amig megfelelo nem lesz
    static int getNumber(int from, int to) {            //feladata: olvasson be egy szamot egy intervallumbol
        do {       

            System.out.printf("Give me a number between %d and %d. ", from, to);

            if (SCANNER.hasNextInt()) {
                int number = SCANNER.nextInt();
         //nem tudjuk a ranget, ne legyen invalid
        //boolean isValidNumber = false;

                if (number >= from && number <= to) {
                //isValidNumber = true;
                    return number;                 //igy nem kell a boolean,es nem szol a fordito mert vegtelen ciklus
                }

            } else {
                swallowString();                    

            }

        } while (true);                             //vagy while (!isValidNumber);

    }

    static void swallowString() {                   //egy Stringet at tud ugrani a folyamban
        SCANNER.next();
    }

    static void printField(char[][] field) {
        for (int i=0; i<field.length; i++){                     //a palya eleje
            System.out.print("_");
        }
        System.out.println();
        
        for (int i = 0; i < field.length; i++) {                //sorokon megy vegig
            for (int j = 0; j < field[i].length; j++) {         //ha a masodik dimenziora vagok kivancsi, akkor az elsot kell rogziteni
                if (field[i][j] == EMPTY) {
                    System.out.print(" ");
                } else {
                    System.out.print(field[i][j]);              //pl. kirakja a jatekost
                }
            }
            System.out.println();
        }
        
        for (int i=0; i<field.length; i++){                     //a palya vege
            System.out.print("^");
        }
        System.out.println();
    }

    static void play(int row, int col) {
        char[][] field = new char[row][col];

        putGifts(field);
        putPlayer(field);
        printField(field);
        putBombs(field);
        

        step(field);

    }

    static void putPlayer(char[][] field) {
        field[playerX][playerY] = PLAYER;
    }

    //letesszuk a bombakat
    static void putBombs(char[][] field) {
        int numberOfBombs = getNumber(MIN_NUMBER_OF_BOMBS, MAX_NUMBER_OF_BOMBS);
        int numberOfGoodBombs = calculateNumberOfGoodBombs(numberOfBombs);
        int numberOfBadBombs = numberOfBombs - numberOfGoodBombs;

        setBombs(field, numberOfGoodBombs, GOOD_BOMB);
        setBombs(field, numberOfBadBombs, BAD_BOMB);

        printField(field);

    }

    static int calculateNumberOfGoodBombs(int numberOfBombs) {
        return numberOfBombs / 3;                               //30% lesz jo bomba
    }

    /**
     * This method returns a random number between from and to excluding to
     *
     * @param min
     * @param max
     * @return
     */
     //randomgeneralo
    static int generateRandomNumber(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }

    //hanyszor es mit rakjon le, hova
    static void setBombs(char[][] field, int count, char character) {
        for (int i = 0; i < count; i++) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[0].length);       //az elso sor hossza is ugyanaz mint a tobbie

            if (field[x][y] == EMPTY) {
                field[x][y] = character;

            } else {
                i--;                //ne rakjon oda bombat ahol van valami mas

            }

        }

    }

    static void putGifts(char[][] field) {
        for (int i = 0; i < MAX_NUMBER_OF_GIFT;) {
            int x = generateRandomNumber(0, field.length);
            int y = generateRandomNumber(0, field[0].length);       

            if (field[x][y] == EMPTY) {
                field[x][y] = GIFT;
                i++;
            }
        }

    }
    //addig menjunk amig van eletunk es tart a palya.
    static void step(char[][] field) {
        while (lifePoints > 0 && !isPlayerInTheLastRow(field)) {
            char c = SCANNER.next().charAt(0);

            switch (c) {
                case 'r':
                    stepRight(field);
                    break;
                case 'd':
                    stepDown(field);
                    break;
                case 'u':
                    stepUp(field);
                    break;
                case 'l':
                    stepLeft(field);
                    break;
            }

            printField(field);
        }
    }

    static void stepRight(char[][] field) {
        if (!isPlayerInTheLastColumn(field)) {
            handleBombs(field, playerX, playerY + 1);
            handleGifts(field, playerX, playerY + 1);

            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY++;
                field[playerX][playerY] = PLAYER;

            }
        }
    }
    
     static void stepLeft(char[][] field) {
        if (playerY != 0) {
            handleBombs(field, playerX, playerY - 1);
            handleGifts(field, playerX, playerY - 1);

            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerY--;
                field[playerX][playerY] = PLAYER;

            }
        }
    }

    static void stepDown(char[][] field) {
        if (!isPlayerInTheLastRow(field)) {             //utolso kiszurese, hogy ne lepjunk le a palyarol
            handleBombs(field, playerX + 1, playerY);   //eletpontok
            handleGifts(field, playerX + 1, playerY);   //eletpontok
            
            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX++;
                field[playerX][playerY] = PLAYER;

            }
        }
    }
    
      static void stepUp(char[][] field) {
        if (playerX != 0) {                             //kiindulasi ponttol feljebb nem megyunk
            handleBombs(field, playerX - 1, playerY);
            handleGifts(field, playerX - 1, playerY);

            if (lifePoints > 0) {
                field[playerX][playerY] = EMPTY;
                playerX--;
                field[playerX][playerY] = PLAYER;

            }
        }
    }

    static void handleBombs(char[][] field, int x, int y) {
        if (field[x][y] == GOOD_BOMB) {
            lifePoints--;
            System.out.println("Ennyi eleted maradt: "+lifePoints);
        }

        if (field[x][y] == BAD_BOMB) {
            lifePoints = 0;
            System.out.println("Sajnos meghaltal.");
        }
    }
    
     static void handleGifts(char[][] field, int x, int y) {
        if (field[x][y] == GIFT) {
            lifePoints++;
            System.out.println("Ennyi eleted lett: "+lifePoints);
        }
     }

    static boolean isPlayerInTheLastColumn(char[][] field) {
        return playerY == field[0].length - 1;                  //utolso index
    }

    //Megvizsgalja hogy az utolso sorban vagyunk-e.
    static boolean isPlayerInTheLastRow(char[][] field) {
        return playerX == field.length - 1;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int row = getNumber(MIN_SIZE, MAX_SIZE);
        int col = getNumber(MIN_SIZE, MAX_SIZE);

        play(row, col);

        //itt a printfield nem lesz jo az itteni tomb miatt int[][] field = null; 
    }

}
  

