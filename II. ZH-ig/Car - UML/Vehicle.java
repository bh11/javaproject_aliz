/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UML;

/**
 *
 * @author Aliz
 */
public class Vehicle {

    private int speed;
    private String color;
    private String registrationNumber;
    private int numWheels;
 
    protected Vehicle() {
    }
    
    public int getSpeed() {
        return this.speed;
    }

    protected void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRegistrationNumber() {
        return this.registrationNumber;
    }

    protected void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getNumWheels() {
        return numWheels;
    }

    protected void setNumWheels(int numWheels) {
        this.numWheels = numWheels;
    }

}
