/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi1011;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Polindrome {

    /*A felhasznalo megad egy mondatot. Dontsuk el, hogy polindrome-e? (oda-vissza ugyanaz)*/
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        boolean isPalindrome = true;

        for (int i = 0; i < word.length() /2; i++) {
            if (word.charAt(i) != word.charAt(word.length() - i - 1)) {     //vege: length()-1, abbol visszafele az annyiadik elem
                isPalindrome = false;
                break;
            }
        }
        if (isPalindrome) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }

    }

}
