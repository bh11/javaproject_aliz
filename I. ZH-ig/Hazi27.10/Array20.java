/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi2710;

import java.util.Scanner;

/**
 *
 * @author Aliz
 */
public class Array20 {

    public static void main(String[] args) {
        /*3. Hf (6. feladat:)
Olvassunk be egész számokat egy 20 elemű tömbbe, majd kérjünk be egy egész számot. Keressük meg a
tömbben az első ilyen egész számot, majd írjuk ki a tömbindexét. Ha a tömbben nincs ilyen szám, írjuk
ki, hogy a beolvasott szám nincs a tömbben.
         */
        Scanner sc = new Scanner(System.in);

        int[] array = new int[20];     
        System.out.println("Kerem adja meg a tomb elemeit!");

        for (int i = 0; i < array.length;) {
            if (!sc.hasNextInt()) {
                System.out.println("Nem egesz szam a megadott ertek.");
                sc.nextLine();
                continue;
            }
            array[i] = sc.nextInt();
            i++;
        }

        System.out.println("Kerem adjon meg egy egesz szamot.");
        int number;
        while (true) {
            if (!sc.hasNextInt()) {
                System.out.println("Nem jo az input.");
                sc.nextLine();
                continue;
            }
            number = sc.nextInt();
            break;
        }

        int numberIndex = 0;
        boolean doesCatchIt = false;
        System.out.println("A tomb elemeinek ertekei: ");
        for (int j = 0; j < array.length; j++) {                //j = index
            System.out.print(array[j] + " ");
            if (number == array[j] && !doesCatchIt) {
                System.out.print("<-- itt van ");
                numberIndex = j;
                doesCatchIt = true;
            }            
        }
        System.out.println("");
        if(doesCatchIt){
        System.out.println("A szam indexe: "+numberIndex);
        } else {
            System.out.println("A szam nincs meg a tombben: "+number);}
    }
}
