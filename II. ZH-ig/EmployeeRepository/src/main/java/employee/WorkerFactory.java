/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee;

import java.util.Objects;
import java.util.Optional;

/**
 *
 * @author Aliz
 */
public class WorkerFactory {       
    private Boss boss;
    
    public WorkerFactory(Boss boss){
        //a megadott boss tartozik a workerFactoryhoz
        this.boss = boss;
    }
    
    public Optional<Worker> createNewWorker(String name, int age){
        Objects.requireNonNull(name);
        Worker result = new Worker(this.boss, name, age);
        //így a főnök tudja hogy ki a dolgozója/hogyha false, akkor dolgozó már létezik, null helyett legyen Optional
        if(this.boss.add(result) == false) {
            return Optional.empty();
        }
        
        return Optional.of(result);
    }
}
