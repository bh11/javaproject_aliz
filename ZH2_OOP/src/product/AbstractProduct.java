/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package product;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public abstract class AbstractProduct implements Comparable<AbstractProduct>{
    private BarCode barCode = new BarCode();
    private String manufacturer;
    private int price;
    private Type type;
    private LocalDateTime date = LocalDateTime.now();

    public AbstractProduct(String manufacturer, int price, Type type) {
        this.manufacturer = manufacturer;
        this.price = price;
        this.type = type;
    }
    
    @Override
    public int compareTo(AbstractProduct abstractProduct){
        return this.barCode.getNumber() - abstractProduct.barCode.getNumber();
    }

    public BarCode getBarCode() {
        return barCode;
    }

    public void setBarCode(BarCode barCode) {
        this.barCode = barCode;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.barCode);
        hash = 29 * hash + Objects.hashCode(this.manufacturer);
        hash = 29 * hash + this.price;
        hash = 29 * hash + Objects.hashCode(this.type);
        hash = 29 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractProduct other = (AbstractProduct) obj;
        if (this.price != other.price) {
            return false;
        }
        if (!Objects.equals(this.manufacturer, other.manufacturer)) {
            return false;
        }
        if (!Objects.equals(this.barCode, other.barCode)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractProduct{" + "barCode=" + barCode + ", manufacturer=" + manufacturer + ", price=" + price + ", type=" + type + ", date=" + date + '}';
    }
    
    
    
}
