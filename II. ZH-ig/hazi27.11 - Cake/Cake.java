/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hazi2711;

import hazi2711.Sweet;

/**
 *
 * @author Aliz
 */
public class Cake extends Sweet{
    private String[] ingredients;
    private int countOfIngredients; 
    private String serving;

    public Cake() {
    }

    // megvalosítjuk az ős tulajdonságait, összetevőket
    public Cake(String fantasyName, int calories, String[] ingredients) {
        setServing("on plate");
        setFantasyName(fantasyName);
        setCalories(calories);
        //itt korlátozzuk hogy a cake-nek hány összetevője lehet
        setIngredientsLength(5);
        //kell a megadott hossz es az osztalyszintű hossz - ha valamelyik rövidebb akkor abbahagyja
        System.out.println(ingredients);
        System.out.println(this.ingredients);
        for (int i = 0; i < ingredients.length && i < this.ingredients.length; i++) {
            this.ingredients[i] = ingredients[i]; 
            countOfIngredients++;
        }
    }

    //így a leszármazottnál több elemszámú összetevőket hozhatunk létre
    //itt megkapja hogy mekkora elemszámú lehet
    protected void setIngredientsLength(int ingredients) {
        this.ingredients = new String[ingredients];
    }
 
    //implementálni kell a Sweetből
    @Override
    public String getServing() {
        return this.serving;
    }

    //leszarmazik
    protected void setServing(String serving) {
        this.serving = serving;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    //megmondja hogy hány sütink van
    public int getCountOfIngredients() {
        return countOfIngredients;
    }
}
