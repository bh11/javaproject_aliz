/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.Model;
import view.View;

/**
 *
 * @author Aliz
 */
public class MyController implements Controller{
    
    private final View view;
    private final Model model;

    /**
     * this method sets the controller into view
     * @param view
     * @param model 
     */
    public MyController(View view, Model model) {
        this.view = view;
        this.model = model;
        
        this.view.setController(this);
        this.model.setController(this);
    }

    @Override
    public void notifyView() {
        this.view.showCountries(new ArrayList<>(model.getAllCountries()));
    }
    
    @Override
    public void filterCountry(String name) {
        if(name.trim().isEmpty()){  //ha nem írtak be semmit a keresőbe akkor értelmetlen a keresés
            return;
        }
        this.model.filterCountries(name);
        this.notifyView();
    }

    

  

    
    
}
