/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PotZH1;



/**
 *
 * @author Aliz
 */
public class Tomb1 {
    
    /*1. Egy metódus paraméterül kap egy 10 elemű, random számokat tartalmazó
tömböt. A metódus visszatérési értéke a legnagyobb, a második legnagyobb és
a legkisebb szám (tehát a visszatérési érték egy 3 elemű tömb).*/

    static int randomNumberGenerator(int max, int min) {
        int number = (int) (Math.random() * (max - min) + min);
        return number;
    }

    static int[] fillArray(int max, int min, int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumberGenerator(max, min);
        }
        return array;
    }

    static int[] findValuesinArray(int[] array) {
        int smallest = array[0];
        int largest = array[0];
        int secondLargest = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < smallest) {
                smallest = array[i];
            }
            if (array[i] > largest) {
                secondLargest = largest;
                largest = array[i];
            } else if (array[i] > secondLargest) {
                secondLargest = array[i];
            }
        }

        int[] newArray = new int[3];
        newArray[0] = largest;
        newArray[1] = secondLargest;
        newArray[2] = smallest;

        return newArray;
    }

    public static void main(String[] args) {
        
        int[] array = new int[10];
        
        fillArray(100, 5, array);
        System.out.println("A tomb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        
        int[] finalArray = findValuesinArray(array);
        System.out.println("A legnagyobb elem: "+finalArray[0]);
        System.out.println("A masodik legnagyobb elem: "+finalArray[1]);
        System.out.println("A legkisebb elem: "+finalArray[2]);
    }
}
