/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.employee.repository;


import hu.braininghub.employee.repository.entity.Employees;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Singleton
public class EmployeeDAO {
    //entity-t ad

    @PersistenceContext
    private EntityManager em;

    public List<Employees> getEmployees() {
        return em.createQuery("SELECT e FROM Employees e", Employees.class)
                .setMaxResults(10)
                .getResultList();
    }

    public List<Employees> findByName(String str) {
        return em.createQuery("SELECT e FROM Employees e WHERE e.firstName LIKE :pattern", Employees.class)
                .setParameter("pattern", "%" + str + "%")
                .getResultList();

    }

}
