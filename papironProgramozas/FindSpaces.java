/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papironProgramozas;

/**
 *
 * @author Aliz
 */
public class FindSpaces {
    /*3.	Egy paraméterül kapott String objektumban számoljuk meg szóközöket.*/
    
    public static int countSpaces(String string){
        int count = 0;
        //végigmegyünk rajta mintha karaktertömb lenne
        for (char character : string.toCharArray()) {
            if(character == ' '){
                count++;
            }
        }
        return count;
    }
    
    public static void main(String[] args) {
        String myWord = "malacka elment a házból";
        
        System.out.println("A szóban lévő szóközök száma: "+countSpaces(myWord));
    }
}
