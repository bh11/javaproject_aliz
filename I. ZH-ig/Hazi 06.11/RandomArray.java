/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hazi0611;

/**
 *
 * @author Aliz
 */
public class RandomArray {
    
    /*
    Leggyakoribb szám: 0-100 közötti véletlen számokkal töltsünk fel egy 300 elemű tömböt, 
    majd keressük meg h melyik szám került bele a leggyakrabban.
    Megnoveljuk egy masik tombben indexet!
    */
    
    static void fillInArray(int[] array){
        int min = 0;
        int max = 100;
        for (int i = 0; i < array.length; i++) {
            array[i]=randomNumberGenerator(max, min);            
        }        
    }
    
    static int randomNumberGenerator(int max, int min){
        int number = (int)(Math.random()*(max - min)+min);
        return number;
    }
    
    static int mostCommonNumber(int[] array){
        int[] otherArray= new int [101];               //0-tol 100ig terjed
        for (int i = 0; i < array.length; i++) {
                int element = array[i];
                otherArray[element]++;                  //az index erteket fog jelenteni
        }
        int max = Integer.MIN_VALUE;
        int mostCommonNumber = 0;
        for (int i = 0; i < otherArray.length; i++) {
            if(otherArray[i] > max){                    //uj max keresese
                max = otherArray[i];
                mostCommonNumber = i;                   //akkor taroljuk i-t, hogyha uj maxot találtunk
            }
            
        }
        return mostCommonNumber;
    }
    
    public static void main(String[] args) {
        int[] arr = new int[300];
        fillInArray(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");     
        }
        System.out.println("");
        System.out.println(mostCommonNumber(arr));       
    }
            
}
