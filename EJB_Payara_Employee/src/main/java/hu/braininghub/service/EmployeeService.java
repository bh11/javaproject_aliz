/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.service;

import hu.braininghub.repository.EmployeeDao;
import hu.braininghub.repository.datatransferobject.Employee;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Aliz
 */
@Stateless
public class EmployeeService {
//olyan mint egy controller
    
    @Inject
    private EmployeeDao dao;
    
    public List<String> getNames(){        
          try {
              return dao.getEmployees().stream()
                      .map(Employee::toString)
                      .collect(Collectors.toList());
              
          } catch (Exception ex) {
              Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
              return new ArrayList<>();
          }
    }
    
    public List<Employee> getEmployees(String str){
          try {
              return dao.findByName(str);
          } catch (Exception ex) {
              Logger.getLogger(EmployeeService.class.getName()).log(Level.SEVERE, null, ex);
              return new ArrayList<>();
          }         
    }
}
